Vue.component('cost-radio',{props:['args','total','id','styles','labels'],data:function(){return{label:'',alias:'',classes:'',radioValue:[],labelFor:'option',radioButtons:null,}},created(){let vm=this;if(vm.args){vm.alias=vm.args.alias;vm.classes=vm.args.additionalStyles;vm.label=vm.args.label?vm.args.label:'';vm.radioButtons=vm.args.options?vm.args.options:[];vm.radioValue=this.args.default&&parseFloat(this.args.default)?parseFloat(this.args.default):[];}},template:`
               <div class="ccb-inner-wrapper">
                    <label  :style="labels">{{label}}:</label>
                    <template v-for="(item, index) in radioButtons" :key="index">
                        <input type="radio" :id="labelFor + index + id" v-model="radioValue" :value="item.optionValue" v-on:change="$emit(\'change\', radioValue, total, id, alias)">
                        <label :for="labelFor + index + id" class="light" :class="classes">{{item.optionText}}</label><br>
                    </template>
               </div>
              `,});
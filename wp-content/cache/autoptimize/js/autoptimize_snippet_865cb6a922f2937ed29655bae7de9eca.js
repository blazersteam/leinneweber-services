Vue.component('cost-quantity',{props:['args','total','id','labels'],data:function(){return{unit:1,alias:'',label:'',classes:'',placeholder:'',quantityValue:0,id_for_label:'quantity_',}},created(){let vm=this;if(vm.args){vm.alias=vm.args.alias;vm.classes=vm.args.additionalStyles;vm.id_for_label=vm.id_for_label+vm.id;vm.label=vm.args.label?vm.args.label:'';vm.placeholder=vm.args.placeholder?vm.args.placeholder:'';vm.unit=parseFloat(vm.args.unit)?parseFloat(vm.args.unit):1;vm.quantityValue=parseFloat(vm.args.default)?parseFloat(vm.args.default):vm.quantityValue;}},template:`
                 <div class="ccb-inner-wrapper">
                    <label :style="labels" :for="id_for_label">{{label}}:</label>
                    <input type="number" :class="classes" :placeholder="placeholder" :id="id_for_label" v-model="quantityValue"  v-on:input="$emit('keyup', quantityValue, total, id, alias, unit)">
                </div>
              `,});
Vue.component('cost-checkbox',{props:['args','total','id','labels'],data:function(){return{label:'',alias:'',classes:'',checkboxes:'',checkboxValue:[],labelFor:'option',}},created(){if(this.args){this.alias=this.args.alias;this.classes=this.args.additionalStyles;this.label=this.args.label?this.args.label:'';this.checkboxes=this.args.options?this.args.options:'';if(this.args.default&&parseFloat(this.args.default)){this.checkboxValue.push(parseFloat(this.args.default));}}},template:`<div class="ccb-inner-wrapper">
                  <label :style="labels">{{label}}:</label>
                  <template v-for="(item, index) in checkboxes" :key="index">
                     <input  type="checkbox" :id="labelFor + index + id" type="checkbox" :value="item.optionValue" v-model="checkboxValue" v-on:change="$emit(\'change\', checkboxValue, total, id, alias)">
                     <label class="light" :for="labelFor + index + id" :class="classes">{{item.optionText}}</label><br>
                  </template>
              </div>
              `,});
Vue.component('cost-input',{props:['args','id','labels'],data:function(){return{type:'',label:'',classes:'',inputValue:'',placeholder:'',id_for_label:'input_',}},created(){if(this.args){this.classes=this.args.additionalStyles;this.id_for_label=this.id_for_label+this.id;this.label=this.args.label?this.args.label:'';this.type=this.args.inputType?this.args.inputType:'text';this.placeholder=this.args.placeholder?this.args.placeholder:'';}},template:` 
                <div class="ccb-inner-wrapper">
                    <label  :style="labels" :for="id_for_label">{{label}}:</label>
                    <input :type="type" :placeholder="placeholder"  :class="classes" :id="id_for_label" v-model="inputValue">
                </div>
              `,});
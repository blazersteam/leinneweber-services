(function($){'use strict';var like={};eltd.modules.like=like;like.eltdLikes=eltdLikes;like.eltdOnDocumentReady=eltdOnDocumentReady;like.eltdOnWindowLoad=eltdOnWindowLoad;like.eltdOnWindowResize=eltdOnWindowResize;like.eltdOnWindowScroll=eltdOnWindowScroll;$(document).ready(eltdOnDocumentReady);$(window).load(eltdOnWindowLoad);$(window).resize(eltdOnWindowResize);$(window).scroll(eltdOnWindowScroll);function eltdOnDocumentReady(){eltdLikes();}
function eltdOnWindowLoad(){}
function eltdOnWindowResize(){}
function eltdOnWindowScroll(){}
function eltdLikes(){$(document).on('click','.eltd-like',function(){var likeLink=$(this),id=likeLink.attr('id'),type;if(likeLink.hasClass('liked')){return false;}
if(typeof likeLink.data('type')!=='undefined'){type=likeLink.data('type');}
var dataToPass={action:'malmo_elated_like',likes_id:id,type:type};var like=$.post(eltdLike.ajaxurl,dataToPass,function(data){likeLink.html(data).addClass('liked').attr('title','You already like this!');if(type!=='portfolio_list'){likeLink.children('span').css('opacity',1);}});return false;});}})(jQuery);
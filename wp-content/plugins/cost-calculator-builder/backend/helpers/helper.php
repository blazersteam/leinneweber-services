<?php
global $recaptcha;
global $recaptcha_info;
function stm_update_or_create_post($stm_temp, $extra, $formula, $settings)
{
    $my_post = array(
        'post_type' => 'cost-calc',
        'post_title' => $extra[1],
        'post_status' => 'publish',
    );

    $id = wp_insert_post($my_post);
    update_post_meta($id, 'stm-name', $extra[1]);
    update_post_meta($id, 'stm-fields', $stm_temp);
    update_post_meta($id, 'stm-formula', $formula);
    update_option('stm_ccb_form_settings_' . $id, $settings);
}


function stm_calc_get_all_posts($post_type)
{
    $args = array(
        'post_type' => $post_type,
        'posts_per_page' => -1,
        'post_status' => array('publish')
    );

    $resources = new WP_Query($args);

    $resources_json = array();

    if ($resources->have_posts()) {
        while ($resources->have_posts()) {
            $resources->the_post();
            $id = get_the_ID();
            $resources_json[$id] = get_the_title();
        }
    }

    return $resources_json;
}

function stm_sanitize_data($data)
{
    $data = json_decode(urldecode($data['data']));
    return $data;
}

function stm_sanitize_extra($data)
{
    $extra = json_decode(urldecode($data['extra']));
    $temp['name'] = $extra[1];

    return $temp;
}

function stm_calc_short_code($atts)
{
    $v = CALC_VERSION;
    wp_enqueue_script("jquery");
    wp_enqueue_script('ccb-vue', plugins_url('/frontend/js/libs/vue.min.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vuex', plugins_url('/frontend/js/libs/vuex.js', STM_CALCULATE));
    wp_enqueue_style('ccb-fontawesome', plugins_url('/frontend/css/fontawesome.min.css', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-resource', plugins_url('/frontend/js/libs/vue-resource.min.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-draggable', plugins_url('/frontend/js/libs/vuedraggable.js', STM_CALCULATE));

    wp_enqueue_style('cc-bcalc-form', plugins_url('/frontend/css/calc-form.css', STM_CALCULATE), []);
    wp_enqueue_script('ccb-date', plugins_url('/frontend/js/libs/vue2-datepicker.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-hr-component', plugins_url('/frontend/js/components/cost-line.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-text-component', plugins_url('/frontend/js/components/cost-text.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-html-component', plugins_url('/frontend/js/components/cost-html.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-range-component', plugins_url('/frontend/js/components/cost-range.js', STM_CALCULATE));
    wp_enqueue_script('ccb-ccb-vue-input-component', plugins_url('/frontend/js/components/cost-input.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-radio-component', plugins_url('/frontend/js/components/cost-radio.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-quantity-component', plugins_url('/frontend/js/components/cost-quantity.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-checkbox-component', plugins_url('/frontend/js/components/cost-checkbox.js', STM_CALCULATE));
    wp_enqueue_script('ccb-vue-drop-down-component', plugins_url('/frontend/js/components/cost-drop-down.js', STM_CALCULATE));
    wp_enqueue_script('ccb-form', plugins_url('/frontend/js/form.js', STM_CALCULATE), array('ccb-vue', 'ccb-vue-resource'), time(), true);
    wp_enqueue_script('ccb-bundle', plugins_url('/frontend/dist/bundle.js', STM_CALCULATE), array('ccb-vue', 'ccb-vue-resource'), time(), true);

    wp_localize_script('ccb-bundle', 'stmCalc', array('siteurl' => get_option('siteurl')));
    do_action('ccb-pro-enqueue-front-scripts');


    $params = shortcode_atts(array(
        'id' => null,
    ), $atts);
    wp_localize_script('ccb-bundle', 'stmCalculate', ['id' => $params['id']]);
    $post = get_post_meta((int)$params['id']);
    return ccb_get_template_calc($post, (int)$params['id']);
}

function ccb_get_template_calc($post, $calc_id)
{
    if (!empty($post)) {

        $totals = [];
        $f_models = [];
        $project_name = $post['stm-name'][0];
        $formula = unserialize($post['stm-formula'][0]);
        $post = unserialize($post['stm-fields'][0]);

        $form = get_option('stm_ccb_form_settings_' . $calc_id);
        $parsed_form = json_encode($form);

        $currency = isset($form[0]['general']) ? $form[0]['general']['currency'] : '$';
        $box_style = isset($form[0]['general']) ? $form[0]['general']['boxStyle'] : 'vertical';
        foreach ($formula as $item) {
            if (!empty($item)) {
                $f_models[] = $item;
                $totals[] = ['label' => $item['label'], 'symbol' => $currency];
            }
        }

        $output = '';

        if ($box_style === 'vertical') {
            return ccb_get_vertical_template($project_name, $f_models, $post, $totals, $form, $parsed_form);
        }elseif ($box_style){
            return ccb_get_horizontal_template($project_name, $f_models, $post, $totals, $form, $parsed_form);
        }

    }

    return __('No calculator was found', 'cost-calculator-builder');
}

function ccb_get_vertical_template($project_name, $f_models, $post, $totals, $form, $parsed_form)
{
    $output = '<div class="ccb-main-calc">
                      <div class="ccb-main-wrapper">
                        <div class="form-wrapper">
		                    <div :style="styles ? styles.containerStyles : {}" class="form-wrapper-content ccb-left-form-wrapper"><form class="form-inner-content">';

    $i = 0;
    $descriptions = [];
    $output .= "<h3>{$project_name}</h3>";
    $total = json_encode($f_models);

    foreach ($post as $key => $value) {
        $data = json_encode($value);
        if ($value['type'] !== 'Total' && $value['type'] !== 'Date Picker') {

            if ($value['type'] !== 'Html' && $value['type'] !== 'Text Area' && $value['type'] !== 'Line' && $value['type'] !== 'Input') {
                $descriptions[] = ['label' => $value['label'], 'key' => $key];
            }

            $output .= "<{$value['_tag']} v-on:{$value['_event']}='stmToCalculate' 
                             v-bind:labels=\"styles ? styles.labelStyles : {}\" v-bind:args='" . $data . "' v-bind:total='" . $total . "' v-bind:id=\"'{$key}'\"></{$value['_tag']}>";
        } elseif ($value['type'] === 'Date Picker') {

            $descriptions[] = ['label' => $value['label'], 'key' => $key, 'alias' => $value['alias'], 'date' => true];

            $output .= '<div class="ccb-inner-wrapper"><label :style="styles ? styles.labelStyles : {}">' . json_decode($data)->label . '</label><div class="ccb-date-picker">';
            $range = filter_var($value['range'], FILTER_VALIDATE_BOOLEAN) ? 'range' : '';
            $output .= "<{$value['_tag']} v-on:change='stmCalendarField($i, $total, $key, \" " . json_decode($data)->alias . " \", date[{$i}])' 
                    format='DD/MM/YYYY' v-model='date[{$i}]' :clearable='false' {$range} :first-day-of-week='1' confirm lang='en'></{$value['_tag']}>";

            $output .= '</div></div>';
            $i++;
        }
    }

    $output .= '</form></div><div class="ccb-form-right-wrapper" :style="styles ? styles.containerStyles : {}" class="ccb-calc-description form-wrapper-content"><div><form class="form-inner-content">
                     <div class="ccb-total-description">
                        <ul><li class="ccb-summary-title"><span><h4>' . __('Total Summary', 'cost-calculator-builder') . '</h4></span><span></span></li>';

    $ccb_date = 0;

    foreach ($descriptions as $desc) {
        if (!isset($desc['date'])) {
            $output .= "<li>
                             <span>{$desc['label']}</span>
                             <span>{{calcTotal.length ? calcTotal[{$desc['key']}] ? calcTotal[{$desc['key']}].value : 0 : 0}}</span>
                           </li>";
        } else {
            $output .= "<li>
                             <span>{$desc['label']}</span>
                             <span>
                                {{ dateDescription[$ccb_date] ? dateDescription[{$ccb_date}].value : 0 }}
                            </span>
                           </li>";
            $ccb_date++;
        }
    }

    foreach ($totals as $key => $total) {
        $output .= "<li><span class=\"ccb-summary-desc\">{$total['label']}: </span><span class=\"ccb-summary-value\">{$total['symbol']} {{formula.length ? formula[{$key}].total : 0}}</span></li>";
    }

    $output .= '</ul></div></form>';

    $has_premiums = false;
    $has_premiums = apply_filters('ccb-pro-exist', $has_premiums);


    if ($has_premiums && count($form) > 0) {

        $contactForm = $form[0]['formFields']['contactFormId'];
        $paypal = filter_var($form[0]['paypal']['enable'], FILTER_VALIDATE_BOOLEAN);
        $stripe = filter_var($form[0]['stripe']['enable'], FILTER_VALIDATE_BOOLEAN);
        $email = filter_var($form[0]['formFields']['accessEmail'], FILTER_VALIDATE_BOOLEAN);
        $recaptcha_enable = filter_var($form[0]['recaptcha']['enable'], FILTER_VALIDATE_BOOLEAN);
        $wooCommerce = (filter_var($form[0]['wooCommerce']['enable'], FILTER_VALIDATE_BOOLEAN) && !empty($form[0]['wooCommerce']['product_id']));

        global $recaptcha;
        global $recaptcha_info;
        $recaptcha = false;
        $recaptcha_info = $form[0]['recaptcha'];

        $recaptcha_access = ($recaptcha_enable && !empty($recaptcha_info['siteKey']));

        if (!$wooCommerce) {

            if (!($paypal || $stripe) && $email) {
                if (empty($contactForm)) {
                    if (count($form) > 0) {
                        $form = json_encode($form[0]);
                        $parsed_value = json_encode($recaptcha_access);

                        $id = uniqid();
                        $output .= "<div class='cbb-mt-40 form-wrapper-content form-inner-content' v-show='!nextButton'><cost-send-form id=\"{$id}\" :iteration='iteration' value=\"{$recaptcha_info['siteKey']}\" :recaptcha=\"{$parsed_value}\" v-on:send-form='sendForm' v-bind:field='" . $form . "'></cost-send-form></div>";
                        if ($recaptcha_access && !$recaptcha) {

                            ?>
                            <script>
                                var ccbCaptchaFnc = function () {
                                    setTimeout(function () {
                                        jQuery('.g-rec').each(function () {
                                            if (jQuery(this).attr('id')) {
                                                let ccb_id = grecaptcha.render(jQuery(this).attr('id'), {'sitekey': '<?php echo $recaptcha_info['siteKey']?>'});
                                                jQuery(this).data('widget_id', ccb_id);
                                            }
                                        });
                                    }, 1000);
                                }
                            </script>
                            <?php
                            echo '<script src="https://www.google.com/recaptcha/api.js?onload=ccbCaptchaFnc&render=explicit" async defer></script>';
                            $recaptcha = true;
                        }
                    }
                } else {
                    $output .= '<div v-show="!nextButton" class="ccb-contact-form7">';
                    $output .= do_shortcode('[contact-form-7 id="' . $contactForm . '"]');
                    $output .= '</div>';
                }
                $output .= "<div v-show=\"nextButton\" class=\"form-wrapper-content ccb-next-button-wrapper\"><a @click.prevent='nextButtonCallback($parsed_form)' class=\"ccb-next-button\">" . __('Submit', 'cost-calculator-builder') . "</a></div>";
            }
            $output .= '</div>';

            if ($paypal || $stripe) {
                ?>
                <script>
                    let stripe_id = '<?php  esc_html_e($form[0]['stripe']['publishKey']);?>';
                </script>

                <?php

                $output .= '<div class="ccb-calc-description form-wrapper-content form-inner-content"><div class="ccb-total-description">
                        <ul id="ccb_payments"><li class="ccb-summary-title"><span><h4 class="ccb_payment_methods">' . __('Payment Methods', 'cost-calculator-builder') . '</h4></span></li>';

                if ($form[0]['paypal']['enable']) {
                    $output .= '<li class="ccb-payment-method-paypal"><input type="radio" v-model="paymentMethod" value="paypal" name="payment_methods" id="payment_methods_paypal"><label for="payment_methods_paypal"><i>' . __('Pay', 'cost-calculator-builder') . '</i><i>' . __('Pal', 'cost-calculator-builder') . '</i></label></li>';
                }

                if ($form[0]['stripe']['enable']) {
                    $output .= '<li class="ccb-payment-method-stripe"><input type="radio" v-model="paymentMethod" value="stripe" name="payment_methods" id="payment_methods_stripe"><label for="payment_methods_stripe">' . __('Stripe', 'cost-calculator-builder') . '</label></li>';
                }

                $output .= '<div v-show="paymentMethod === \'stripe\'" id="stm-lms-stripe"></div>';
                $output .= '</ul></div></div><div class="no-methods" :class="payment.status">{{payment.message}}</div><button class="ccb-purchase"';
                $output .= "@click.prevent='paymentMethodCallback($parsed_form, \"$project_name\")'";
                $output .= '>' . __('Purchase', 'cost-calculator-builder') . '</button>';
            }
        } else {
            $descriptions = json_encode($descriptions);
            $output .= "<div v-show=\"nextButton\" class=\"form-wrapper-content ccb-next-button-wrapper\"><a @click.prevent='wooCommerceCallback($parsed_form,  \"$project_name\", $descriptions)' class=\"ccb-next-button\">" . __('Submit', 'cost-calculator-builder') . "</a></div>";
        }
        $output .= '</div></div></div></div>';

    } else {
        $output .= '</div></div></div></div></div>';
    }

    return $output;
}

function ccb_get_horizontal_template($project_name, $f_models, $post, $totals, $form, $parsed_form)
{
    $output = '<div class="ccb-main-calc">
                      <div class="ccb-main-wrapper">
                        <div class="form-wrapper">
		                    <div class="form-wrapper-content ccb-left-form-wrapper">';


    $output .= '<div class="ccb-horizontal-calc-wrapper" :style="styles && styles.horizontalStyle ? styles.horizontalStyle : {}">';

    $i = 0;
    $count = 0;
    $descriptions = [];
    $output .= "<h3>{$project_name}</h3>";
    $total = json_encode($f_models);
    $output .=  '<div id="ccb-horizontal-main">';
    foreach ($post as $key => $value) {
        $data = json_encode($value);
        if ($value['type'] !== 'Total' && $value['type'] !== 'Date Picker') {

            if ($value['type'] !== 'Html' && $value['type'] !== 'Text Area' && $value['type'] !== 'Line' && $value['type'] !== 'Input') {
                $descriptions[] = ['label' => $value['label'], 'key' => $key];
            }

            $output .= "<{$value['_tag']} v-on:{$value['_event']}='stmToCalculate' 
                              v-bind:labels=\"styles ? styles.labelStyles : {}\" v-bind:args='" . $data . "' v-bind:total='" . $total . "' v-bind:id=\"'{$key}'\"></{$value['_tag']}>";
        } elseif ($value['type'] === 'Date Picker') {

            $descriptions[] = ['label' => $value['label'], 'key' => $key, 'alias' => $value['alias'], 'date' => true];

            $output .= '<div class="ccb-inner-wrapper"><label :style="styles ? styles.labelStyles : {}">' . json_decode($data)->label . '</label><div class="ccb-date-picker">';
            $range = filter_var($value['range'], FILTER_VALIDATE_BOOLEAN) ? 'range' : '';
            $output .= "<{$value['_tag']} v-on:change='stmCalendarField($i, $total, $key, \" " . json_decode($data)->alias . " \", date[{$i}])' 
                    format='DD/MM/YYYY' v-model='date[{$i}]' :clearable='false' {$range} :first-day-of-week='1' confirm lang='en'></{$value['_tag']}>";

            $output .= '</div></div>';
            $i++;
        }
        $count++;
    }

    $output .= '</div></div><div class="ccb-horizontal-calc-wrapper" :style="styles && styles.horizontalStyle ? styles.horizontalStyle : {}"><div id="ccb-horizontal-total-summary"><ul><li class="ccb-horizontal-summary-title"><span><h4>' . __('Total Summary', 'cost-calculator-builder') . '</h4></span><span></span></li>';
    $ccb_date = 0;

    foreach ($descriptions as $desc) {
        if (!isset($desc['date'])) {
            $output .= "<li class='ccb-horizontal-summary-list'>
                             <span>{$desc['label']}</span>
                             <span>{{calcTotal.length ? calcTotal[{$desc['key']}] ? calcTotal[{$desc['key']}].value : 0 : 0}}</span>
                           </li>";
        } else {
            $output .= "<li>
                             <span>{$desc['label']}</span>
                             <span>
                                {{ dateDescription[$ccb_date] ? dateDescription[{$ccb_date}].value : 0 }}
                            </span>
                           </li>";
            $ccb_date++;
        }
    }
    foreach ($totals as $key => $total) {
        $output .= "<li class='ccb-horizontal-summary-list'><span><b>{$total['label']}:</b></span><span class=\"ccb-summary-value\">{$total['symbol']} {{formula.length ? formula[{$key}].total : 0}}</span></li>";
    }
    $output .= '</ul>';
    $has_premiums = false;
    $has_premiums = apply_filters('ccb-pro-exist', $has_premiums);

    if ($has_premiums && count($form) > 0) {

        $contactForm = $form[0]['formFields']['contactFormId'];
        $paypal = filter_var($form[0]['paypal']['enable'], FILTER_VALIDATE_BOOLEAN);
        $stripe = filter_var($form[0]['stripe']['enable'], FILTER_VALIDATE_BOOLEAN);
        $email = filter_var($form[0]['formFields']['accessEmail'], FILTER_VALIDATE_BOOLEAN);
        $recaptcha_enable = filter_var($form[0]['recaptcha']['enable'], FILTER_VALIDATE_BOOLEAN);
        $wooCommerce = (filter_var($form[0]['wooCommerce']['enable'], FILTER_VALIDATE_BOOLEAN) && !empty($form[0]['wooCommerce']['product_id']));

        global $recaptcha;
        global $recaptcha_info;
        $recaptcha = false;
        $recaptcha_info = $form[0]['recaptcha'];

        $recaptcha_access = ($recaptcha_enable && !empty($recaptcha_info['siteKey']));

        if (!$wooCommerce) {
            if (!($paypal || $stripe) && $email) {
                if (empty($contactForm)) {
                    if (count($form) > 0) {
                        $form = json_encode($form[0]);
                        $parsed_value = json_encode($recaptcha_access);
                        $id = uniqid();
                        $output .= "<div class='cbb-mt-40 form-wrapper-content form-inner-content' v-show='!nextButton'><cost-send-form id=\"{$id}\" :iteration='iteration' value=\"{$recaptcha_info['siteKey']}\" :recaptcha=\"{$parsed_value}\" v-on:send-form='sendForm' v-bind:field='" . $form . "'></cost-send-form></div>";
                        if ($recaptcha_access && !$recaptcha) {

                            ?>
                            <script>
                                var ccbCaptchaFnc = function () {
                                    setTimeout(function () {
                                        jQuery('.g-rec').each(function () {
                                            if (jQuery(this).attr('id')) {
                                                let ccb_id = grecaptcha.render(jQuery(this).attr('id'), {'sitekey': '<?php echo $recaptcha_info['siteKey']?>'});
                                                jQuery(this).data('widget_id', ccb_id);
                                            }
                                        });
                                    }, 1000);
                                }
                            </script>
                            <?php
                            echo '<script src="https://www.google.com/recaptcha/api.js?onload=ccbCaptchaFnc&render=explicit" async defer></script>';
                            $recaptcha = true;
                        }
                    }
                }else{
                    $output .= '<div v-show="!nextButton" class="ccb-horizontal-contact-form7">';
                    $output .= do_shortcode('[contact-form-7 id="' . $contactForm . '"]');
                    $output .= '</div>';
                }
                $output .= "<div v-show=\"nextButton\" class=\"form-wrapper-content ccb-next-button-wrapper\" :class=\"{'next-button-active': nextButton}\"><a @click.prevent='nextButtonCallback($parsed_form)' class=\"ccb-next-button\">" . __('Submit', 'cost-calculator-builder') . "</a></div>";
            }
            if ($paypal || $stripe) {
                ?>
                <script>
                    let stripe_id = '<?php  esc_html_e($form[0]['stripe']['publishKey']);?>';
                </script>

                <?php

                $output .= '<div class="ccb-calc-description form-wrapper-content form-inner-content"><div class="ccb-total-description">
                        <ul id="ccb_payments"><li class="ccb-summary-title"><span><h4 class="ccb_payment_methods">' . __('Payment Methods', 'cost-calculator-builder') . '</h4></span></li>';

                if ($form[0]['paypal']['enable']) {
                    $output .= '<li class="ccb-payment-method-paypal"><input type="radio" v-model="paymentMethod" value="paypal" name="payment_methods" id="payment_methods_paypal"><label for="payment_methods_paypal"><i>' . __('Pay', 'cost-calculator-builder') . '</i><i>' . __('Pal', 'cost-calculator-builder') . '</i></label></li>';
                }

                if ($form[0]['stripe']['enable']) {
                    $output .= '<li class="ccb-payment-method-stripe"><input type="radio" v-model="paymentMethod" value="stripe" name="payment_methods" id="payment_methods_stripe"><label for="payment_methods_stripe">' . __('Stripe', 'cost-calculator-builder') . '</label></li>';
                }

                $output .= '<div v-show="paymentMethod === \'stripe\'" id="stm-lms-stripe"></div>';
                $output .= '</ul></div></div><div class="no-methods" :class="payment.status">{{payment.message}}</div><button class="ccb-purchase"';
                $output .= "@click.prevent='paymentMethodCallback($parsed_form, \"$project_name\")'";
                $output .= '>' . __('Purchase', 'cost-calculator-builder') . '</button>';
            }
        }else {
            $descriptions = json_encode($descriptions);
            $output .= "<div v-show=\"nextButton\" class=\"form-wrapper-content ccb-next-button-wrapper\" :class=\"{'next-button-active': nextButton}\"><a @click.prevent='wooCommerceCallback($parsed_form,  \"$project_name\", $descriptions)' class=\"ccb-next-button\">" . __('Submit', 'cost-calculator-builder') . "</a></div>";
        }
    }

    $output .= '</div></div></div></div></div></div>';

    return $output;
}

add_shortcode('stm-calc', 'stm_calc_short_code');
<?php

namespace STM_CALC\backend\includes\Controllers;

class STM_Rest_Api_Callbacks
{
    public function stm_send_styles_callback()
    {
        $option = get_option('stm-cost-calc-styles');
        if ($option) {
            wp_send_json(['success' => true, 'message' => $option]);
        } else {
            wp_send_json(['success' => false, 'message' => 'option not found']);
        }
    }

    public function stm_save_styles_callback($data)
    {
        $params = $data->get_json_params();
        $styles = $params['styles'];
        update_option('stm-cost-calc-styles', $styles);
        wp_send_json(['success' => true, 'message']);
    }

    public function stm_edit_existing_callback($data)
    {
        $params = $data->get_json_params();
        $id = $params['id'];
        $post = get_post_meta($id);
        $formula = unserialize($post['stm-formula'][0]);

        if (!empty($post)) {
            $temp['id'] = $id;
            $temp['project_name'] = get_the_title($id);
            $temp['field'] = unserialize($post['stm-fields'][0]);
            $temp['settings'] = get_option('stm_ccb_form_settings_' . $id);
            wp_send_json(['success' => true, 'message' => ['field' => $temp, 'formula' => $formula]]);
        } else {
            wp_send_json(['success' => false, 'message' => 'post not found']);
        }
    }

    public function stm_remove_existing_callback($data)
    {
        $params = $data->get_json_params();
        wp_delete_post($params['id']);
        delete_post_meta($params['id'], 'cost-calc');
        wp_send_json(['success' => true, 'message' => $params]);
    }

    public function stm_get_listing_callback()
    {
        // getting woo products
        $all_products = [];
        $all_products = apply_filters('ccb_getting_woo_products', $all_products);

        // getting contact forms 7
        $forms[] = ['ID' => '', 'title' => 'Contact-form 7 not found'];
        $forms = apply_filters('ccb_getting_contact_forms', $forms);

        // getting all the calculators
        $main = [];
        $calc_posts = stm_calc_get_all_posts('cost-calc');
        foreach ($calc_posts as $key => $value) {
            $temp = [];
            $post = get_post_meta($key);
            $temp['id'] = $key;
            $temp['project_name'] = !empty($value) ? $value : 'name is empty';
            $temp['field'] = unserialize($post['stm-fields'][0]);
            $main[] = $temp;
        }

        wp_send_json(['success' => true, 'message' => ['main' => $main, 'forms' => $forms, 'products' => $all_products]]);
    }

    public function stm_auto_calc($data)
    {
        $post = get_post_meta($data['id']);
        if (!empty($post)) {
            $post['stm-fields'] = unserialize($post['stm-fields'][0]);
            foreach ($post['stm-formula'] as $key => $value){
                $post['stm-formula'] = unserialize($value);
            }

            wp_send_json(['success' => true, 'message' => $post]);
        }
        wp_send_json(['success' => false, 'message' => 'post not found']);
    }

    public function stm_save_calc($data)
    {
        $params = $data->get_json_params();
        $formula = (isset($params['formula'])) ? $params['formula'] : [];
        $extra = (isset($params['extraInfo'])) ? $params['extraInfo'] : [];
        $settings = (isset($params['settings'])) ? $params['settings'] : [];
        $data = (isset($params['readyFields'])) ? $params['readyFields'] : [];

        $calc_posts = stm_calc_get_all_posts('cost-calc');

        if (empty($calc_posts) || empty($extra[0])) {
            stm_update_or_create_post($data, $extra, $formula, $settings);
        } else {
            update_option('stm_ccb_form_settings_' . $extra[0], $settings);
            foreach ($calc_posts as $key => $value) {
                if ($key === (int)$extra[0]) {
                    $calc_update = ['ID' => $key, 'post_title' => $extra[1]];
                    wp_update_post($calc_update);
                    update_post_meta($key, 'stm-fields', $data);
                    update_post_meta($key, 'stm-name', $extra[1]);
                    update_post_meta($key, 'stm-formula', $formula);
                }
            }
        }

        wp_send_json(['success' => true, 'message' => 'Form created successfully']);
    }

    public function stm_get_preview_callback($data)
    {
        $totals = [];
        $f_models = [];
        $descriptions = [];
        $params = $data->get_json_params();
        $formula = (isset($params['formula'])) ? $params['formula'] : [];
        $post = (isset($params['readyFields'])) ? $params['readyFields'] : [];

        foreach ($formula as $item) {
            if (!empty($item)) {
                $f_models[] = $item;
                $totals[] = ['label' => $item['label'], 'symbol' => $item['symbol']];
            }
        }

        $temp1 = '<div class="form-wrapper">
		                    <form :style="styles ? styles.containerStyles : {}">';

        foreach ($post as $key => $value) {
            $data = json_encode($value);
            $total = json_encode($f_models);
            if ($value['type'] !== 'Total') {
                if ($value['type'] !== 'Html' && $value['type'] !== 'Text Area' && $value['type'] !== 'Line' && $value['type'] !== 'Input') {
                    $descriptions[] = ['label' => $value['label'], 'key' => $key];
                }

                $temp1 .= "<{$value['_tag']} v-on:{$value['_event']}='stmToCalculate' 
                          v-bind:fields=\"styles ? styles.fieldStyles : {}\" v-bind:labels=\"styles ? styles.labelStyles : {}\" v-bind:args='"
                    . $data . "' v-bind:total='" . $total . "' v-bind:id=\"'{$key}'\"></{$value['_tag']}>";
            }
        }

        $temp1 .= '</form><form class="ccb-calc-description">
                     <div class="ccb-total-description">
                        <ul><li class="ccb-summary-title"><span><h4>' . __('Total Descriptions', 'cost-calculator-builder') . '</h4></span><span></span></li>';

        foreach ($descriptions as $desc) {
            $temp1 .= "<li>
                         <span>{$desc['label']}</span>
                         <span>{{calcTotal.length ? calcTotal[{$desc['key']}] ? calcTotal[{$desc['key']}].value : 0 : 0}}</span>
                       </li>";
        }

        foreach ($totals as $key => $total) {
            $temp1 .= "<li><span class=\"ccb-summary-desc\">{$total['label']}: </span><span class=\"ccb-summary-value\">{$total['symbol']} {{formula.length ? formula[{$key}].total : 0}}</span></li>";
        }

        $temp1 .= '</ul></div></form></div>';

        wp_send_json(['success' => true, 'message' => $temp1]);
    }

}
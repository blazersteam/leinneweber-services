<?php
    $template = '<template v-else-if="modalField === \'date-picker\'">
                    <ccb-free-datepicker v-on:click="closeModal"></ccb-free-datepicker>
                </template>';

    $main_settings_template = '<template v-else-if="modalField === \'main-settings\'">
                                   <free-main-settings v-on:set-settings="setSettings" v-bind:products="wcProducts" v-bind:forms="contactForms" v-bind:field="settings[settingsId]" v-bind:id="settingsId"></free-main-settings>
                               </template>'
?>

<div id="ccbModal" class="ccb-modal" v-bind:class="{'ccb-display' : stmDisplay}">
    <div class="ccb-modal-content">
        <span class="ccb-close" v-on:click="closeModal">&times;</span>
        <div class="ccb-modal-container">
            <template v-if="modalField === 'input'">
                <input-field v-bind:field="modalData" :index="newIndex" v-bind:order="order" v-bind:id="fieldId" v-on:click="saveField"></input-field>
            </template>
            <template v-else-if="modalField === 'drop-down'">
                <drop-down-field v-bind:field="modalData" :index="newIndex" v-bind:order="order" v-bind:id="fieldId" v-on:click="saveField"></drop-down-field>
            </template>
            <template v-else-if="modalField === 'radio-button'">
                <radio-button v-bind:field="modalData" v-bind:order="order" :index="newIndex" v-bind:id="fieldId" v-on:click="saveField"></radio-button>
            </template>
            <template v-else-if="modalField === 'checkbox'">
                <checkbox-field v-bind:field="modalData" v-bind:order="order" :index="newIndex" v-bind:id="fieldId" v-on:click="saveField"></checkbox-field>
            </template>
            <template v-else-if="modalField === 'range-button'">
                <range-field v-bind:field="modalData" v-bind:order="order" :index="newIndex" v-bind:id="fieldId" v-on:click="saveField"></range-field>
            </template>
            <template v-else-if="modalField === 'quantity'">
                <quantity-field v-bind:field="modalData" v-bind:order="order" :index="newIndex" v-bind:id="fieldId" v-on:click="saveField"></quantity-field>
            </template>
            <template v-else-if="modalField === 'text-area'">
                <text-area-field v-bind:field="modalData" v-bind:order="order" :index="newIndex" v-bind:id="fieldId" v-on:click="saveField"></text-area-field>
            </template>
            <template v-else-if="modalField === 'html'">
                <html-field v-bind:field="modalData" v-bind:order="order" :index="newIndex" v-bind:id="fieldId" v-on:click="saveField"></html-field>
            </template>
            <template v-else-if="modalField === 'line'">
                <line-field v-bind:field="modalData" v-bind:order="order" :index="newIndex" v-bind:id="fieldId" v-on:click="saveField"></line-field>
            </template>
            <?php
                $template = apply_filters('ccb_date_picker', $template);
                $main_settings_template = apply_filters('ccb_main_settings', $main_settings_template);

                echo $template;
                echo $main_settings_template;
            ?>

            <template v-else-if="modalField === 'total'">
                <total-field v-bind:available="readyFields"
                             v-bind:id="fieldId"
                             v-bind:order="order"
                             v-on:click="saveField"
                             v-bind:index="newIndex"
                             v-bind:field="modalData" >
                </total-field>
            </template>
        </div>
    </div>
</div>

<div id="listing-delete-modal" class="modal">
    <p><?php echo __('Are you sure to delete this item?', 'cost-calculator-builder') ?></p>
    <a href="#" rel="modal:close" v-on:click.prevent="removeItem"><?php echo __('Delete', 'cost-calculator-builder') ?></a>
</div>
<div class="create-page-wrapper">
    <div>
        <h1><?php echo __("Create Cost Calculator", "cost-calculator-builder"); ?></h1>
        <div id="ccb-crete">
            <label for="create-input" class="ccb-create-input">
                <input type="text" id="create-input" placeholder="&nbsp;" v-model="projectName">
                <span class="ccb-label-1"><?php echo __("Name", "cost-calculator-builder"); ?></span>
                <span class="border"></span>

                <div v-on:click="showContainer()" id="ccb-add-form">
                    <i class="fa fa-plus" id="plus"></i>
                </div>
            </label>
        </div>
        <div class="ccb-temp-sidebar">
            <div class="ccb-sidebar-info" v-show="access">
                <div><?php echo __("Name:", "cost-calculator-builder"); ?> <span class="ccb-project-name">{{projectNameMain}}</span>
                </div>
            </div>
            <div class="ccb-modal-body" v-bind:class="{ 'ccb-tools-opacity' : readyFields.length }">
                <p>
                    <button class="accent"
                            @click.prevent="toggleModal()"><?php echo __('Preview', 'cost-calculator-builder') ?></button>
                </p>

                <cost-modal v-if="modal.isOpen" :has-mask="modal.hasMask" :can-click-mask="modal.canClickMask"
                            :has-x="modal.hasX" @toggle="toggleModal">
                    <article v-cloak>
                        <section class="ccb-modal-section" v-if="previewSuccess">
                            <div class="form-wrapper">
                                <template v-if="settings[0].general && settings[0].general.boxStyle === 'horizontal'">
                                    <div class="form-wrapper-content ccb-left-form-wrapper">
                                        <div class="ccb-horizontal-calc-wrapper">
                                            <h3>{{projectName}}</h3>
                                            <div id="ccb-horizontal-main">
                                                <stm-show-preview v-for="(value, key) in readyFields"
                                                                  v-on:calendar-event='stmCalendarField'
                                                                  :key="key" :id="key" :styles="styles" :total="temp"
                                                                  :field="value" v-on:stm-callback="stmToCalculate">
                                                </stm-show-preview>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ccb-horizontal-calc-wrapper form-wrapper-horizontal-summary">
                                        <div id="ccb-horizontal-total-summary">
                                            <ul>
                                                <li class="ccb-horizontal-summary-title">
                                                    <span>
                                                        <h4><?php echo __('Total Summary', 'cost-calculator-builder');?></h4>
                                                    </span>
                                                </li>
                                                <template v-for="(value, key) in readyFields">
                                                    <li class="ccb-horizontal-summary-list" v-if="value.type !== 'Total' && value.type !== 'Date Picker' && value.type !== 'Html' && value.type !== 'Line' && value.type !== 'Text Area' && value.type !== 'Input'">
                                                        <span>{{value.label}}</span>
                                                        <span>{{calcTotal.length ? calcTotal[key] ? calcTotal[key].value : 0 : 0}}</span>
                                                    </li>
                                                    <li v-else-if="value.type === 'Date Picker'">
                                                        <span>{{value.label}}</span>
                                                        <span v-if="dateDescription.length">
                                                        <template v-for="element in dateDescriptiƒon">
                                                            {{ element && element.alias === value.alias ? element.value : ''}}
                                                        </template>
                                                    </span>
                                                        <span v-else>
                                                        0
                                                    </span>
                                                    </li>
                                                </template>
                                                <li class="ccb-horizontal-summary-list" v-for="(value, key) in formula" :key="key">
                                                    <span class="ccb-summary-desc">{{value.label}}: </span>
                                                    <span class="ccb-summary-value">{{settings[0].general && settings[0].general.currency ? settings[0].general.currency : '$'}} {{value.total ? value.total : 0}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </template>
                                <template v-else>
                                    <div class="form-wrapper-content form-inner-content" id="left_preview">
                                        <stm-show-preview v-for="(value, key) in readyFields"
                                                          v-on:calendar-event='stmCalendarField'
                                                          :key="key" :id="key" :styles="styles" :total="temp"
                                                          :field="value" v-on:stm-callback="stmToCalculate">
                                        </stm-show-preview>
                                    </div>
                                    <div class="ccb-calc-description form-wrapper-content form-inner-content"
                                         id="right_preview">
                                        <div class="ccb-total-description">
                                            <ul>
                                                <li class="ccb-summary-title">
                                                    <span><h4><?php echo __('Total Summary', 'cost-calculator-builder');?></h4></span>
                                                </li>
                                                <template v-for="(value, key) in readyFields">
                                                    <li v-if="value.type !== 'Total' && value.type !== 'Date Picker' && value.type !== 'Html' && value.type !== 'Line' && value.type !== 'Text Area' && value.type !== 'Input'">
                                                        <span>{{value.label}}</span>
                                                        <span>{{calcTotal.length ? calcTotal[key] ? calcTotal[key].value : 0 : 0}}</span>
                                                    </li>
                                                    <li v-else-if="value.type === 'Date Picker'">
                                                        <span>{{value.label}}</span>
                                                        <span v-if="dateDescription.length">
                                                        <template v-for="element in dateDescription">
                                                            {{ element && element.alias === value.alias ? element.value : ''}}
                                                        </template>
                                                    </span>
                                                        <span v-else>
                                                        0
                                                    </span>
                                                    </li>
                                                </template>
                                                <li v-for="(value, key) in formula" :key="key">
                                                    <span class="ccb-summary-desc">{{value.label}}: </span>
                                                    <span class="ccb-summary-value">{{settings[0].general && settings[0].general.currency ? settings[0].general.currency : '$'}} {{value.total ? value.total : 0}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </template>
                            </div>

                        </section>
                    </article>
                    <footer>
                        <div class="forward-actions">
                            <button class="accent save" :disabled="!isLastStep" v-show="isLastStep"
                                    @click.prevent="finish"><i
                                        class="fa fa-fw fa-lg fa-check"></i></button>
                        </div>
                    </footer>
                </cost-modal>

            </div>
        </div>

        <div v-bind:class="{'ccb-tools-opacity' : hasAccess, 'ccb-modal-active': toolZIndex}" class="ccb-form-wrap">
            <div class="ccb-cost-calc-block">
                <div class="main-settings" v-on:click="checkSettings('main-settings', settingsId)">
                    <i title="Edit Field" class="fal fa-pencil"></i>
                </div>

                <div>
                    <div class="ccb-cost-calc-empty" v-if="!readyFields.length">
                        <i title="<?php echo __("Add", "cost-calculator-builder"); ?>"
                           class="fa fa-plus ccb-show-tools"></i>
                    </div>
                </div>
                <draggable group="fields"
                           :list="readyFields"
                           v-model="readyFields"
                           v-bind="dragOptions"
                           @change="log"
                >
                    <transition-group type="transition" name="flip-list">
                        <cost-field
                                v-for="(field,key) in readyFields"
                                v-bind:id="key" v-bind:key="key" v-bind:field="field"
                                v-on:remove="removeFields" v-bind:order="order"
                                v-on:click="editField">
                        </cost-field>
                    </transition-group>
                </draggable>
            </div>
        </div>

        <div class="ccb-buttons-wrapper">
            <div class="ccb-extra-buttons"
                 v-bind:class="{'ccb-tools-opacity' : readyFields.length, 'ccb-modal-active': toolZIndex}"
                 v-if="create">
                <button type="submit" class="ccb-box">
                    <a href="#" v-on:click.prevent="saveCalc"
                       class="ccb-extra-btn ccb-btn-white ccb-btn-animation-1"><?php echo __("Create", "cost-calculator-builder"); ?></a>
                </button>
            </div>
        </div>
        <div class="ccb-buttons-wrapper" v-if="!create">
            <div class="ccb-extra-buttons"
                 v-bind:class="{'ccb-tools-opacity' : readyFields.length, 'ccb-modal-active': toolZIndex}">
                <button type="submit" class="ccb-box">
                    <a href="#" v-on:click.prevent="saveCalc"
                       class="ccb-extra-btn ccb-btn-white ccb-btn-animation-1"><?php echo __("Save", "cost-calculator-builder"); ?></a>
                </button>
            </div>
            <div class="ccb-extra-buttons"
                 v-bind:class="{'ccb-tools-opacity' : readyFields.length, 'ccb-modal-active': toolZIndex}">
                <button type="submit" class="ccb-box ccb-cancel">
                    <a href="#" v-on:click.prevent="stmCancel"
                       class="ccb-extra-btn ccb-btn-red ccb-btn-animation-1"><?php echo __("Cancel", "cost-calculator-builder-pro"); ?></a>
                </button>
            </div>
        </div>
    </div>

    <div v-bind:class="{ 'ccb-tools-opacity': showTools, 'ccb-modal-active': toolZIndex, 'ccb-zIndex' : navZIndex }"
         class="ccb-side-bar-tools">
        <nav class="ccb-nav">
            <ul class="ccb-tools">
                <draggable
                        :list="$store.getters.getFields"
                        :group="{ name: 'fields', pull: 'clone', put: false }"
                        @change="log"
                        :sort="false"
                >
                    <li v-for="tool in $store.getters.getFields" v-bind:key="tool.id"
                        v-on:click.prevent="getField(tool.type)">

                        <a href="">
                            <i v-bind:class="tool.icon"></i>
                            <strong>{{tool._name}}</strong>
                            <small>{{tool.description}}</small>
                        </a>
                    </li>
                </draggable>

            </ul>
        </nav>
    </div>
</div>
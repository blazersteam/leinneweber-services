<div>
    <div id="ccb-custom-wrapper">
        <div class="ccb-custom-container">
            <div class="ccb-custom-inner-wrap">
                <div class="ccb-gc ccb-gc-custom-1">
                    <div class="ccb-custom-menu">
                        <div class="ccb-custom-active"><span
                                    class="light"></span><span><?php echo __("Label", "cost-calculator-builder"); ?></span>
                        </div>
                        <div>
                            <span class="light"></span><span><?php echo __("V-Container", "cost-calculator-builder"); ?></span>
                        </div>
                        <div>
                            <span class="light"></span><span><?php echo __("H-Container", "cost-calculator-builder"); ?></span>
                        </div>
                    </div>
                </div>
                <div class="ccb-gc ccb-gc-custom-2" v-if="load">
                    <ul class="ccb-custom-ul">
                        <li class="ccb-custom-active">
                            <div class="ccb-custom-text">
                                <table class="custom-table">
                                    <tbody>
                                    <tr>
                                        <td><?php echo __("Font-Size", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" class="ccb-custom-range"
                                                   v-model="styles.labelStyles.fontSize"/></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Weight/Style", "cost-calculator-builder"); ?></td>
                                        <td>
                                            <div class="ccb-custom-checkbox-wrap">
                                            <span class="ccb-custom-check-button">
                                                <input type="checkbox" class="ccb-custom-checkbox" id="fontWeight"
                                                       v-model="styles.labelStyles.fontWeight" value="bold">
                                                <label for="fontWeight">bold</label>
                                            </span>
                                                <span class="ccb-custom-check-button">
                                                <input type="checkbox" class="ccb-custom-checkbox" id="fontStyle"
                                                       v-model="styles.labelStyles.fontStyle" value="italic">
                                                <label for="fontStyle">italic</label>
                                            </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Text-shadow", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 3px 3px rgba(0, 0, 0, 0.1)"
                                                   class="ccb-custom-input" v-model="styles.labelStyles.textShadow">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Padding", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 30px 30px 30px 30px"
                                                   class="ccb-custom-input"
                                                   v-model="styles.labelStyles.padding"></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Margin", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 30px 30px 30px 30px"
                                                   class="ccb-custom-input"
                                                   v-model="styles.labelStyles.margin"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <div class="ccb-custom-text">
                                <table class="custom-table">
                                    <tbody>
                                    <tr>
                                        <td><?php echo __("Width", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 720px" class="ccb-custom-input"
                                                   v-model="styles.containerStyles.width"></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Margin", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 30px 30px 30px 30px"
                                                   class="ccb-custom-input" v-model="styles.containerStyles.margin">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Padding", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 30px 30px 30px 30px"
                                                   class="ccb-custom-input" v-model="styles.containerStyles.padding">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Box-shadow", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 0 0 9px rgba(0, 0, 0, 0.2)"
                                                   class="ccb-custom-input" v-model="styles.containerStyles.boxShadow">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Inset Box-Shadow", "cost-calculator-builder"); ?></td>
                                        <td>
                                            <div class="ccb-custom-checkbox-wrap">
                                            <span class="ccb-custom-check-button">
                                                <input type="checkbox" class="ccb-custom-checkbox" id="shadowInset"
                                                       v-model="styles.containerStyles.inset" value="inset">
                                                <label for="shadowInset">Inset</label>

                                            </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Border-Radius", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="1px"
                                                   class="ccb-custom-input"
                                                   v-model="styles.containerStyles.borderRadius"></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Background-Color", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="#color" class="ccb-custom-input"
                                                   v-model="styles.containerStyles.backgroundColor"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li v-if="styles.horizontalStyle">
                            <div class="ccb-custom-text">
                                <table class="custom-table">
                                    <tbody>
                                    <tr>
                                        <td><?php echo __("Width", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 720px" class="ccb-custom-input"
                                                   v-model="styles.horizontalStyle.width"></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Margin", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 30px 30px 30px 30px"
                                                   class="ccb-custom-input" v-model="styles.horizontalStyle.margin">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Padding", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 30px 30px 30px 30px"
                                                   class="ccb-custom-input" v-model="styles.horizontalStyle.padding">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Box-shadow", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="example 0 0 9px rgba(0, 0, 0, 0.2)"
                                                   class="ccb-custom-input" v-model="styles.horizontalStyle.boxShadow">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Inset Box-Shadow", "cost-calculator-builder"); ?></td>
                                        <td>
                                            <div class="ccb-custom-checkbox-wrap">
                                            <span class="ccb-custom-check-button">
                                                <input type="checkbox" class="ccb-custom-checkbox" id="shadowInset"
                                                       v-model="styles.horizontalStyle.inset" value="inset">
                                                <label for="shadowInset">Inset</label>

                                            </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Border-Radius", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="1px"
                                                   class="ccb-custom-input"
                                                   v-model="styles.horizontalStyle.borderRadius"></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __("Background-Color", "cost-calculator-builder"); ?></td>
                                        <td><input type="text" placeholder="#color" class="ccb-custom-input"
                                                   v-model="styles.horizontalStyle.backgroundColor"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<button class="ccb-custom-box" v-on:click.prevent="changeStyles">
    <a href="#"
       class="ccb-extra-btn ccb-btn-white ccb-btn-animation-1"><?php echo __("Save", "cost-calculator-builder"); ?></a>
</button>

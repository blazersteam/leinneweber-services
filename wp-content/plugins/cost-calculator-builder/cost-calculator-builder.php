<?php
/**
 * Plugin Name: Cost Calculator Builder
 * Plugin URI: https://wordpress.org/plugins/cost-calculator-builder/
 * Description: WP Cost Calculator helps you to build any type of estimation forms on a few easy steps. The plugin offers its own calculation builder.
 * Author: StylemixThemes
 * Author URI: https://stylemixthemes.com/
 * License: GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: cost-calculator-builder
 * Version: 1.0.5
 */

namespace STM_CALC;
if (!defined('ABSPATH')) exit;

use STM_CALC\backend\includes\Controllers\STM_Notice;
use STM_CALC\backend\includes\Controllers\STM_Rest_Api;
use STM_CALC\backend\includes\Controllers\STM_EnqueueController;
use STM_CALC\backend\includes\Controllers\STM_SettingsController;

define('CALC_VERSION', '1.0.5');
define('STM_CALCULATE', __FILE__);
define('STM_CALCULATE_PATH', dirname(__FILE__));
define('STM_CALCULATE_URL', plugins_url(__FILE__));

spl_autoload_register(__NAMESPACE__ . '\\stm_autoload');
add_action('plugins_loaded', array(new STM_CALC, 'stm_init'));

class STM_CALC
{
    private $rest_api;
    public static $settings;

    public function __construct()
    {
        require STM_CALCULATE_PATH . '/backend/includes/autoload.php';
        $this->rest_api = new STM_Rest_Api();
        load_plugin_textdomain('cost-calculator-builder', false, basename(dirname(__FILE__)) . '/languages');
        $this->stm_init();

    }

    public function stm_init()
    {
        add_action('init', array($this->rest_api, 'stm_init'));
        load_plugin_textdomain('cost-calculator-builder', false, basename(dirname(__FILE__)) . '/languages');
        add_action('admin_enqueue_scripts', array(STM_EnqueueController::stmGetInstance(), 'stm_enqueue_admin_scripts'));
        add_action('admin_menu', array(STM_SettingsController::stmGetInstance(), 'stm_init'));
    }
}

function stm_autoload($class = '')
{
    if (strpos($class, 'STM_CALC') !== 0) {
        return;
    }

    $return = str_replace('STM_CALC\\', '', $class);
    $return = str_replace('\\', '/', $return);
    require $return . '.php';
}
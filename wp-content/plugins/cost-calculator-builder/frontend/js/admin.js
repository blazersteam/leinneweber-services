import store from './data/index';

Vue.component('date-picker', DatePicker.default);

let $ = jQuery;
var iteration = 0;

$('.ccb-main-calc').each(function () {
    let $this = $(this);
    new Vue({
        el: $(this)[0],
        store,
        data: function () {
            return {
                temp: '',
                date: [],
                order: 0,
                drag: false,
                formula: [],
                load: false,
                settings: [],
                styles: null,
                create: true,
                listings: [],
                fieldId: null,
                extraInfo: [],
                access: false,
                calcTotal: [],
                wcProducts: [],
                modalField: '',
                oderFields: [],
                removeId: null,
                newIndex: null,
                modalData: null,
                isActive: false,
                projectId: null,
                projectName: '',
                readyFields: [],
                preloader: true,
                navZIndex: true,
                contactForms: [],
                nextButton: true,
                settingsId: null,
                hasAccess: false,
                showTools: false,
                toolZIndex: false,
                stmDisplay: false,
                dateDescription: [],
                projectNameMain: '',
                iteration: iteration,
                removeArrayId: false,
                previewSuccess: false,
                stmMainContainer: true,
                location_url: stmCalc.siteurl,

                paymentMethod: '',
                stripe: {
                    stripe: '',
                    stripeCard: '',
                    stripeComplete: '',
                },

                payment: {
                    status: '',
                    message: '',
                },

                modal: {
                    isOpen: false,
                    hasMask: true,
                    canClickMask: false,
                    hasX: false
                },
                list: [],

            }
        },

        created() {
            let vm = this;
            vm.setOption();
            vm.editItem();
            vm.getListings().then(function (response) {
                if (response.body.success) {
                    vm.listings = response.body.message.main;
                    vm.contactForms = response.body.message.forms;
                    vm.wcProducts = response.body.message.products;
                }
            });
            vm.stmAutoCalc();
        },

        computed: {
            dragOptions() {
                return {
                    animation: 200,
                    group: "description",
                    disabled: false,
                    ghostClass: "ghost"
                };
            },

            isLastStep: function () {
                let vm = this;
                return (vm.step === vm.max)
            },
        },

        watch: {
            readyFields() {
                this.checkAvailable();
            },

            paymentMethod: function (value) {
                let vm = this;
                if (value === 'stripe') {
                    vm.generateStripe();
                } else {
                    vm.payment.status = '';
                    vm.payment.message = '';
                }
            }
        },

        methods: {
            checkAvailable() {
                let vm = this;
                vm.readyFields.forEach((value, index) => {
                    if (typeof value === "undefined" || !value.hasOwnProperty('_id')) {
                        vm.readyFields.splice(index, 1);
                    }
                });
            },

            log: function (event) {
                let vm = this;
                let current = event.added;
                if (current) {
                    vm.newIndex = current.newIndex;
                    vm.getField(current.element.type);
                }
            },

            generateStripe: function () {
                let vm = this;
                Vue.nextTick(function () {

                    if (!stripe_id.length) {
                        vm.payment.status = 'danger';
                        vm.payment.message = 'Publish key is empty';
                        return false;
                    } else {
                        vm.payment.status = '';
                        vm.payment.message = '';
                    }

                    vm.stripe.stripe = Stripe(stripe_id);
                    let elements = vm.stripe.stripe.elements();
                    vm.stripe.stripeCard = elements.create('card');
                    vm.stripe.stripeCard.mount('#stm-lms-stripe');
                    vm.stripe.stripeCard.addEventListener('change', function (event) {
                        vm.stripe.stripeComplete = event.complete;
                    });
                });
            },

            toggleModal: function () {
                let vm = this;
                vm.calcTotal = [];
                vm.toolZIndex = !vm.toolZIndex;
                vm.modal.isOpen = !vm.modal.isOpen;
                if (this.modal.isOpen) {
                    vm.temp = vm.checkTotalFields();
                    vm.previewSuccess = true;

                    setTimeout(function () {
                        vm.$sections = vm.$el.querySelectorAll('.ccb-modal-section');
                        vm.max = vm.$sections.length;
                    }, 1000);
                }
            },

            finish: function () {
                let vm = this;
                vm.toggleModal()
            },

            getListings: function () {
                let vm = this;
                return vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/stm-listing/');
            },

            checkURI: function (parameterName) {
                let result = '',
                    temp = [];
                let uri = location.search.substr(1).split("&");
                for (let index = 0; index < uri.length; index++) {
                    temp = uri[index].split("=");
                    if (temp[0] === parameterName) result = decodeURIComponent(temp[1]);
                }
                return result;
            },

            sendForm: function (userEmail, subject, fields, ccb_recaptcha) {
                let vm = this;
                vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/send-form/', {
                    mainInfo: {
                        subject: subject,
                        userEmail: userEmail,
                    },
                    sendFields: fields,
                    calcTotals: vm.formula,
                    ccb_recaptcha: ccb_recaptcha,
                })
            },

            wooCommerceCallback: function (params, item_name, descriptions) {
                let vm = this;
                let desc = [];
                descriptions.forEach(function (element) {
                    desc.push({
                        alias: element.label,
                        value: vm.calcTotal[element.key] ? vm.calcTotal[element.key].value : 0
                    });
                });

                $.ajax({
                    type: 'POST',
                    url: '/wp-admin/admin-ajax.php',
                    data: {
                        action: 'stm_woo_callback',
                        descriptions: desc,
                        item_name: item_name,
                        calcTotals: vm.formula,
                        woo_info: params[0].wooCommerce,
                    },
                    success: function (data) {
                        if (data.status) {
                            data.page = data.page ? data.page : '';
                            vm.isActive = false;
                            window.location.href = data.page;
                        }
                    }
                });
            },

            paymentMethodCallback: function (data, item_name) {
                let vm = this;
                if (vm.paymentMethod.length > 0) {
                    vm.payment.status = '';
                    if (vm.paymentMethod === 'paypal') {

                        vm.payment.status = 'success';
                        vm.payment.message = 'Redirecting to PayPal';

                        vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/ccb-paypal/', {
                            item_name: item_name,
                            calcTotals: vm.formula,
                            paypal_info: data.paypal,
                        }).then(function (data) {
                            if (data.body.success) {
                                window.location.replace(data.body.url);
                            }
                        });
                    } else if (vm.paymentMethod === 'stripe') {
                        vm.stripe.stripe.createToken(vm.stripe.stripeCard).then(function (result) {
                            if (result.error) {
                                vm.payment.message = result.error.message;
                                vm.payment.status = result.error;
                            } else {
                                data = data[0];
                                vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/ccb-stripe/', {
                                    item_name: item_name,
                                    calcTotals: vm.formula,
                                    stripe_info: data.stripe,
                                    token_id: result.token.id,
                                }).then(function (data) {
                                    vm.payment.status = data.body.status;
                                    vm.payment.message = data.body.message;
                                    setTimeout(function () {
                                        if (data.body.reload) document.location.reload(true);
                                    }, 1500);
                                });
                            }
                        })
                    }
                } else {
                    vm.payment.status = 'danger';
                    vm.payment.message = 'No payment method selected';
                }
            },

            saveCalc: function () {
                let vm = this;
                let temp = vm.checkTotalFields();
                let id = vm.projectId ? vm.projectId : null;

                vm.extraInfo = [];
                vm.extraInfo.push(id);
                vm.extraInfo.push(vm.projectNameMain);
                vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/save-settings/', {
                    formula: temp,
                    settings: vm.settings,
                    extraInfo: vm.extraInfo,
                    readyFields: vm.readyFields,
                }).then(function () {
                    vm.stmCloseModal('.ccb-create-calc', 1500);
                    setTimeout(function () {
                        window.location.replace(vm.location_url + '/wp-admin/admin.php?page=cost_calculator_builder&action=listing');
                    }, 1000)
                });
            },

            checkTotalFields: function () {
                let _formula = '',
                    vm = this,
                    temp = [];
                vm.formula = [];
                vm.readyFields.forEach(function (element) {
                    if (element.type === 'Total') {
                        vm.formula.push({
                            label: element.label,
                            formula: element.costCalcFormula,
                            symbol: element.symbol
                        });
                    }
                });

                if (!vm.formula.length) {
                    vm.readyFields.forEach(function (element) {
                        if (element.alias) {
                            _formula += element.alias + ' + ';
                        }
                    });

                    let last = _formula.lastIndexOf(" ") - 1;
                    _formula = _formula.substring(0, last);
                    temp = [{label: 'Total', formula: _formula, symbol: ''}];

                } else {
                    temp = vm.formula;
                }
                return temp;
            },

            removeFields: function (id, order_id) {
                let vm = this;
                vm.formula.splice(id, 1);
                vm.readyFields.splice(id, 1);
                if (order_id) {
                    vm.calcTotal.splice(order_id - 1, 1);
                }
            },

            closeModal: function () {
                let vm = this;
                vm.fieldId = null;
                vm.modalData = null;
                vm.modalField = false;
                vm.stmDisplay = false;
            },

            getField: function (type) {
                let vm = this;
                vm.modalField = type;
                vm.stmDisplay = true;
            },

            checkSettings: function (type, id) {
                let vm = this;
                vm.fieldId = id;
                vm.modalField = type;
                vm.stmDisplay = true;
            },

            showContainer: function () {
                let vm = this;
                if (vm.projectName.length) {
                    vm.access = true;
                    vm.hasAccess = true;
                    vm.projectNameMain = vm.projectName;
                    vm.showTools = true;
                    vm.navZIndex = false;
                    vm.stmMainContainer = false;
                    vm.stmCloseModal('.ccb-create-msg');
                } else {
                    vm.stmCloseModal('.ccb-error-msg');
                }
                vm.projectName = '';

            },

            setSettings: function (field, id) {
                let vm = this;
                vm.closeModal();
                vm.settingsId = id;
                vm.settings[id] = field;
            },

            nextButtonCallback: function (forms) {

                String.prototype.replaceAll = function (search, replace) {
                    return this.split(search).join(replace);
                };

                let i = 0;
                let vm = this;
                let totalFound = '';
                let text = forms[0].formFields.body;

                vm.nextButton = false;

                if (vm.formula.length > 0) {
                    vm.formula.forEach(function (element, index) {
                        if (text.indexOf('[ccb-total-' + index + ']') !== -1) {
                            let regex = '[ccb-total-' + index + ']';
                            text = text.replaceAll(regex, element.total);
                        }
                    });
                }

                let replacement = '[ccb-total-';
                while (text.indexOf(replacement) !== -1) {
                    totalFound = replacement + i + ']';
                    let position = text.indexOf(totalFound);
                    if (position !== -1) {
                        text = text.replace(text.substr(position, totalFound.length), 0);
                    } else {
                        i++;
                    }
                }

                if (document.querySelector('.ccb-contact-form7')) {
                    document.querySelectorAll('.ccb-contact-form7')[vm.iteration].querySelector('textarea').value = text;
                }
            },

            editField: function (key) {
                let vm = this;
                let gotFieldByKey = vm.readyFields[key];

                let type = vm.readyFields[key].type.toLowerCase().replace(' ', '-');
                vm.fieldId = key;
                vm.modalField = type;
                vm.modalData = gotFieldByKey;

                vm.stmDisplay = true;
            },

            editItem: function () {
                let vm = this;
                if (vm.checkURI('action') === 'edit' && vm.checkURI('id')) {
                    let id = vm.checkURI('id');
                    vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/edit-listing/', {id: id}).then(function (response) {
                        let data = response.body.message.field;
                        if (response.body.success) {
                            vm.access = true;
                            vm.create = false;
                            vm.showTools = true;
                            vm.hasAccess = true;
                            vm.navZIndex = false;
                            vm.projectId = data.id;
                            vm.stmMainContainer = false;
                            vm.readyFields = data.field;
                            vm.projectName = data.project_name;
                            vm.projectNameMain = data.project_name;
                            vm.formula = response.body.message.formula;

                            if (data.settings.length) {
                                vm.settingsId = 0;
                                vm.settings = data.settings;
                            }
                        }
                    });
                } else if (vm.checkURI('action') === 'listing') {
                    vm.create = true;
                    document.querySelector('#ccb-create-calc').removeAttribute('checked');
                    document.querySelector('#ccb-calc-list').setAttribute('checked', 'checked');
                }
            },

            saveField: function (data, id = null, index = null) {
                let vm = this;
                if (id === null) {
                    if (index || index === 0) {
                        let len = vm.readyFields.length + 1;
                        let current = vm.readyFields[index];
                        for (let i = index; i < len; i++) {
                            let next = vm.readyFields[i + 1];
                            vm.readyFields[i + 1] = current;
                            current = next;
                        }
                        vm.readyFields[index] = data;
                        vm.checkAvailable();
                    } else {
                        vm.readyFields.push(data);
                    }
                } else {
                    vm.readyFields[id] = data;
                }
                vm.newIndex = null;
                vm.closeModal();
            },

            setOption: function () {
                let vm = this;
                vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/get-styles/').then(function (response) {
                    if (response.body.success) {
                        vm.load = true;
                        vm.styles = response.body.message;
                    } else {
                        vm.load = true;
                        vm.styles = store.getters.getStyles;
                    }
                });
            },

            stmCalendarField: function (index, total, id, alias, value = null) {

                let vm = this;
                let time = null;

                if (value.length) {
                    time = value[1] - value[0];
                    vm.dateDescription[index] = {
                        alias: alias,
                        value: value[0].toLocaleDateString() + ' - ' + value[1].toLocaleDateString()
                    };
                } else {
                    let date = new Date(value);
                    time = (date - Date.now());
                    vm.dateDescription[index] = {alias: alias, value: date.toLocaleDateString()};
                }

                time = time > 0 ? Math.round(time / 1000 / 3600 / 24) : 0;
                time = typeof time === 'number' ? time : 0;

                vm.stmToCalculate(time, total, id, alias);
            },

            stmCancel: function () {
                let vm = this;
                window.location.replace(vm.location_url+ '/wp-admin/admin.php?page=cost_calculator_builder&action=listing');
            },

            stmAutoCalc: function(){
                let vm = this;
                if(typeof stmCalculate !== 'undefined' && stmCalculate.hasOwnProperty('id')) {
                    let currentId = stmCalculate.id;
                    vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/auto-calc/', {
                        id: currentId,
                    }).then(function (response) {
                        if(response.body.success){
                            let fields = response.body.message;
                            let formula = fields['stm-formula'];
                            if(fields['stm-fields']){
                                fields['stm-fields'].forEach(function (elem, index) {
                                    let unit = elem.hasOwnProperty('unit') ? elem.unit : 1;
                                    if(elem.alias && elem.hasOwnProperty('default') && elem.type !== 'Date Picker'){
                                        vm.stmToCalculate(elem.default, formula, index, elem.alias, unit);
                                    }
                                });
                            }
                        }
                    });
                }
            },

            stmToCalculate(value, mainFormula, id, alias, unit = 1) {
                let vm = this;
                vm.nextButton = true;

                let _formula = JSON.parse(JSON.stringify(mainFormula));
                if (typeof value === 'string' || typeof value === 'number') {
                    value = parseFloat(value);
                    vm.calcTotal[id] = {value: value ? value * unit : 0, alias: alias};
                } else if (typeof value === 'object') {
                    let total = 0;
                    for (let j in value) {
                        total += parseFloat(value[j]);
                    }
                    vm.calcTotal[id] = {value: total, alias: alias};
                }

                _formula.forEach(function (item) {
                    vm.calcTotal.forEach(element => {
                        item.formula = item.formula.replace(new RegExp(element.alias, 'g'), element.value);
                    });
                });

                let replacement = ['range_field_id_', 'radio_field_id_', 'quantity_field_id_', 'dropDown_field_id_', 'checkbox_field_id_', 'datePicker_field_id_'];

                _formula.forEach(function (element) {
                    let i = 0;
                    while (element.formula.indexOf('_field_id_') !== -1) {
                        let position = element.formula.indexOf(replacement[i]);
                        if (position !== -1) {
                            element.formula = element.formula.replace(element.formula.substr(position, replacement[i].length + 2), 0);
                        } else {
                            i++;
                        }
                    }
                });

                vm.formula = [];
                _formula.forEach(function (element) {
                    let summary = eval(element.formula);
                    summary = summary !== summary || !isFinite(summary) ? 0 : summary;
                    vm.formula.push({
                        label: element.label,
                        total: summary.toFixed(2)

                    });
                });
            },

            removeFromListing: function (id, index) {
                let vm = this;
                vm.removeId = id;
                vm.removeArrayId = index;
            },

            removeItem: function () {
                let vm = this;
                vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/remove-existing/', {
                    id: vm.removeId,
                }).then(function (response) {
                    if (response.body.success) {
                        vm.listings.splice(vm.removeArrayId, 1);
                        vm.stmCloseModal('.ccb-delete-calc');
                    }
                });
            },

            changeStyles: function () {
                let vm = this;
                vm.styles.labelStyles.fontSize = parseInt(vm.styles.labelStyles.fontSize) + 'px';
                vm.styles.containerStyles.boxShadow = vm.styles.containerStyles.boxShadow.replace(/inset/gi, ' ');
                vm.styles.containerStyles.boxShadow = vm.styles.containerStyles.inset + ' ' + vm.styles.containerStyles.boxShadow;

                vm.$http.post(vm.location_url + '/wp-json/cost-calc/v1/save-styles/', {
                    styles: vm.styles,
                }).then(function () {
                    vm.stmCloseModal('.ccb-changes-saved');
                });
            },

            stmCloseModal: function (selector, speed = 2500, needle = 'ccb-msg-active') {
                $(selector).addClass(needle);
                $({to: 0}).animate({to: 1}, speed, function () {
                    $(selector).removeClass(needle);
                });
            },

            changePosition: function (event) {
                let selector = '.ccb-side-bar-tools',
                    hide = 'ccb-tools-hide',
                    posSelector = 'ccb-li-position-relative';

                let labelAttrFor = event.target.getAttribute('for');
                let liSelector = $('#' + labelAttrFor).attr('class');

                let stmArr = liSelector.split(' ');

                $('.' + posSelector).removeClass(posSelector);
                $('.' + hide).removeClass(hide);
                $('li.' + stmArr[1]).addClass(posSelector);

                if (labelAttrFor === 'ccb-create-calc') {
                    $(selector).removeClass(hide);
                } else {
                    $(selector).addClass(hide);
                }
            },
        },

        updated() {
            let vm = this;
            vm.order = vm.readyFields.length;
        },

        mounted: function () {
            let vm = this;
            vm.preloader = false;
        }
    });

    iteration++;
});
Vue.component('cost-html', {
    props: ['args'],
    data: function(){
        return {
            classes: '',
            htmlContent: '',
        }
    },

    created(){

        if(this.args){
            this.classes = this.args.additionalStyles;
            this.htmlContent = this.args.htmlContent ? this.args.htmlContent : '';
        }
    },
    template: `
                <fieldset v-html="htmlContent" :class="classes"></fieldset>
             `,
});
Vue.component('cost-line', {
    props: ['args'],

    data: function () {
        return {
            styles: {
                width: '',
                classes: '',
                borderWidth: '',
                borderStyle: '',
            }
        }
    },

    created() {
        if (this.args) {
            this.classes = this.args.additionalStyles;
            this.styles.width = this.args.len ? this.args.len : 0;
            this.styles.borderWidth = this.args.size ? this.args.size : 0;
            this.styles.borderStyle = this.args.style ? this.args.style : 0;
        }
    },

    template: `
                <hr class="ccb-hr" :class="classes" :style="styles">
              `,
});
Vue.component('cost-range', {
    props: ['args', 'id', 'labels', 'total'],
    data: function () {
        return {
            unit: 1,
            label: '',
            alias: '',
            step : null,
            classes: '',
            rangeValue: 0,
            minValue: null,
            maxValue: null,
        }
    },

    created() {
        let vm = this;
        if (vm.args) {
            vm.alias = vm.args.alias;
            vm.classes = vm.args.additionalStyles;
            vm.label = vm.args.label ? vm.args.label : '';
            vm.unit = parseInt(vm.args.unit) ? parseInt(vm.args.unit) : 1;
            vm.step = parseInt(vm.args.step) ? parseInt(vm.args.step) : 100;
            vm.minValue = parseInt(vm.args.minValue) ? parseInt(vm.args.minValue) : 0;
            vm.maxValue = parseInt(vm.args.maxValue) ? parseInt(vm.args.maxValue) : 100;

            let defVal = parseFloat(vm.args.default) ? parseFloat(vm.args.default) : vm.rangeValue;
            defVal = defVal > vm.maxValue || defVal < vm.minValue ? vm.rangeValue : defVal;
            vm.rangeValue = defVal;
        }
    },

    template: `
                  <div class="ccb-inner-wrapper">
                        <label :style="labels" for="">{{label}}</label>
                        <div class="ccb-range-slider">
                            <input class="ccb-range-slider__range" type="range" value="0" :class="classes" :min="minValue" :step="step" :max="maxValue" v-model="rangeValue" v-on:change="$emit(\'change\', rangeValue, total, id, alias, unit)">
                            <span class="ccb-range-slider__value">{{rangeValue}}</span>
                        </div>
                  </div>
              `,
});
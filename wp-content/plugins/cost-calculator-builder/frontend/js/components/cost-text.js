Vue.component('cost-text', {
    props: ['args', 'id', 'labels'],
    data: function(){
        return {
            label: '',
            classes: '',
            placeholder: '',
            id_for_label: 'text_area',
        }
    },

    created(){
        if(this.args){
            this.classes = this.args.additionalStyles;
            this.id_for_label = this.id_for_label + this.id;
            this.label = this.args.label ? this.args.label : '';
        }
    },

    template: `
                <div class="ccb-inner-wrapper">
                    <label :style="labels" :class="classes" :for="id_for_label">{{label}}:</label>
                    <textarea :id="id_for_label" :placeholder="placeholder"></textarea>
                </div>
              `,
});
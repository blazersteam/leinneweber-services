Vue.component('stm-show-preview', {
    props: ['field', 'styles', 'total', 'id'],

    data: function(){
        return {
            value: null,
            range: false,
        }
    },

    created(){
        this.range = this.field.range && Boolean(parseInt(this.field.range)) ? true : this.range;
    },

    methods: {
        stmCallback: function (value, total, id, alias, unit) {
            this.$emit('stm-callback', value, total, id, alias, unit);
        },
    },
    template: `
                <template v-if="field._tag === 'cost-quantity'">
                    <cost-quantity 
                        :id="id"
                        :args="field"
                        :total="total"
                        v-on:keyup="stmCallback"
                        :fields="styles ? styles.fieldStyles : {}" 
                        :labels="styles ? styles.labelStyles : {}"
                        >
                    </cost-quantity>
                </template>
                
                <template v-else-if="field._tag === 'cost-range'">
                    <cost-range 
                        :id="id"
                        :args="field"
                        :total="total"
                        v-on:change="stmCallback"
                        :fields="styles ? styles.fieldStyles : {}" 
                        :labels="styles ? styles.labelStyles : {}"
                        >
                    </cost-range>
                </template>
                
                <template v-else-if="field._tag === 'cost-drop-down'">
                    <cost-drop-down 
                        :id="id"
                        :args="field"
                        :total="total"
                        v-on:change="stmCallback"
                        :fields="styles ? styles.fieldStyles : {}" 
                        :labels="styles ? styles.labelStyles : {}"
                        >
                    </cost-drop-down>
                </template>
                
                <template v-else-if="field._tag === 'cost-checkbox'">
                    <cost-checkbox 
                        :id="id"
                        :args="field"
                        :total="total"
                        v-on:change="stmCallback"
                        :fields="styles ? styles.fieldStyles : {}" 
                        :labels="styles ? styles.labelStyles : {}"
                        >
                    </cost-checkbox>
                </template>
                
                <template v-else-if="field._tag === 'cost-radio'">
                    <cost-radio 
                        :id="id"
                        :args="field"
                        :total="total"
                        v-on:change="stmCallback"
                        :fields="styles ? styles.fieldStyles : {}" 
                        :labels="styles ? styles.labelStyles : {}"
                        >
                    </cost-radio>
                </template>
                
                <template v-else-if="field._tag === 'cost-input'">
                    <cost-input 
                        :id="id"
                        :args="field"
                        :fields="styles ? styles.fieldStyles : {}" 
                        :labels="styles ? styles.labelStyles : {}"
                        >
                    </cost-input>
                </template>
                
                <template v-else-if="field._tag === 'cost-html'">
                    <cost-html :args="field"></cost-html>
                </template>
                                
                <template v-else-if="field._tag === 'date-picker'">
                    <div class="ccb-inner-wrapper">
                        <label>{{field.label}}</label>
                        <div class="ccb-date-picker">
                            <date-picker
                                :range="range"
                                confirm lang='en'
                                :clearable='false'
                                format='DD/MM/YYYY'
                                :first-day-of-week='1'
                                v-model="value"
                                v-on:change="$emit('calendar-event', field._id, total, id, field.alias, value)"
                            >
                            </date-picker>
                        </div>
                    </div>
                </template>
                
                <template v-else-if="field._tag === 'cost-line'">
                    <cost-line :args="field"></cost-line>
                </template>
                
                <template v-else-if="field._tag === 'cost-text'">
                    <cost-text :labels="styles ? styles.labelStyles : {}" :id="id" :args="field"></cost-text>
                </template>
              `,
});
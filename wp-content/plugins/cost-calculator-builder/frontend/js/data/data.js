export default [
    {id: 1, type: 'html', icon: 'fab fa-html5', _name: 'Html', description: 'html elements'},
    {id: 2, type: 'input', icon: 'fa fa-edit', _name: 'Input', description: 'input fields'},
    {id: 3, type: 'drop-down', icon: 'fas fa-chevron-down', _name: 'Drop Down', description: 'drop-down fields'},
    {id: 4, type: 'radio-button', icon: 'fa-dot-circle', _name: 'Radio button', description: 'html radio buttons'},
    {id: 5, type: 'checkbox', icon: 'fas fa-check-circle', _name: 'Checkbox', description: 'checkbox fields'},
    {id: 6, type: 'range-button', icon: 'far fa-exchange', _name: 'Range button', description: 'range buttons'},
    {id: 7, type: 'quantity', icon: 'fas fa-calculator', _name: 'Quantity', description: 'quantity filed'},
    {id: 8, type: 'text-area', icon: 'far fa-pencil', _name: 'Text', description: 'text field'},
    {id: 9, type: 'line', icon: 'fas fa-ellipsis-h', _name: 'Line', description: 'horizontal rule'},
    {id: 10, type: 'date-picker', icon: 'fa-calendar-week', _name: 'Date Picker', description: 'date-picker field'},
    {id: 11, type: 'total', icon: 'fa-money-bill-alt', _name: 'Total', description: 'total field'},
]
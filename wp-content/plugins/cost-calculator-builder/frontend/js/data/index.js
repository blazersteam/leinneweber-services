import Vuex from '../libs/vuex'
import formFields from "./data";
import styleData from "./styleData";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        formFields,
        styleData,
        hasAccess: false,
    },
    getters: {
        getFields: state => {
            return state.formFields;
        },

        getStyles: state => {
            return state.styleData;
        },
    },
});
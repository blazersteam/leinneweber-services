Vue.component('checkbox-field', {
    props: ['field', 'id', 'order', 'index', 'default'],
    data: function () {
        return {
            btnMsg: 'Add',
            checkboxField: this.resetValue(),
        }
    },

    created() {
        if (this.id === null) {
            this.checkboxField = this.resetValue();
            this.checkboxField._id = this.order;
            this.checkboxField.alias = this.checkboxField.alias + this.checkboxField._id;
        } else {
            this.btnMsg = 'Save';
            this.checkboxField = this.field;
        }
    },

    methods: {
        resetValue: function () {
            return {
                _id: null,
                label: '',
                default: '',
                _event: 'change',
                type: 'Checkbox',
                additionalStyles: '',
                _tag: 'cost-checkbox',
                icon: 'fas fa-check-circle',
                alias: 'checkbox_field_id_',
                options: [
                    {
                        optionText: '',
                        optionValue: '',
                    }
                ],
            };
        },
        addOption: function () {
            this.checkboxField.options.push({optionText: '', optionValue: '',});
        },
    },

    template: `<div class="ccb-modal-view">
                    <div class="ccb-modal-row">
                        <h4 class="ccb-title">
                            <i v-bind:class="checkboxField.icon"></i>
                            {{checkboxField.type}}
                        </h4>
                        <div class="ccb-input-group ccb-input-group-icon">
                            <input type="text" placeholder="Label" v-model="checkboxField.label" />
                            <div class="ccb-input-icon"><i class="far fa-quote-right"></i></div>
                        </div>
                    </div>
                    <div class="ccb-input-group ccb-input-group-icon" v-if="checkboxField.hasOwnProperty('default')">
                        <select v-model="checkboxField.default">
                            <option value="">Select a default value</option>
                            <option v-for="(value, index) in checkboxField.options" :key="index" :value="value.optionValue">{{value.optionText}}</option>
                        </select>
                        <div class="ccb-input-icon"><i class="fas fa-list"></i></div>
                    </div>
                    <div class="ccb-modal-row">
                        <div id="ccb-add-option" class="ccb-col-half">
                            <h4>Add Checkbox</h4>
                            <div id="ccb-add-option-btn" class="ccb-add-form" v-on:click="addOption">
                                <i class="fa fa-plus plus"></i>
                            </div>
                        </div>
                    </div>
                   
                    <div class="ccb-options">
                        <div class="ccb-item">
                            <template v-for="(option, index) in checkboxField.options">
                                <div class="ccb-col-half">
                                   <div class="ccb-input-group ccb-input-group-icon">
                                        <input type="text" placeholder="Checkbox Label"
                                           v-model="option.optionText"/>
                                        <div class="ccb-input-icon"><i class="far fa-bookmark"></i></div>
                                    </div>
                                </div>
                                <div class="ccb-col-half">
                                    <div class="ccb-input-group ccb-input-group-icon">
                                        <input type="text" placeholder="Checkbox Value"
                                               v-model="option.optionValue"/>
                                        <div class="ccb-input-icon"><i class="fas fa-calculator"></i></i></div>
                                    </div>
                                </div>
                           
                                <div class="ccb-col-half delete-option">
                                    <button class="ccb-delete-option" v-on:click.prevent="checkboxField.options.splice(index, 1)"><i class="fa fa-trash"></i></button>
                                </div>
                            </template>    
                        </div>
                    </div>
                    <div class="ccb-additional-classes">
                        <div class="ccb-modal-row">
                            <h4 class="ccb-title">
                              <i class="fab fa-css3"></i>
                                Additional classes
                            </h4>
                        </div>
                        <textarea v-model="checkboxField.additionalStyles"></textarea>
                    </div>
                   <button v-on:click="$emit(\'click\', checkboxField, id, index)" class="ccb-submit-button">{{btnMsg}}</button>
              </div>`,
});

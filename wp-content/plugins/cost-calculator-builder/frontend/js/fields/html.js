import ClassicEditor from '../libs/ckeditor';

Vue.component('html-field', {
    props: ['field', 'id', 'order', 'index'],

    data: function () {
        return {
            btnMsg: 'Add',
            htmlField: this.resetValue(),
        }
    },

    methods: {
        resetValue: function () {
            return {
                _id: null,
                _event: '',
                type: 'Html',
                htmlContent: '',
                placeholder: '',
                _tag: 'cost-html',
                additionalStyles: '',
                icon: 'fab fa-html5',
            }
        },

        saveItems: function () {
            let vm = this;
            vm.htmlField.htmlContent = document.querySelector('.ck-editor__editable').innerHTML;
            vm.$emit('click', vm.htmlField, vm.id, this.index);
        },
    },

    created() {
        if (this.id === null) {
            this.htmlField = this.resetValue();
            this.htmlField._id = this.order;
        } else {
            this.btnMsg = 'Save';
            this.htmlField = this.field;
        }
    },

    mounted() {
        setTimeout(function () {
            ClassicEditor
                .create( document.querySelector( '#editor' ) )
                .then( editor => {
                    window.editor = editor;
                } )
                .catch( err => {
                    console.error( err.stack );
                } );
        }, 100);

    },

    template: `
                 <div class="ccb-modal-view" >
                    <div class="ccb-modal-row">
                        <h4 class="ccb-title">
                            <i v-bind:class="htmlField.icon"></i>
                            {{htmlField.type}}
                        </h4>
                    </div>
                    <div>
                         <textarea name="content" id="editor">
                           {{ htmlField.htmlContent }}
                        </textarea>
                    </div>
                    <div class="ccb-additional-classes">
                        <div class="ccb-modal-row">
                            <h4 class="ccb-title">
                              <i class="fab fa-css3"></i>
                                Additional classes
                            </h4>
                        </div>
                        <textarea v-model="htmlField.additionalStyles"></textarea>
                    </div>
                    <button v-on:click.prevent="saveItems" class="ccb-submit-button">{{btnMsg}}</button>
                 </div>
              `,
});
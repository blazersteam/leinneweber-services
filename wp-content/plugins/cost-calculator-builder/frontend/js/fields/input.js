Vue.component('input-field', {
    props: ['field', 'id', 'order', 'index'],
    data: function () {
        return {
            btnMsg: 'Add',
            inputField: this.resetValue(),
        }
    },

    created() {
        if (this.id === null) {
            this.inputField = this.resetValue();
            this.inputField._id = this.order;
        } else {
            this.btnMsg = 'Save';
            this.inputField = this.field;
        }
    },

    methods: {
        resetValue: function () {
            return {
                _id: null,
                label: '',
                inputType: '',
                type: 'Input',
                placeholder: '',
                _event: '',
                _tag: 'cost-input',
                icon: 'fa fa-edit',
                additionalStyles: '',
            };
        },
    },

    template: ` <div class="ccb-modal-view">
                    <div class="ccb-modal-row">
                        <h4 class="ccb-title">
                            <i v-bind:class="inputField.icon"></i>
                            {{inputField.type}}
                        </h4>
                        <div class="ccb-input-group ccb-input-group-icon">
                            <input type="text" placeholder="Label" v-model="inputField.label" />
                            <div class="ccb-input-icon"><i class="far fa-quote-right"></i></div>
                        </div>
                        <div class="ccb-input-group ccb-input-group-icon">
                            <input type="text" placeholder="Placeholder"
                                   v-model="inputField.placeholder" />
                            <div class="ccb-input-icon"><i class="fas fa-ellipsis-h"></i></div>
                        </div>
                        <div class="ccb-input-group ccb-input-group-icon">
                            <select v-model="inputField.inputType" >
                                <option value="" selected>Type</option>
                                <option value="text">text</option>
                                <option value="number">number</option>
                                <option value="password">password</option>
                                <option value="email">email</option>
                            </select>
                            <div class="ccb-input-icon"><i class="fas fa-list"></i></div>
                        </div>
                    </div>
                    <div class="ccb-additional-classes">
                        <div class="ccb-modal-row">
                            <h4 class="ccb-title">
                              <i class="fab fa-css3"></i>
                                Additional classes
                            </h4>
                        </div>
                        <textarea v-model="inputField.additionalStyles"></textarea>
                    </div>
                    <button v-on:click="$emit(\'click\', inputField, id, index)" class="ccb-submit-button">{{btnMsg}}</button>
                </div>`,
});

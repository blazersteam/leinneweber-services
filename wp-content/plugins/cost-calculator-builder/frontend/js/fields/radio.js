Vue.component('radio-button', {
    props: ['field', 'id', 'order', 'index'],
    data: function () {
        return {
            btnMsg: 'Add',
            radioField: this.resetValue(),
        }
    },

    created() {
        if (this.id === null) {
            this.radioField = this.resetValue();
            this.radioField._id = this.order;
            this.radioField.alias = this.radioField.alias + this.radioField._id;
        } else {
            this.btnMsg = 'Save';
            this.radioField = this.field;
        }
    },

    methods: {
        resetValue: function () {
            return {
                label: '',
                _id: null,
                default: '',
                onValue: null,
                _event: 'change',
                _tag: 'cost-radio',
                additionalStyles: '',
                type: 'Radio Button',
                icon: 'fa-dot-circle',
                alias: 'radio_field_id_',
                options: [
                    {
                        optionText: '',
                        optionValue: '',
                    }
                ],
            };
        },

        addOption: function () {
            this.radioField.options.push({optionText: '', optionValue: '',});
        },
    },

    template: `  <div class="ccb-modal-view" data-id="3">
                    <div class="ccb-modal-row">
                        <h4 class="ccb-title">
                            <i v-bind:class="radioField.icon"></i>
                            {{radioField.type}}
                        </h4>
                        <div class="ccb-input-group ccb-input-group-icon">
                            <input type="text" placeholder="Label" v-model="radioField.label"/>
                            <div class="ccb-input-icon"><i class="far fa-quote-right"></i></div>
                        </div>
                    </div>
                    
                    <div class="ccb-input-group ccb-input-group-icon" v-if="radioField.hasOwnProperty('default')">
                        <select v-model="radioField.default">
                            <option value="">Select a default value</option>
                            <option v-for="(value, index) in radioField.options" :key="index" :value="value.optionValue">{{value.optionText}}</option>
                        </select>
                        <div class="ccb-input-icon"><i class="fas fa-list"></i></div>
                    </div>
                    
                    <div class="ccb-modal-row">
                        <div id="ccb-add-option" class="ccb-col-half">
                            <h4>Add Radio</h4>
                            <div id="ccb-add-option-btn" class="ccb-add-form" v-on:click="addOption">
                                <i class="fa fa-plus plus"></i>
                            </div>
                        </div>
                    </div>
                
                    <div class="ccb-options">
                        <div class="ccb-item">
                            <template v-for="(option, index) in radioField.options">
                                <div class="ccb-col-half">
                                    <div class="ccb-input-group ccb-input-group-icon">
                                        <input type="text" placeholder="Radio Button Label"
                                               v-model="option.optionText"/>
                                        <div class="ccb-input-icon"><i class="far fa-bookmark"></i></div>
                                    </div>
                                </div>
                                <div class="ccb-col-half">
                                    <div class="ccb-input-group ccb-input-group-icon">
                                        <input type="text" placeholder="Radio Button Value"
                                               v-model="option.optionValue"/>
                                        <div class="ccb-input-icon"><i class="fas fa-calculator"></i></div>
                                    </div>
                                </div>
                                <div class="ccb-col-half delete-option">
                                    <button class="ccb-delete-option" v-on:click.prevent="radioField.options.splice(index, 1)"><i class="fa fa-trash"></i></button>
                                </div>
                            </template>    
                        </div>
                    </div>
                    <div class="ccb-additional-classes">
                        <div class="ccb-modal-row">
                            <h4 class="ccb-title">
                              <i class="fab fa-css3"></i>
                                Additional classes
                            </h4>
                        </div>
                        <textarea v-model="radioField.additionalStyles"></textarea>
                    </div>
                    <button v-on:click="$emit(\'click\', radioField, id, index)" class="ccb-submit-button">{{btnMsg}}</button>
                </div>`,
});

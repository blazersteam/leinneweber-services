=== Cost Calculator Builder ===

Contributors: Stylemix
Donate link: https://stylemixthemes.com
Tags: cost calculator, calculator, calculator form builder
Requires at least: 4.6
Tested up to: 5.2
Stable tag: 1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

WP Cost Calculator – if you’re searching for a smart, simple tool that allows you to easily create price estimation forms to give your clients the opportunity to get information about your services and products costs you are in the right place.

WP Cost Calculator helps you to build any type of estimation forms on a few easy steps. The plugin offers its own calculation builder. Moreover, you can create an unlimited number of calculation forms and insert them on any page or post.

Our plugin is built super easy for the visitors of your WordPress website, Cost Calculator is very suitable if services or products are variable, and there are many options to estimate the cost. Do you need to make a price calculator to know your project cost? – this tool is for you, Loan Calculator? – just install our plugin. Table Order? - Easy. The possibilities are unlimited. Let the WP Cost Calculator become your virtual assistant and increase the conversion of your website.

8 Different Calculator Elements:
○      Input Box
○      Dropdown Box
○      Switch Box
○      Checkbox
○      Range Slider
○      Quantity Box
○      Textbox
○      Horizontal Line

[Documentation](https://support.stylemixthemes.com/manuals/plugins/CCB_Manual.pdf)

https://www.youtube.com/watch?v=SFLZczK-yGY

You can give a value to any element of the form and it will automatically affect the total price.

Also, you can assign your own calculation formula with the ability to add, subtract, multiply and divide elements of the form. Integrate your forms in your pages using shortcodes.

Don’t worry about customization, Freely customize colors, fonts, background color, etc.

This plugin suit to sell highly customizable products or services or provide mathematical help for your site visitors:
- Car Rental Agencies
- Graphic & Web Designers
- Retail & Small Business
- Medical Services
- SEO Agencies
- Printing Business
- Hosting Companies
- Delivery Services
- Venue Rentals
- Much More

== Installation ==
This section describes how to install the plugin and get it working.

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Please find more details on Plugin Installation in documentation
4. Set Up Page in Menu -> Calculator Builder.

Also, you can import the demo sample using Tools -> Import -> WordPress Import.
Demo sample XML file included in plugin archive.

== Changelog ==

= 1.0.5 =
* Default Value field included to all Elements.

= 1.0.4 =
* Fixed problem with subdomain/subfolder websites.

= 1.0.3 =
* Minor bug fixes.

= 1.0.2 =
* Refactoring.

= 1.0.1 =
* Minor bug fixes.

= 1.0.0 =
* First Version of Plugin.

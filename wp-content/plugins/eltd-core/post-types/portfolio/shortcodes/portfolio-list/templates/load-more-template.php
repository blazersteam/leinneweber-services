<?php if($query_results->max_num_pages > 1) { ?>
    <div class="eltd-ptf-list-paging">
		<span class="eltd-ptf-list-load-more">
			<?php if(eltd_core_theme_installed()) : ?>
                <?php
                echo malmo_elated_get_button_html(array(
                    'link'       => 'javascript: void(0)',
                    'text'       => esc_html__('Load More', 'eltd_core'),
                    'type'       => 'black',
                    'hover_type' => 'solid',
                    'size'       => 'medium'
                ));
                ?>
            <?php else: ?>
                <a href="javascript: void(0)"><?php esc_html_e('Load More', 'eltd_core'); ?></a>
            <?php endif; ?>
		</span>
    </div>
<?php }
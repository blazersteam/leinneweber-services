<?php

if(!function_exists('malmo_elated_remove_auto_ptag')) {
	function malmo_elated_remove_auto_ptag($content, $autop = false) {
        if($autop) {
            $content = preg_replace('#^<\/p>|<p>$#', '', $content);
        }

        return do_shortcode($content);
	}
}
<?php
if(!defined('ABSPATH')) {
    exit;
}

class ElatedInstagramWidget extends WP_Widget {

    protected $params;

    public function __construct() {
        parent::__construct(
            'eltd_instagram_widget',
            'Elated Instagram Widget',
            array('description' => __('Display instagram images', 'malmo'))
        );

        $this->setParams();
    }

    protected function setParams() {
        $this->params = array(
            array(
                'name'  => 'title',
                'type'  => 'textfield',
                'title' => esc_html__('Title', 'malmo')
            ),
            array(
                'name'  => 'tag',
                'type'  => 'textfield',
                'title' => esc_html__('Tag', 'malmo')
            ),
            array(
                'name'  => 'number_of_photos',
                'type'  => 'textfield',
                'title' => esc_html__('Number of photos', 'malmo')
            ),
            array(
                'name'    => 'number_of_cols',
                'type'    => 'dropdown',
                'title'   => esc_html__('Number of columns', 'malmo'),
                'options' => array(
                    '2' => esc_html__('Two', 'malmo'),
                    '3' => esc_html__('Three', 'malmo'),
                    '4' => esc_html__('Four', 'malmo'),
                    '6' => esc_html__('Six', 'malmo'),
                    '7' => esc_html__('Seven', 'malmo'),
                    '9' => esc_html__('Nine', 'malmo'),
                )
            ),
            array(
                'name'    => 'image_size',
                'type'    => 'dropdown',
                'title'   => esc_html__('Image Size', 'malmo'),
                'options' => array(
                    'thumbnail'           => esc_html__('Small', 'malmo'),
                    'low_resolution'      => esc_html__('Medium', 'malmo'),
                    'standard_resolution' => esc_html__('Large', 'malmo')
                )
            ),
            array(
                'name'    => 'space_between_cols',
                'type'    => 'dropdown',
                'title'   => esc_html__('Space Between Columns?', 'malmo'),
                'options' => array(
                    'yes' => esc_html__('Yes', 'malmo'),
                    'no'  => esc_html__('No', 'malmo')
                )
            ),
            array(
                'name'    => 'show_overlay',
                'type'    => 'dropdown',
                'title'   => esc_html__('Show Overlay?', 'malmo'),
                'options' => array(
                    'yes' => esc_html__('Yes', 'malmo'),
                    'no'  => esc_html__('No', 'malmo')
                )
            ),
            array(
                'name'  => 'transient_time',
                'type'  => 'textfield',
                'title' => esc_html__('Images Cache Time', 'malmo')
            ),
        );
    }

    public function getParams() {
        return $this->params;
    }

    public function widget($args, $instance) {
        extract($instance);

        $title              = !empty($title) ? $title : '';
        $number_of_photos   = !(empty($number_of_photos)) ? $number_of_photos : '';
        $tag                = !empty($tag) ? $tag : '';
        $transient_time     = !empty($transient_time) ? $transient_time : '';
        $number_of_cols     = !empty($number_of_cols) ? $number_of_cols : '';
        $show_overlay       = !empty($show_overlay) ? $show_overlay : '';
        $space_between_cols = !empty($space_between_cols) ? $space_between_cols : '';

		echo malmo_elated_get_module_part($args['before_widget']);
		echo malmo_elated_get_module_part($args['before_title'] . $title . $args['after_title']);

        $instagram_api = ElatedInstagramApi::getInstance();
        $images_array  = $instagram_api->getImages($number_of_photos, $tag, array(
            'use_transients' => true,
            'transient_name' => $args['widget_id'],
            'transient_time' => $transient_time
        ));

        $number_of_cols     = $number_of_cols == '' ? 3 : $number_of_cols;
        $space_between_cols = $space_between_cols == 'yes' ? '' : 'without-space';
        $show_overlay       = $show_overlay == 'yes' ? true : false;

        if(is_array($images_array) && count($images_array)) { ?>
            <ul class="eltd-instagram-feed clearfix eltd-col-<?php echo malmo_elated_get_module_part($number_of_cols); ?> <?php echo esc_attr($space_between_cols); ?>">
                <?php
                foreach($images_array as $image) { ?>
                    <li>
                        <div class="eltd-instagram-item-holder">
                            <a href="<?php echo esc_url($instagram_api->getHelper()->getImageLink($image)); ?>" target="_blank">
                                <?php if(function_exists('malmo_elated_kses_img')) : ?>
                                    <?php echo malmo_elated_kses_img($instagram_api->getHelper()->getImageHTML($image, $image_size)); ?>
                                <?php else : ?>
                                    <?php echo malmo_elated_get_module_part($instagram_api->getHelper()->getImageHTML($image, $image_size)); ?>
                                <?php endif; ?>

                                <?php if($show_overlay) : ?>

                                    <div class="eltd-instagram-overlay">
                                        <div class="eltd-instagram-overlay-inner">
                                            <div class="eltd-instagram-overlay-inner2">
                                                <span class="overlay-icon social-instagram"></span>
                                            </div>
                                        </div>
                                    </div>

                                <?php endif; ?>
                            </a>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        <?php }

		echo malmo_elated_get_module_part($args['after_widget']);
    }

    public function form($instance) {
        foreach($this->params as $param_array) {
            $param_name    = $param_array['name'];
            ${$param_name} = isset($instance[$param_name]) ? esc_attr($instance[$param_name]) : '';
        }

        //user has connected with instagram. Show form
        if(get_option('eltd_instagram_code')) {
            foreach($this->params as $param) {
                switch($param['type']) {
                    case 'textfield':
                        ?>
                        <p>
                            <label for="<?php echo esc_attr($this->get_field_id($param['name'])); ?>"><?php echo
                                esc_html($param['title']); ?></label>
                            <input class="widefat" id="<?php echo esc_attr($this->get_field_id($param['name'])); ?>" name="<?php echo esc_attr($this->get_field_name($param['name'])); ?>" type="text" value="<?php echo esc_attr(${$param['name']}); ?>"/>
                        </p>
                        <?php
                        break;
                    case 'dropdown':
                        ?>
                        <p>
                            <label for="<?php echo esc_attr($this->get_field_id($param['name'])); ?>"><?php echo
                                esc_html($param['title']); ?></label>
                            <?php if(isset($param['options']) && is_array($param['options']) && count($param['options'])) { ?>
                                <select class="widefat" name="<?php echo esc_attr($this->get_field_name($param['name'])); ?>" id="<?php echo esc_attr($this->get_field_id($param['name'])); ?>">
                                    <?php foreach($param['options'] as $param_option_key => $param_option_val) {
                                        $option_selected = '';
                                        if(${$param['name']} == $param_option_key) {
                                            $option_selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo esc_attr($option_selected); ?> value="<?php echo esc_attr($param_option_key); ?>"><?php echo esc_attr($param_option_val); ?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>
                        </p>

                        <?php
                        break;
                }
            }
        } else { ?>
            <p><?php esc_html_e('You haven\'t connected with Instagram. Please go to Elated Options -> Social page and connect with your Instagram account.', 'eltd_instagram_feed'); ?></p>
        <?php }
    }
}

function eltd_instagram_widget_load() {
    register_widget('ElatedInstagramWidget');
}

add_action('widgets_init', 'eltd_instagram_widget_load');
<?php
if(!function_exists('malmo_elated_design_styles')) {
    /**
     * Generates general custom styles
     */
    function malmo_elated_design_styles() {

        $preload_background_styles = array();

        if(malmo_elated_options()->getOptionValue('preload_pattern_image') !== "") {
            $preload_background_styles['background-image'] = 'url('.malmo_elated_options()->getOptionValue('preload_pattern_image').') !important';
        } else {
            $preload_background_styles['background-image'] = 'url('.esc_url(ELATED_ASSETS_ROOT."/img/preload_pattern.png").') !important';
        }

        echo malmo_elated_dynamic_css('.eltd-preload-background', $preload_background_styles);

        if(malmo_elated_options()->getOptionValue('google_fonts')) {
            $font_family = malmo_elated_options()->getOptionValue('google_fonts');
            if(malmo_elated_is_font_option_valid($font_family)) {
                echo malmo_elated_dynamic_css('body', array('font-family' => malmo_elated_get_font_option_val($font_family)));
            }
        }

        if(malmo_elated_options()->getOptionValue('first_color') !== "") {
            $color_selector = array(
                'a',
                'a:hover',
                'h1 a:hover',
                'h2 a:hover',
                'h3 a:hover',
                'h4 a:hover',
                'h5 a:hover',
                'h6 a:hover',
                'p a',
                'p a:hover',
                '.eltd-comment-holder .eltd-comment-text .eltd-comment-name',
                '.eltd-pagination li.active span',
                '.eltd-like.liked',
                'aside.eltd-sidebar .widget.eltd-latest-posts-widget .eltd-image-in-box .eltd-item-title:hover a',
                'aside.eltd-sidebar .widget.eltd-latest-posts-widget .eltd-minimal .eltd-item-title:hover a',
                'aside.eltd-sidebar .widget ul li a:hover',
                'aside.eltd-sidebar .widget.widget_nav_menu .current-menu-item>a',
                'aside.eltd-sidebar .widget.widget_eltd_twitter_widget .eltd-tweet-holder a:hover',
                'aside.eltd-sidebar .widget.widget_eltd_twitter_widget .eltd_tweet_time a:hover',
                'aside.eltd-sidebar .widget.widget_eltd_twitter_widget ul.eltd_twitter_widget li:after',
                '.eltd-main-menu ul li.eltd-active-item a',
                '.eltd-main-menu ul li:hover a',
                '.eltd-main-menu ul .eltd-menu-featured-icon',
                '.eltd-main-menu>ul>li.eltd-active-item>a',
                'body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu>ul>li:hover>a',
                '.eltd-drop-down .wide .second .inner ul li.sub .flexslider ul li a:hover',
                '.eltd-drop-down .wide .second ul li .flexslider ul li a:hover',
                '.eltd-drop-down .wide .second .inner ul li.sub .flexslider.widget_flexslider .menu_recent_post_text a:hover',
                '.eltd-header-vertical .eltd-vertical-dropdown-float .second .inner ul li a:hover',
                '.eltd-header-vertical .eltd-vertical-menu ul li a:hover',
                '.eltd-header-vertical .eltd-vertical-menu .second .inner ul li a:hover',
                '.eltd-mobile-header .eltd-mobile-nav a:hover',
                '.eltd-mobile-header .eltd-mobile-nav h4:hover',
                '.eltd-mobile-header .eltd-mobile-menu-opener a:hover',
                '.eltd-page-header .eltd-sticky-header .eltd-main-menu>ul>li.eltd-active-item>a:hover',
                '.eltd-page-header .eltd-sticky-header .eltd-main-menu>ul>li:hover>a',
                '.eltd-page-header .eltd-sticky-header .eltd-main-menu>ul>li>a:hover',
                '.eltd-page-header .eltd-sticky-header .eltd-search-opener:hover',
                '.eltd-page-header .eltd-sticky-header .eltd-side-menu-button-opener:hover',
                'body:not(.eltd-menu-item-first-level-bg-color) .eltd-page-header .eltd-sticky-header .eltd-main-menu>ul>li:hover>a',
                'body:not(.eltd-menu-item-first-level-bg-color) .eltd-page-header .eltd-sticky-header .eltd-main-menu>ul>li>a:hover',
                '.eltd-page-header .eltd-search-opener:hover',
                '.eltd-light-header .eltd-drop-down .second .inner ul li a:hover',
                'footer .eltd-footer-top-holder .widget.widget_eltd_twitter_widget .eltd_tweet_text a:hover',
                '.eltd-title .eltd-title-holder .eltd-title-subtitle-holder .eltd-title-icon',
                '.eltd-side-menu-button-opener:hover',
                '.eltd-side-menu a:hover',
                '.eltd-fullscreen-menu-opener .eltd-fullscreen-menu-opener-icon:hover',
                'nav.eltd-fullscreen-menu ul li a:hover',
                'nav.eltd-fullscreen-menu ul li ul li a',
                '.eltd-search-cover .eltd-search-close a:hover',
                '.eltd-search-slide-header-bottom .eltd-search-submit:hover',
                '.eltd-portfolio-single-holder .eltd-portfolio-item-content:before',
                '.eltd-portfolio-single-holder .eltd-portfolio-author-holder h6.eltd-author-position',
                '.eltd-portfolio-single-holder .eltd-portfolio-single-likes .eltd-like.liked .lnr-heart',
                '.small-images .eltd-portfolio-info-holder .eltd-portfolio-item-social:before',
                '.small-slider .eltd-portfolio-info-holder .eltd-portfolio-item-social:before',
                '.eltd-portfolio-single-holder.big-images .eltd-portfolio-item-social .eltd-portfolio-single-share-holder:hover .social_share',
                '.eltd-portfolio-single-holder.big-images .eltd-portfolio-single-likes .eltd-like.liked .lnr-heart',
                '.eltd-portfolio-single-holder.big-slider .eltd-portfolio-item-social .eltd-portfolio-single-share-holder:hover .social_share',
                '.eltd-portfolio-single-holder.big-slider .eltd-portfolio-single-likes .eltd-like.liked .lnr-heart',
                '.eltd-portfolio-single-holder.gallery .eltd-portfolio-item-social .eltd-portfolio-single-share-holder:hover .social_share',
                '.eltd-portfolio-single-holder.gallery .eltd-portfolio-single-likes .eltd-like.liked .lnr-heart',
                '.eltd-team .eltd-team-social-wrapp.light:before',
                '.eltd-team.main-info-below-image .eltd-team-text:before',
                '.eltd-team.main-info-below-image .eltd-team-social-wrapp.light:before',
                '.eltd-team.main-info-below-image .eltd-team-social-wrapp .eltd-team-social-icon:hover',
                '.countdown-period:before',
                '.eltd-message .eltd-message-inner a.eltd-close i:hover',
                '.eltd-ordered-list ol>li:before',
                '.eltd-icon-list-item .eltd-icon-list-icon-holder-inner .font_elegant',
                '.eltd-icon-list-item .eltd-icon-list-icon-holder-inner i',
                '.eltd-blog-slider-holder .eltd-bs-item-date',
                '.eltd-blog-slider-holder .eltd-bs-item-excerpt:before',
                '.eltd-blog-slider-holder .eltd-bs-item-bottom-section .eltd-bs-item-author a:hover',
                '.eltd-blog-slider-holder .eltd-bs-item-bottom-section .eltd-bs-item-categories a:hover',
                '.wpb_widgetised_column .widget.eltd-latest-posts-widget .eltd-image-in-box .eltd-item-title:hover a',
                '.wpb_widgetised_column .widget.eltd-latest-posts-widget .eltd-minimal .eltd-item-title:hover a',
                '.wpb_widgetised_column .widget ul li a:hover',
                '.wpb_widgetised_column .widget.widget_nav_menu .current-menu-item>a',
                '.wpb_widgetised_column .widget.widget_eltd_twitter_widget .eltd-tweet-holder a:hover',
                '.wpb_widgetised_column .widget.widget_eltd_twitter_widget .eltd_tweet_time a:hover',
                '.wpb_widgetised_column .widget.widget_eltd_twitter_widget ul.eltd_twitter_widget li:after',
                '.eltd-separator-with-icon-holder .eltd-icon-holder',
                '.eltd-blog-list-holder .eltd-item-info-section>div>a:hover',
                '.eltd-blog-list-holder.eltd-grid-type-2 .eltd-post-item-author-holder a:hover',
                '.eltd-blog-list-holder.eltd-masonry .eltd-post-item-author-holder a:hover',
                '.eltd-blog-list-holder.eltd-image-in-box h4.eltd-item-title a:hover',
                '.eltd-btn.eltd-btn-outline',
                '.post-password-form input.eltd-btn-outline[type=submit]',
                '.woocommerce .eltd-btn-outline.button',
                '.woocommerce .star-rating',
                '.eltd-pl-holder .eltd-pli .eltd-pli-rating',
                'input.eltd-btn-outline.wpcf7-form-control.wpcf7-submit',
                '.eltd-btn.eltd-btn-transparent',
                '.post-password-form input.eltd-btn-transparent[type=submit]',
                '.woocommerce .eltd-btn-transparent.button',
                'input.eltd-btn-transparent.wpcf7-form-control.wpcf7-submit',
                'blockquote .eltd-icon-quotations-holder',
                '.eltd-image-gallery .eltd-gallery-image .eltd-image-gallery-holder .eltd-icon-holder',
                '.eltd-video-button-play .eltd-video-button-wrapper',
                '.eltd-dropcaps',
                '.eltd-ptf-pinterest .eltd-ptf-item-text-overlay .eltd-ptf-category-holder:after',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-masonry .eltd-ptf-item-text-overlay .eltd-ptf-item-text-overlay-inner .eltd-ptf-category-holder:after',
                '.eltd-portfolio-list-holder-outer.eltd-ptf-justified-gallery .eltd-ptf-item-text-overlay .eltd-ptf-item-text-overlay-inner .eltd-ptf-category-holder:after',
                '.eltd-portfolio-filter-holder .eltd-portfolio-filter-holder-inner ul li.active',
                '.eltd-portfolio-filter-holder .eltd-portfolio-filter-holder-inner ul li.current',
                '.eltd-portfolio-filter-holder .eltd-portfolio-filter-holder-inner ul li:hover',
                '.eltd-iwt .eltd-iwt-icon-holder',
                '.eltd-social-share-holder.eltd-list li a:hover',
                '.eltd-social-share-holder.eltd-dropdown a:hover',
                '.eltd-process-holder.eltd-process-horizontal .eltd-pi-icon-holder .eltd-pi-icon',
                '.eltd-process-holder.eltd-process-vertical .eltd-pi-image-wrapper .eltd-pi-icon-holder .eltd-pi-icon',
                '.woocommerce-pagination .page-numbers li a.current',
                '.woocommerce-pagination .page-numbers li a:hover',
                '.woocommerce-pagination .page-numbers li span.current',
                '.woocommerce-pagination .page-numbers li span:hover',
                '.woocommerce-page .eltd-content .eltd-quantity-buttons .eltd-quantity-minus:hover',
                '.woocommerce-page .eltd-content .eltd-quantity-buttons .eltd-quantity-plus:hover',
                'div.woocommerce .eltd-quantity-buttons .eltd-quantity-minus:hover',
                'div.woocommerce .eltd-quantity-buttons .eltd-quantity-plus:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-text-inner .eltd-pli-price:before',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pl-buttons .added_to_cart:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pl-buttons .button:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pl-buttons .yith-wcwl-add-button a:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pl-buttons .yith-wcwl-wishlistaddedbrowse a:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pl-buttons .yith-wcwl-wishlistexistsbrowse a:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pl-buttons .yith-wcqv-button:hover',
                '.eltd-woo-single-page .related.products>h2:before',
                '.eltd-woo-single-page .upsells.products>h2:before',
                '.eltd-single-product-summary .woocommerce-product-rating:after',
                'ul.products>.product .eltd-pl-text-wrapper a.eltd-category-shop-link:hover',
                '.related.products ul.products>.product .eltd-pl-text-wrapper .price:before',
                '.woocommerce-page ul.products>.product .eltd-pl-text-wrapper .price:before',
                '.eltd-woocommerce-page.woocommerce-account .woocommerce-MyAccount-navigation ul li.is-active a',
                '.eltd-woocommerce-page.woocommerce-account .woocommerce table.shop_table td.order-number a:hover',
                '.widget.woocommerce.widget_shopping_cart .widget_shopping_cart_content ul li a:not(.remove):hover',
                '.widget.woocommerce.widget_shopping_cart .widget_shopping_cart_content ul li .remove:hover',
                '.widget.woocommerce.widget_layered_nav_filters a:hover',
                '.widget.woocommerce.widget_products ul li .product-title:hover',
                '.widget.woocommerce.widget_recently_viewed_products ul li .product-title:hover',
                '.widget.woocommerce.widget_top_rated_products ul li .product-title:hover',
                '.widget.woocommerce.widget_recent_reviews a:hover',
                '.eltd-shopping-cart-dropdown .eltd-item-info-holder .remove:hover',
                '.eltd-accordion-holder.eltd-woocommerce-tabs .eltd-title-holder:after',
                '.eltd-blog-date-on-side .format-quote p.eltd-quote-author',
                '.eltd-blog-holder article.sticky .eltd-post-title a',
                '.eltd-blog-holder article .eltd-post-info:after',
                '.eltd-blog-holder article.format-quote .eltd-post-mark',
                '.eltd-blog-holder article.format-link .eltd-post-mark',
                '.eltd-filter-blog-holder li.eltd-active',
                '.single .eltd-blog-holder .format-link .eltd-share-icons-single:before',
                '.single .eltd-author-description .eltd-author-social-holder a:hover',
                'article .eltd-category span.icon_tags',
                '.eltd-light-header .eltd-drop-down .menu_icon_wrapper',
                '.eltd-pagination li:hover a',
                '.eltd-blog-list-holder.eltd-grid-type-1 .eltd-post-info a:hover',
            );

            $color_important_selector = array(
                '.eltd-btn.eltd-btn-hover-white:not(.eltd-btn-custom-hover-color):hover',
                '.post-password-form input.eltd-btn-hover-white[type=submit]:not(.eltd-btn-custom-hover-color):hover',
                '.woocommerce .eltd-btn-hover-white.button:not(.eltd-btn-custom-hover-color):hover',
                'input.eltd-btn-hover-white.wpcf7-form-control.wpcf7-submit:not(.eltd-btn-custom-hover-color):hover'
            );

            $background_color_selector = array(
                '.eltd-st-loader .pulse',
                '.eltd-st-loader .double_pulse .double-bounce1',
                '.eltd-st-loader .double_pulse .double-bounce2',
                '.eltd-st-loader .cube',
                '.eltd-st-loader .rotating_cubes .cube1',
                '.eltd-st-loader .rotating_cubes .cube2',
                '.eltd-st-loader .stripes>div',
                '.eltd-st-loader .wave>div',
                '.eltd-st-loader .two_rotating_circles .dot1',
                '.eltd-st-loader .two_rotating_circles .dot2',
                '.eltd-st-loader .five_rotating_circles .container1>div',
                '.eltd-st-loader .five_rotating_circles .container2>div',
                '.eltd-st-loader .five_rotating_circles .container3>div',
                '.eltd-st-loader .atom .ball-1:before',
                '.eltd-st-loader .atom .ball-2:before',
                '.eltd-st-loader .atom .ball-3:before',
                '.eltd-st-loader .atom .ball-4:before',
                '.eltd-st-loader .clock .ball:before',
                '.eltd-st-loader .mitosis .ball',
                '.eltd-st-loader .lines .line1',
                '.eltd-st-loader .lines .line2',
                '.eltd-st-loader .lines .line3',
                '.eltd-st-loader .lines .line4',
                '.eltd-st-loader .fussion .ball',
                '.eltd-st-loader .fussion .ball-1',
                '.eltd-st-loader .fussion .ball-2',
                '.eltd-st-loader .fussion .ball-3',
                '.eltd-st-loader .fussion .ball-4',
                '.eltd-st-loader .wave_circles .ball',
                '.eltd-st-loader .pulse_circles .ball',
                '.eltd-carousel-pagination .owl-page.active span',
                'aside.eltd-sidebar .widget.widget_product_tag_cloud .tagcloud a:hover',
                'aside.eltd-sidebar .widget.widget_tag_cloud .tagcloud a:hover',
                'footer .eltd-footer-top-holder .widget.widget_product_tag_cloud .tagcloud a:hover',
                'footer .eltd-footer-top-holder .widget.widget_tag_cloud .tagcloud a:hover',
                '.eltd-side-menu .eltd-sidearea.widget_tag_cloud .tagcloud a:hover',
                '.eltd-icon-shortcode.circle',
                '.eltd-icon-shortcode.square',
                '.eltd-progress-bar .eltd-progress-content-outer .eltd-progress-content',
                '.eltd-price-table .eltd-price-table-inner .eltd-pt-label-holder .eltd-pt-label-inner',
                '.eltd-price-table.eltd-pt-active .eltd-price-table-inner',
                '.eltd-pie-chart-doughnut-holder .eltd-pie-legend ul li .eltd-pie-color-holder',
                '.eltd-pie-chart-pie-holder .eltd-pie-legend ul li .eltd-pie-color-holder',
                '.eltd-tabs.eltd-horizontal .eltd-tabs-nav li.ui-tabs-active:after',
                '.wpb_widgetised_column .widget.widget_product_tag_cloud .tagcloud a:hover',
                '.wpb_widgetised_column .widget.widget_tag_cloud .tagcloud a:hover',
                '.eltd-btn.eltd-btn-solid',
                '.post-password-form input[type=submit]',
                '.woocommerce .button',
                'input.wpcf7-form-control.wpcf7-submit',
                '.eltd-btn.eltd-btn-hover-solid .eltd-btn-helper',
                '.post-password-form input.eltd-btn-hover-solid[type=submit] .eltd-btn-helper',
                '.woocommerce .eltd-btn-hover-solid.button .eltd-btn-helper',
                'input.eltd-btn-hover-solid.wpcf7-form-control.wpcf7-submit .eltd-btn-helper',
                '.eltd-dropcaps.eltd-circle',
                '.eltd-dropcaps.eltd-square',
                '.eltd-comparision-pricing-tables-holder .eltd-cpt-table .eltd-cpt-table-btn a',
                '.widget_eltd_call_to_action_button .eltd-call-to-action-button',
                '.woocommerce-page .eltd-content .wc-forward:not(.added_to_cart):not(.checkout-button)',
                'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button)',
                '.woocommerce .eltd-onsale',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-image .eltd-pli-onsale',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-image .eltd-pli-new-product',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-text-inner .eltd-pli-add-to-cart.eltd-default-skin .added_to_cart',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-text-inner .eltd-pli-add-to-cart.eltd-default-skin .button',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-text-inner .eltd-pli-add-to-cart.eltd-light-skin .added_to_cart:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-text-inner .eltd-pli-add-to-cart.eltd-light-skin .button:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-text-inner .eltd-pli-add-to-cart.eltd-dark-skin .added_to_cart:hover',
                '.eltd-pl-holder .eltd-pli-inner .eltd-pli-text-inner .eltd-pli-add-to-cart.eltd-dark-skin .button:hover',
                'ul.products>.product .eltd-pl-inner .eltd-pl-text-inner .button:hover',
                '.widget.woocommerce.widget_price_filter .price_slider_wrapper .ui-widget-content .ui-slider-range',
                '.eltd-shopping-cart-dropdown .eltd-cart-bottom .eltd-view-cart',
                '.eltd-blog-holder.eltd-blog-type-masonry .format-quote .eltd-post-content',
                '.eltd-blog-holder.eltd-blog-type-masonry .format-link .eltd-post-content',
                '.eltd-blog-date-on-side .format-link .eltd-post-mark',
                '.eltd-blog-date-on-side .format-quote .eltd-post-mark',
                '.single .eltd-single-tags-holder .eltd-tags a:hover'
            );

            $background_color_important_selector = array(
                '.eltd-btn.eltd-btn-hover-solid:not(.eltd-btn-custom-hover-bg):not(.eltd-btn-with-animation):hover',
                '.post-password-form input.eltd-btn-hover-solid[type=submit]:not(.eltd-btn-custom-hover-bg):not(.eltd-btn-with-animation):hover',
                '.woocommerce .eltd-btn-hover-solid.button:not(.eltd-btn-custom-hover-bg):not(.eltd-btn-with-animation):hover',
                'input.eltd-btn-hover-solid.wpcf7-form-control.wpcf7-submit:not(.eltd-btn-custom-hover-bg):not(.eltd-btn-with-animation):hover'
            );

            $border_color_selector = array(
                '.eltd-st-loader .pulse_circles .ball',
                '.wpcf7-form-control.wpcf7-date:focus',
                '.wpcf7-form-control.wpcf7-number:focus',
                '.wpcf7-form-control.wpcf7-quiz:focus',
                '.wpcf7-form-control.wpcf7-select:focus',
                '.wpcf7-form-control.wpcf7-text:focus',
                '.wpcf7-form-control.wpcf7-textarea:focus',
                '#respond input[type=text]:focus',
                '#respond textarea:focus',
                '.post-password-form input[type=password]:focus',
                'aside.eltd-sidebar .widget.widget_product_tag_cloud .tagcloud a:hover',
                'aside.eltd-sidebar .widget.widget_tag_cloud .tagcloud a:hover',
                'footer .eltd-footer-top-holder .widget.widget_product_tag_cloud .tagcloud a:hover',
                'footer .eltd-footer-top-holder .widget.widget_tag_cloud .tagcloud a:hover',
                '.eltd-side-menu .eltd-sidearea.widget_tag_cloud .tagcloud a:hover',
                '.wpb_widgetised_column .widget.widget_product_tag_cloud .tagcloud a:hover',
                '.wpb_widgetised_column .widget.widget_tag_cloud .tagcloud a:hover',
                '.eltd-btn.eltd-btn-solid',
                '.post-password-form input[type=submit]',
                '.woocommerce .button',
                'input.wpcf7-form-control.wpcf7-submit',
                '.eltd-btn.eltd-btn-outline',
                '.post-password-form input.eltd-btn-outline[type=submit]',
                '.woocommerce .eltd-btn-outline.button',
                'input.eltd-btn-outline.wpcf7-form-control.wpcf7-submit',
                '.single .eltd-single-tags-holder .eltd-tags a:hover'
            );

            $border_color_important_selector = array(
                '.eltd-btn.eltd-btn-hover-solid:not(.eltd-btn-custom-border-hover):hover',
                '.post-password-form input.eltd-btn-hover-solid[type=submit]:not(.eltd-btn-custom-border-hover):hover',
                '.woocommerce .eltd-btn-hover-solid.button:not(.eltd-btn-custom-border-hover):hover',
                'input.eltd-btn-hover-solid.wpcf7-form-control.wpcf7-submit:not(.eltd-btn-custom-border-hover):hover'
            );
            $border_color_top_selector       = array(
                '.eltd-progress-bar .eltd-progress-number-wrapper.eltd-floating .eltd-down-arrow'
            );

            $border_color_right_selector = array(
                '.single .eltd-blog-date-on-side .format-quote .eltd-post-info>div',
                '.single .eltd-blog-date-on-side article.format-link .eltd-post-info>div'
            );

            echo malmo_elated_dynamic_css($color_selector, array('color' => malmo_elated_options()->getOptionValue('first_color')));
            echo malmo_elated_dynamic_css($color_important_selector, array('color' => malmo_elated_options()->getOptionValue('first_color').'!important'));
            echo malmo_elated_dynamic_css('::selection', array('background' => malmo_elated_options()->getOptionValue('first_color')));
            echo malmo_elated_dynamic_css('::-moz-selection', array('background' => malmo_elated_options()->getOptionValue('first_color')));
            echo malmo_elated_dynamic_css($background_color_selector, array('background-color' => malmo_elated_options()->getOptionValue('first_color')));
            echo malmo_elated_dynamic_css($background_color_important_selector, array('background-color' => malmo_elated_options()->getOptionValue('first_color').'!important'));
            echo malmo_elated_dynamic_css($border_color_selector, array('border-color' => malmo_elated_options()->getOptionValue('first_color')));
            echo malmo_elated_dynamic_css($border_color_important_selector, array('border-color' => malmo_elated_options()->getOptionValue('first_color').'!important'));
            echo malmo_elated_dynamic_css($border_color_top_selector, array('border-top-color' => malmo_elated_options()->getOptionValue('first_color')));
            echo malmo_elated_dynamic_css($border_color_right_selector, array('border-right-color' => malmo_elated_options()->getOptionValue('first_color')));
        }

        if(malmo_elated_options()->getOptionValue('page_background_color')) {
            $background_color_selector = array(
                '.eltd-wrapper-inner',
                '.eltd-content'
            );
            echo malmo_elated_dynamic_css($background_color_selector, array('background-color' => malmo_elated_options()->getOptionValue('page_background_color')));
        }

        if(malmo_elated_options()->getOptionValue('selection_color')) {
            echo malmo_elated_dynamic_css('::selection', array('background' => malmo_elated_options()->getOptionValue('selection_color')));
            echo malmo_elated_dynamic_css('::-moz-selection', array('background' => malmo_elated_options()->getOptionValue('selection_color')));
        }

        $boxed_background_style = array();
        if(malmo_elated_options()->getOptionValue('page_background_color_in_box')) {
            $boxed_background_style['background-color'] = malmo_elated_options()->getOptionValue('page_background_color_in_box');
        }

        if(malmo_elated_options()->getOptionValue('boxed_background_image')) {
            $boxed_background_style['background-image']    = 'url('.esc_url(malmo_elated_options()->getOptionValue('boxed_background_image')).')';
            $boxed_background_style['background-position'] = 'center 0px';
            $boxed_background_style['background-repeat']   = 'no-repeat';
        }

        if(malmo_elated_options()->getOptionValue('boxed_pattern_background_image')) {
            $boxed_background_style['background-image']    = 'url('.esc_url(malmo_elated_options()->getOptionValue('boxed_pattern_background_image')).')';
            $boxed_background_style['background-position'] = '0px 0px';
            $boxed_background_style['background-repeat']   = 'repeat';
        }

        if(malmo_elated_options()->getOptionValue('boxed_background_image_attachment')) {
            $boxed_background_style['background-attachment'] = (malmo_elated_options()->getOptionValue('boxed_background_image_attachment'));
        }

        echo malmo_elated_dynamic_css('.eltd-boxed.eltd-wrapper', $boxed_background_style);
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_design_styles');
}

if(!function_exists('malmo_elated_h1_styles')) {

    function malmo_elated_h1_styles() {

        $h1_styles = array();

        if(malmo_elated_options()->getOptionValue('h1_color') !== '') {
            $h1_styles['color'] = malmo_elated_options()->getOptionValue('h1_color');
        }
        if(malmo_elated_options()->getOptionValue('h1_google_fonts') !== '-1') {
            $h1_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('h1_google_fonts'));
        }
        if(malmo_elated_options()->getOptionValue('h1_fontsize') !== '') {
            $h1_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h1_fontsize')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h1_lineheight') !== '') {
            $h1_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h1_lineheight')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h1_texttransform') !== '') {
            $h1_styles['text-transform'] = malmo_elated_options()->getOptionValue('h1_texttransform');
        }
        if(malmo_elated_options()->getOptionValue('h1_fontstyle') !== '') {
            $h1_styles['font-style'] = malmo_elated_options()->getOptionValue('h1_fontstyle');
        }
        if(malmo_elated_options()->getOptionValue('h1_fontweight') !== '') {
            $h1_styles['font-weight'] = malmo_elated_options()->getOptionValue('h1_fontweight');
        }
        if(malmo_elated_options()->getOptionValue('h1_letterspacing') !== '') {
            $h1_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h1_letterspacing')).'px';
        }

        $h1_selector = array(
            'h1'
        );

        if(!empty($h1_styles)) {
            echo malmo_elated_dynamic_css($h1_selector, $h1_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_h1_styles');
}

if(!function_exists('malmo_elated_h2_styles')) {

    function malmo_elated_h2_styles() {

        $h2_styles = array();

        if(malmo_elated_options()->getOptionValue('h2_color') !== '') {
            $h2_styles['color'] = malmo_elated_options()->getOptionValue('h2_color');
        }
        if(malmo_elated_options()->getOptionValue('h2_google_fonts') !== '-1') {
            $h2_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('h2_google_fonts'));
        }
        if(malmo_elated_options()->getOptionValue('h2_fontsize') !== '') {
            $h2_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h2_fontsize')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h2_lineheight') !== '') {
            $h2_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h2_lineheight')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h2_texttransform') !== '') {
            $h2_styles['text-transform'] = malmo_elated_options()->getOptionValue('h2_texttransform');
        }
        if(malmo_elated_options()->getOptionValue('h2_fontstyle') !== '') {
            $h2_styles['font-style'] = malmo_elated_options()->getOptionValue('h2_fontstyle');
        }
        if(malmo_elated_options()->getOptionValue('h2_fontweight') !== '') {
            $h2_styles['font-weight'] = malmo_elated_options()->getOptionValue('h2_fontweight');
        }
        if(malmo_elated_options()->getOptionValue('h2_letterspacing') !== '') {
            $h2_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h2_letterspacing')).'px';
        }

        $h2_selector = array(
            'h2'
        );

        if(!empty($h2_styles)) {
            echo malmo_elated_dynamic_css($h2_selector, $h2_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_h2_styles');
}

if(!function_exists('malmo_elated_h3_styles')) {

    function malmo_elated_h3_styles() {

        $h3_styles = array();

        if(malmo_elated_options()->getOptionValue('h3_color') !== '') {
            $h3_styles['color'] = malmo_elated_options()->getOptionValue('h3_color');
        }
        if(malmo_elated_options()->getOptionValue('h3_google_fonts') !== '-1') {
            $h3_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('h3_google_fonts'));
        }
        if(malmo_elated_options()->getOptionValue('h3_fontsize') !== '') {
            $h3_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h3_fontsize')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h3_lineheight') !== '') {
            $h3_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h3_lineheight')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h3_texttransform') !== '') {
            $h3_styles['text-transform'] = malmo_elated_options()->getOptionValue('h3_texttransform');
        }
        if(malmo_elated_options()->getOptionValue('h3_fontstyle') !== '') {
            $h3_styles['font-style'] = malmo_elated_options()->getOptionValue('h3_fontstyle');
        }
        if(malmo_elated_options()->getOptionValue('h3_fontweight') !== '') {
            $h3_styles['font-weight'] = malmo_elated_options()->getOptionValue('h3_fontweight');
        }
        if(malmo_elated_options()->getOptionValue('h3_letterspacing') !== '') {
            $h3_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h3_letterspacing')).'px';
        }

        $h3_selector = array(
            'h3'
        );

        if(!empty($h3_styles)) {
            echo malmo_elated_dynamic_css($h3_selector, $h3_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_h3_styles');
}

if(!function_exists('malmo_elated_h4_styles')) {

    function malmo_elated_h4_styles() {

        $h4_styles = array();

        if(malmo_elated_options()->getOptionValue('h4_color') !== '') {
            $h4_styles['color'] = malmo_elated_options()->getOptionValue('h4_color');
        }
        if(malmo_elated_options()->getOptionValue('h4_google_fonts') !== '-1') {
            $h4_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('h4_google_fonts'));
        }
        if(malmo_elated_options()->getOptionValue('h4_fontsize') !== '') {
            $h4_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h4_fontsize')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h4_lineheight') !== '') {
            $h4_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h4_lineheight')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h4_texttransform') !== '') {
            $h4_styles['text-transform'] = malmo_elated_options()->getOptionValue('h4_texttransform');
        }
        if(malmo_elated_options()->getOptionValue('h4_fontstyle') !== '') {
            $h4_styles['font-style'] = malmo_elated_options()->getOptionValue('h4_fontstyle');
        }
        if(malmo_elated_options()->getOptionValue('h4_fontweight') !== '') {
            $h4_styles['font-weight'] = malmo_elated_options()->getOptionValue('h4_fontweight');
        }
        if(malmo_elated_options()->getOptionValue('h4_letterspacing') !== '') {
            $h4_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h4_letterspacing')).'px';
        }

        $h4_selector = array(
            'h4'
        );

        if(!empty($h4_styles)) {
            echo malmo_elated_dynamic_css($h4_selector, $h4_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_h4_styles');
}

if(!function_exists('malmo_elated_h5_styles')) {

    function malmo_elated_h5_styles() {

        $h5_styles = array();

        if(malmo_elated_options()->getOptionValue('h5_color') !== '') {
            $h5_styles['color'] = malmo_elated_options()->getOptionValue('h5_color');
        }
        if(malmo_elated_options()->getOptionValue('h5_google_fonts') !== '-1') {
            $h5_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('h5_google_fonts'));
        }
        if(malmo_elated_options()->getOptionValue('h5_fontsize') !== '') {
            $h5_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h5_fontsize')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h5_lineheight') !== '') {
            $h5_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h5_lineheight')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h5_texttransform') !== '') {
            $h5_styles['text-transform'] = malmo_elated_options()->getOptionValue('h5_texttransform');
        }
        if(malmo_elated_options()->getOptionValue('h5_fontstyle') !== '') {
            $h5_styles['font-style'] = malmo_elated_options()->getOptionValue('h5_fontstyle');
        }
        if(malmo_elated_options()->getOptionValue('h5_fontweight') !== '') {
            $h5_styles['font-weight'] = malmo_elated_options()->getOptionValue('h5_fontweight');
        }
        if(malmo_elated_options()->getOptionValue('h5_letterspacing') !== '') {
            $h5_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h5_letterspacing')).'px';
        }

        $h5_selector = array(
            'h5'
        );

        if(!empty($h5_styles)) {
            echo malmo_elated_dynamic_css($h5_selector, $h5_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_h5_styles');
}

if(!function_exists('malmo_elated_h6_styles')) {

    function malmo_elated_h6_styles() {

        $h6_styles = array();

        if(malmo_elated_options()->getOptionValue('h6_color') !== '') {
            $h6_styles['color'] = malmo_elated_options()->getOptionValue('h6_color');
        }
        if(malmo_elated_options()->getOptionValue('h6_google_fonts') !== '-1') {
            $h6_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('h6_google_fonts'));
        }
        if(malmo_elated_options()->getOptionValue('h6_fontsize') !== '') {
            $h6_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h6_fontsize')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h6_lineheight') !== '') {
            $h6_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h6_lineheight')).'px';
        }
        if(malmo_elated_options()->getOptionValue('h6_texttransform') !== '') {
            $h6_styles['text-transform'] = malmo_elated_options()->getOptionValue('h6_texttransform');
        }
        if(malmo_elated_options()->getOptionValue('h6_fontstyle') !== '') {
            $h6_styles['font-style'] = malmo_elated_options()->getOptionValue('h6_fontstyle');
        }
        if(malmo_elated_options()->getOptionValue('h6_fontweight') !== '') {
            $h6_styles['font-weight'] = malmo_elated_options()->getOptionValue('h6_fontweight');
        }
        if(malmo_elated_options()->getOptionValue('h6_letterspacing') !== '') {
            $h6_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('h6_letterspacing')).'px';
        }

        $h6_selector = array(
            'h6'
        );

        if(!empty($h6_styles)) {
            echo malmo_elated_dynamic_css($h6_selector, $h6_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_h6_styles');
}

if(!function_exists('malmo_elated_text_styles')) {

    function malmo_elated_text_styles() {

        $text_styles = array();

        if(malmo_elated_options()->getOptionValue('text_color') !== '') {
            $text_styles['color'] = malmo_elated_options()->getOptionValue('text_color');
        }
        if(malmo_elated_options()->getOptionValue('text_google_fonts') !== '-1') {
            $text_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('text_google_fonts'));
        }
        if(malmo_elated_options()->getOptionValue('text_fontsize') !== '') {
            $text_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('text_fontsize')).'px';
        }
        if(malmo_elated_options()->getOptionValue('text_lineheight') !== '') {
            $text_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('text_lineheight')).'px';
        }
        if(malmo_elated_options()->getOptionValue('text_texttransform') !== '') {
            $text_styles['text-transform'] = malmo_elated_options()->getOptionValue('text_texttransform');
        }
        if(malmo_elated_options()->getOptionValue('text_fontstyle') !== '') {
            $text_styles['font-style'] = malmo_elated_options()->getOptionValue('text_fontstyle');
        }
        if(malmo_elated_options()->getOptionValue('text_fontweight') !== '') {
            $text_styles['font-weight'] = malmo_elated_options()->getOptionValue('text_fontweight');
        }
        if(malmo_elated_options()->getOptionValue('text_letterspacing') !== '') {
            $text_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('text_letterspacing')).'px';
        }

        $text_selector = array(
            'p'
        );

        if(!empty($text_styles)) {
            echo malmo_elated_dynamic_css($text_selector, $text_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_text_styles');
}

if(!function_exists('malmo_elated_link_styles')) {

    function malmo_elated_link_styles() {

        $link_styles = array();

        if(malmo_elated_options()->getOptionValue('link_color') !== '') {
            $link_styles['color'] = malmo_elated_options()->getOptionValue('link_color');
        }
        if(malmo_elated_options()->getOptionValue('link_fontstyle') !== '') {
            $link_styles['font-style'] = malmo_elated_options()->getOptionValue('link_fontstyle');
        }
        if(malmo_elated_options()->getOptionValue('link_fontweight') !== '') {
            $link_styles['font-weight'] = malmo_elated_options()->getOptionValue('link_fontweight');
        }
        if(malmo_elated_options()->getOptionValue('link_fontdecoration') !== '') {
            $link_styles['text-decoration'] = malmo_elated_options()->getOptionValue('link_fontdecoration');
        }

        $link_selector = array(
            'a',
            'p a'
        );

        if(!empty($link_styles)) {
            echo malmo_elated_dynamic_css($link_selector, $link_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_link_styles');
}

if(!function_exists('malmo_elated_link_hover_styles')) {

    function malmo_elated_link_hover_styles() {

        $link_hover_styles = array();

        if(malmo_elated_options()->getOptionValue('link_hovercolor') !== '') {
            $link_hover_styles['color'] = malmo_elated_options()->getOptionValue('link_hovercolor');
        }
        if(malmo_elated_options()->getOptionValue('link_hover_fontdecoration') !== '') {
            $link_hover_styles['text-decoration'] = malmo_elated_options()->getOptionValue('link_hover_fontdecoration');
        }

        $link_hover_selector = array(
            'a:hover',
            'p a:hover'
        );

        if(!empty($link_hover_styles)) {
            echo malmo_elated_dynamic_css($link_hover_selector, $link_hover_styles);
        }

        $link_heading_hover_styles = array();

        if(malmo_elated_options()->getOptionValue('link_hovercolor') !== '') {
            $link_heading_hover_styles['color'] = malmo_elated_options()->getOptionValue('link_hovercolor');
        }

        $link_heading_hover_selector = array(
            'h1 a:hover',
            'h2 a:hover',
            'h3 a:hover',
            'h4 a:hover',
            'h5 a:hover',
            'h6 a:hover'
        );

        if(!empty($link_heading_hover_styles)) {
            echo malmo_elated_dynamic_css($link_heading_hover_selector, $link_heading_hover_styles);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_link_hover_styles');
}

if(!function_exists('malmo_elated_smooth_page_transition_styles')) {

    function malmo_elated_smooth_page_transition_styles() {

        $loader_style = array();

        if(malmo_elated_options()->getOptionValue('smooth_pt_bgnd_color') !== '') {
            $loader_style['background-color'] = malmo_elated_options()->getOptionValue('smooth_pt_bgnd_color');
        }

        $loader_selector = array('.eltd-smooth-transition-loader');

        if(!empty($loader_style)) {
            echo malmo_elated_dynamic_css($loader_selector, $loader_style);
        }

        $spinner_style = array();

        if(malmo_elated_options()->getOptionValue('smooth_pt_spinner_color') !== '') {
            $spinner_style['background-color'] = malmo_elated_options()->getOptionValue('smooth_pt_spinner_color');
        }

        $spinner_selectors = array(
            '.eltd-st-loader.pulse',
            '.eltd-st-loader.double_pulse.double-bounce1',
            '.eltd-st-loader.double_pulse.double-bounce2',
            '.eltd-st-loader.cube',
            '.eltd-st-loader.rotating_cubes.cube1',
            '.eltd-st-loader.rotating_cubes.cube2',
            '.eltd-st-loader.stripes > div',
            '.eltd-st-loader.wave > div',
            '.eltd-st-loader.two_rotating_circles.dot1',
            '.eltd-st-loader.two_rotating_circles.dot2',
            '.eltd-st-loader.five_rotating_circles.container1 > div',
            '.eltd-st-loader.five_rotating_circles.container2 > div',
            '.eltd-st-loader.five_rotating_circles.container3 > div',
            '.eltd-st-loader.atom.ball-1:before',
            '.eltd-st-loader.atom.ball-2:before',
            '.eltd-st-loader.atom.ball-3:before',
            '.eltd-st-loader.atom.ball-4:before',
            '.eltd-st-loader.clock.ball:before',
            '.eltd-st-loader.mitosis.ball',
            '.eltd-st-loader.lines.line1',
            '.eltd-st-loader.lines.line2',
            '.eltd-st-loader.lines.line3',
            '.eltd-st-loader.lines.line4',
            '.eltd-st-loader.fussion.ball',
            '.eltd-st-loader.fussion.ball-1',
            '.eltd-st-loader.fussion.ball-2',
            '.eltd-st-loader.fussion.ball-3',
            '.eltd-st-loader.fussion.ball-4',
            '.eltd-st-loader.wave_circles.ball',
            '.eltd-st-loader.pulse_circles.ball'
        );

        if(!empty($spinner_style)) {
            echo malmo_elated_dynamic_css($spinner_selectors, $spinner_style);
        }
    }

    add_action('malmo_elated_style_dynamic', 'malmo_elated_smooth_page_transition_styles');
}
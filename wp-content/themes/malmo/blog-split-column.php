<?php
/*
	Template Name: Blog: Split Column
*/
?>
<?php get_header(); ?>
<?php malmo_elated_get_title(); ?>
<?php get_template_part('slider'); ?>
	<div class="eltd-container">
		<?php do_action('malmo_elated_after_container_open'); ?>
		<div class="eltd-container-inner">
			<?php malmo_elated_get_blog('split-column'); ?>
		</div>
		<?php do_action('malmo_elated_before_container_close'); ?>
	</div>
<?php get_footer(); ?>
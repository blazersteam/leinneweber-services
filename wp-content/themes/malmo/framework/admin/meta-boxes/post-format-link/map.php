<?php

/*** Link Post Format ***/

$link_post_format_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('post'),
        'title' => esc_html__('Link Post Format', 'malmo'),
        'name'  => 'post_format_link_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_link_link_meta',
        'type'        => 'text',
        'label'       => esc_html__('Link', 'malmo'),
        'description' => esc_html__('Enter link', 'malmo'),
        'parent'      => $link_post_format_meta_box,

    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_link_text_meta',
        'type'        => 'text',
        'label'       => esc_html__('Link Text', 'malmo'),
        'description' => esc_html__('Enter text for the link', 'malmo'),
        'parent'      => $link_post_format_meta_box,

    )
);


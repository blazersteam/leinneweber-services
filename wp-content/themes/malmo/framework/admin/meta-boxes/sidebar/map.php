<?php

$malmo_custom_sidebars = malmo_elated_get_custom_sidebars();

$malmo_sidebar_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post'),
        'title' => esc_html__('Sidebar', 'malmo'),
        'name'  => 'sidebar_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_sidebar_meta',
        'type'        => 'select',
        'label'       => esc_html__('Layout', 'malmo'),
        'description' => esc_html__('Choose the sidebar layout', 'malmo'),
        'parent'      => $malmo_sidebar_meta_box,
        'options'     => array(
            ''                 => esc_html__('Default', 'malmo'),
            'no-sidebar'       => esc_html__('No Sidebar', 'malmo'),
            'sidebar-33-right' => esc_html__('Sidebar 1/3 Right', 'malmo'),
            'sidebar-25-right' => esc_html__('Sidebar 1/4 Right', 'malmo'),
            'sidebar-33-left'  => esc_html__('Sidebar 1/3 Left', 'malmo'),
            'sidebar-25-left'  => esc_html__('Sidebar 1/4 Left', 'malmo'),
        )
    )
);

if(count($malmo_custom_sidebars) > 0) {
    malmo_elated_create_meta_box_field(array(
        'name'        => 'eltd_custom_sidebar_meta',
        'type'        => 'selectblank',
        'label'       => esc_html__('Choose Widget Area in Sidebar', 'malmo'),
        'description' => esc_html__('Choose Custom Widget area to display in Sidebar"', 'malmo'),
        'parent'      => $malmo_sidebar_meta_box,
        'options'     => $malmo_custom_sidebars
    ));
}

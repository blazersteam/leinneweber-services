<?php

if(!function_exists('malmo_elated_load_elements_map')) {
    /**
     * Add Elements option page for shortcodes
     */
    function malmo_elated_load_elements_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '_elements_page',
                'title' => esc_html__('Elements', 'malmo'),
                'icon'  => 'fa fa-header'
            )
        );

        do_action('malmo_elated_options_elements_map');

    }

    add_action('malmo_elated_options_map', 'malmo_elated_load_elements_map', 11);

}
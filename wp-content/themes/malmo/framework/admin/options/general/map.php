<?php

if(!function_exists('malmo_elated_general_options_map')) {
    /**
     * General options page
     */
    function malmo_elated_general_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '',
                'title' => esc_html__('General', 'malmo'),
                'icon'  => 'fa fa-institution'
            )
        );

        $panel_design_style = malmo_elated_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_design_style',
                'title' => esc_html__('Design Style', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'google_fonts',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo'),
                'description'   => esc_html__('Choose a default Google font for your site', 'malmo'),
                'parent'        => $panel_design_style
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'additional_google_fonts',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Additional Google Fonts', 'malmo'),
                'description'   => '',
                'parent'        => $panel_design_style,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_additional_google_fonts_container"
                )
            )
        );

        $additional_google_fonts_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_design_style,
                'name'            => 'additional_google_fonts_container',
                'hidden_property' => 'additional_google_fonts',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font1',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo'),
                'description'   => esc_html__('Choose additional Google font for your site', 'malmo'),
                'parent'        => $additional_google_fonts_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font2',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo'),
                'description'   => esc_html__('Choose additional Google font for your site', 'malmo'),
                'parent'        => $additional_google_fonts_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font3',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo'),
                'description'   => esc_html__('Choose additional Google font for your site', 'malmo'),
                'parent'        => $additional_google_fonts_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font4',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo'),
                'description'   => esc_html__('Choose additional Google font for your site', 'malmo'),
                'parent'        => $additional_google_fonts_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font5',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'malmo'),
                'description'   => esc_html__('Choose additional Google font for your site', 'malmo'),
                'parent'        => $additional_google_fonts_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'first_color',
                'type'        => 'color',
                'label'       => esc_html__('First Main Color', 'malmo'),
                'description' => esc_html__('Choose the most dominant theme color. Default color is #ff1d4d', 'malmo'),
                'parent'      => $panel_design_style
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'page_background_color',
                'type'        => 'color',
                'label'       => esc_html__('Page Background Color', 'malmo'),
                'description' => esc_html__('Choose the background color for page content. Default color is #ffffff', 'malmo'),
                'parent'      => $panel_design_style
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'page_fixed_background_image',
                'type'        => 'image',
                'label'       => esc_html__('Fixed Background Image', 'malmo'),
                'description' => esc_html__('Choose an image that will be displayed in the page background as fixed', 'malmo'),
                'parent'      => $panel_design_style
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'selection_color',
                'type'        => 'color',
                'label'       => esc_html__('Text Selection Color', 'malmo'),
                'description' => esc_html__('Choose the color users see when selecting text', 'malmo'),
                'parent'      => $panel_design_style
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'boxed',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Boxed Layout', 'malmo'),
                'description'   => '',
                'parent'        => $panel_design_style,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_boxed_container"
                )
            )
        );

        $boxed_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_design_style,
                'name'            => 'boxed_container',
                'hidden_property' => 'boxed',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'page_background_color_in_box',
                'type'        => 'color',
                'label'       => esc_html__('Page Background Color', 'malmo'),
                'description' => esc_html__('Choose the page background color outside box.', 'malmo'),
                'parent'      => $boxed_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'boxed_background_image',
                'type'        => 'image',
                'label'       => esc_html__('Background Image', 'malmo'),
                'description' => esc_html__('Choose an image to be displayed in background', 'malmo'),
                'parent'      => $boxed_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'boxed_pattern_background_image',
                'type'        => 'image',
                'label'       => esc_html__('Background Pattern', 'malmo'),
                'description' => esc_html__('Choose an image to be used as background pattern', 'malmo'),
                'parent'      => $boxed_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'boxed_background_image_attachment',
                'type'          => 'select',
                'default_value' => 'fixed',
                'label'         => esc_html__('Background Image Attachment', 'malmo'),
                'description'   => esc_html__('Choose background image attachment', 'malmo'),
                'parent'        => $boxed_container,
                'options'       => array(
                    'fixed'  => esc_html__('Fixed', 'malmo'),
                    'scroll' => esc_html__('Scroll', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'initial_content_width',
                'type'          => 'select',
                'parent'        => $panel_design_style,
                'default_value' => 'grid-1300',
                'label'         => esc_html__('Initial Width of Content', 'malmo'),
                'description'   => esc_html__('Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid"', 'malmo'),
                'options'       => array(
                    "grid-1300" => esc_html__('1300px - default', 'malmo'),
                    "grid-1200" =>  esc_html__('1200px', 'malmo'),
                    ""          =>  esc_html__('1100px', 'malmo'),
                    "grid-1000" =>  esc_html__('1000px', 'malmo'),
                    "grid-800"  =>  esc_html__('800px', 'malmo')
                )

            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'preload_pattern_image',
                'type'        => 'image',
                'label'       => esc_html__('Preload Pattern Image', 'malmo'),
                'description' => esc_html__('Choose preload pattern image to be displayed until images are loaded ', 'malmo'),
                'parent'      => $panel_design_style
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'element_appear_amount',
                'type'        => 'text',
                'label'       => esc_html__('Element Appearance', 'malmo'),
                'description' => esc_html__('For animated elements, set distance (related to browser bottom) to start the animation', 'malmo'),
                'parent'      => $panel_design_style,
                'args'        => array(
                    'col_width' => 2,
                    'suffix'    => 'px'
                )
            )
        );

        $panel_settings = malmo_elated_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_settings',
                'title' => esc_html__('Settings', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'smooth_scroll',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Smooth Scroll', 'malmo'),
                'description'   => esc_html__('Enabling this option will perform a smooth scrolling effect on every page (except on Mac and touch devices)', 'malmo'),
                'parent'        => $panel_settings
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'smooth_page_transitions',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Smooth Page Transitions', 'malmo'),
                'description'   => esc_html__('Enabling this option will perform a smooth transition between pages when clicking on links.', 'malmo'),
                'parent'        => $panel_settings,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_page_transitions_container"
                )
            )
        );

        $page_transitions_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_settings,
                'name'            => 'page_transitions_container',
                'hidden_property' => 'smooth_page_transitions',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'   => 'smooth_pt_bgnd_color',
                'type'   => 'color',
                'label'  => esc_html__('Page Loader Background Color', 'malmo'),
                //'description'   => 'Enabling this option will perform a smooth transition between pages when clicking on links.',
                'parent' => $page_transitions_container
            )
        );

        $group_pt_spinner_animation = malmo_elated_add_admin_group(array(
            'name'        => 'group_pt_spinner_animation',
            'title'       => esc_html__('Loader Style', 'malmo'),
            'description' => esc_html__('Define styles for loader spinner animation', 'malmo'),
            'parent'      => $page_transitions_container
        ));

        $row_pt_spinner_animation = malmo_elated_add_admin_row(array(
            'name'   => 'row_pt_spinner_animation',
            'parent' => $group_pt_spinner_animation
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectsimple',
            'name'          => 'smooth_pt_spinner_type',
            'default_value' => 'wave',
            'label'         => esc_html__('Spinner Type', 'malmo'),
            'parent'        => $row_pt_spinner_animation,
            'options'       => array(
                "pulse"                 => esc_html__('Pulse', 'malmo'),
                "double_pulse"          => esc_html__('Double Pulse', 'malmo'),
                "cube"                  => esc_html__('Cube', 'malmo'),
                "rotating_cubes"        => esc_html__('Rotating Cubes', 'malmo'),
                "stripes"               => esc_html__('Stripes', 'malmo'),
                "wave"                  => esc_html__('Wave', 'malmo'),
                "two_rotating_circles"  => esc_html__('2 Rotating Circles', 'malmo'),
                "five_rotating_circles" => esc_html__('5 Rotating Circles', 'malmo'),
                "atom"                  => esc_html__('Atom', 'malmo'),
                "clock"                 => esc_html__('Clock', 'malmo'),
                "mitosis"               => esc_html__('Mitosis', 'malmo'),
                "lines"                 => esc_html__('Lines', 'malmo'),
                "fussion"               => esc_html__('Fussion', 'malmo'),
                "wave_circles"          => esc_html__('Wave Circles', 'malmo'),
                "pulse_circles"         => esc_html__('Pulse Circles', 'malmo')
            )
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'smooth_pt_spinner_color',
            'default_value' => '',
            'label'         => esc_html__('Spinner Color', 'malmo'),
            'parent'        => $row_pt_spinner_animation
        ));

        malmo_elated_add_admin_field(
            array(
                'name'          => 'smooth_pt_true_ajax',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('True AJAX Transitions', 'malmo'),
                'description'   => esc_html__('Choose whether to load pages using AJAX or refresh the browser window, only mimicking AJAX behavior.', 'malmo'),
                'parent'        => $page_transitions_container,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_true_ajax_params_container"
                )
            )
        );

        $true_ajax_params_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $page_transitions_container,
                'name'            => 'true_ajax_params_container',
                'hidden_property' => 'smooth_pt_true_ajax',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'default_page_transition',
                'type'          => 'select',
                'default_value' => 'fade',
                'label'         => esc_html__('Page Transition', 'malmo'),
                'description'   => esc_html__('Choose the default type of transition between pages', 'malmo'),
                'parent'        => $true_ajax_params_container,
                'options'       => array(
                    'no-animation' => esc_html__('No animation', 'malmo'),
                    'fade'         => esc_html__('Fade', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'internal_no_ajax_links',
                'type'        => 'textarea',
                'label'       => esc_html__('List of Internal URLs Loaded Without AJAX (Comma-Separated)', 'malmo'),
                'description' => esc_html__('To disable AJAX transitions on certain pages, enter their full URLs here (for example: http://www.mydomain.com/forum/)', 'malmo'),
                'parent'      => $true_ajax_params_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'elements_animation_on_touch',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Elements Animation on Mobile/Touch Devices', 'malmo'),
                'description'   => esc_html__('Enabling this option will allow elements (shortcodes) to animate on mobile / touch devices', 'malmo'),
                'parent'        => $panel_settings
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'show_back_button',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Show "Back To Top Button"', 'malmo'),
                'description'   => esc_html__('Enabling this option will display a Back to Top button on every page', 'malmo'),
                'parent'        => $panel_settings
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'responsiveness',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Responsiveness', 'malmo'),
                'description'   => esc_html__('Enabling this option will make all pages responsive', 'malmo'),
                'parent'        => $panel_settings
            )
        );

        $panel_custom_code = malmo_elated_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_custom_code',
                'title' => esc_html__('Custom Code', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'custom_css',
                'type'        => 'textarea',
                'label'       => esc_html__('Custom CSS', 'malmo'),
                'description' => esc_html__('Enter your custom CSS here', 'malmo'),
                'parent'      => $panel_custom_code
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'custom_js',
                'type'        => 'textarea',
                'label'       => esc_html__('Custom JS', 'malmo'),
                'description' => esc_html__('Enter your custom Javascript here', 'malmo'),
                'parent'      => $panel_custom_code
            )
        );

        $panel_google_api = malmo_elated_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_google_api',
                'title' => esc_html__('Google API', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'google_maps_api_key',
                'type'        => 'text',
                'label'       => esc_html__('Google Maps Api Key', 'malmo'),
                'description' => esc_html__('Insert your Google Maps API key here. For instructions on how to create a Google Maps API key, please refer to our documentation.', 'malmo'),
                'parent'      => $panel_google_api
            )
        );
    }

    add_action('malmo_elated_options_map', 'malmo_elated_general_options_map', 1);

}
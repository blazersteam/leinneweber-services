<?php

if(!function_exists('malmo_elated_parallax_options_map')) {
    /**
     * Parallax options page
     */
    function malmo_elated_parallax_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug' => '_parallax_page',
                'title' => esc_html__('Parallax', 'malmo'),
                'icon' => 'fa fa-unsorted'
            )
        );

        $panel_parallax = malmo_elated_add_admin_panel(
            array(
                'page'  => '_parallax_page',
                'name'  => 'panel_parallax',
                'title' => esc_html__('Parallax', 'malmo')
            )
        );

        malmo_elated_add_admin_field(array(
            'type'          => 'onoff',
            'name'          => 'parallax_on_off',
            'default_value' => 'off',
            'label'         => esc_html__('Parallax on touch devices', 'malmo'),
            'description'   => esc_html__('Enabling this option will allow parallax on touch devices', 'malmo'),
            'parent'        => $panel_parallax
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'text',
            'name'          => 'parallax_min_height',
            'default_value' => '400',
            'label'         => esc_html__('Parallax Min Height', 'malmo'),
            'description'   => esc_html__('Set a minimum height for parallax images on small displays (phones, tablets, etc.)', 'malmo'),
            'args'          => array(
                'col_width' => 3,
                'suffix'    => 'px'
            ),
            'parent'        => $panel_parallax
        ));

    }

    add_action('malmo_elated_options_map', 'malmo_elated_parallax_options_map', 16);

}
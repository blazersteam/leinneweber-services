<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <div class="eltd-post-image-wrapper <?php if (has_post_thumbnail()) echo 'eltd-post-with-image'; ?>">
            <?php malmo_elated_get_module_template_part('templates/single/parts/image', 'blog'); ?>
            <?php malmo_elated_post_boxed_date(); ?>
            <div class="eltd-audio-player-holder">
                <?php malmo_elated_get_module_template_part('templates/parts/audio', 'blog'); ?>
            </div>
        </div>
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner clearfix">
                <?php malmo_elated_get_module_template_part('templates/single/parts/title', 'blog'); ?>
                <div class="eltd-post-info">
                    <?php malmo_elated_post_info(array('author' => 'yes', 'like' => 'yes', 'comments' => 'yes', 'category' => 'yes')) ?>
                </div>
                <?php
                the_content();
                ?>
            </div>
        </div>
    </div>
</article>
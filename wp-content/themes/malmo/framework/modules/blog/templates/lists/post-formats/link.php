<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner">
                <div class="eltd-post-link-holder">
                    <?php 
                    $link_text = get_post_meta(get_the_ID(), "eltd_post_link_text_meta", true);
                    $link = get_post_meta(get_the_ID(), "eltd_post_link_link_meta", true);
                    ?>
                    <div class="eltd-post-mark"><span aria-hidden="true" class="lnr lnr-link"></span></div>
                    <div class="eltd-post-link-text"><h3><a href="<?php echo get_permalink(); ?>" target="_blank"><?php echo esc_html($link_text); ?></a></h3></div>
                    <div class="eltd-post-link-link"><h6><a href="<?php echo esc_url($link); ?>" target="_blank"><?php echo esc_html($link); ?></a></h6></div>
                </div>
            </div>
        </div>
    </div>
</article>
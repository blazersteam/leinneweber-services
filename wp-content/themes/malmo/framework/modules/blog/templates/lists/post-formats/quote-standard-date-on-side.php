<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="eltd-post-content clearfix">
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner">
                <div class="eltd-post-mark">
                    <?php echo malmo_elated_icon_collections()->renderIcon('icon_quotations', 'font_elegant'); ?>
                </div>
                <div class="eltd-post-title-holder">
                    <a href="<?php the_permalink() ?>">
                        <h4 class="eltd-post-title"><?php echo esc_html(get_post_meta(get_the_ID(), 'eltd_post_quote_text_meta', true)); ?></h4>

                        <p class="eltd-quote-author"><?php the_title(); ?></p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</article>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner">
                <div class="eltd-post-quote-holder">
                    <?php 
                    $quote_text = get_post_meta(get_the_ID(), "eltd_post_quote_text_meta", true);
                    $quote_author = get_post_meta(get_the_ID(), "eltd_post_quote_author_meta", true);
                    $quote_position = get_post_meta(get_the_ID(), "eltd_post_quote_position_meta", true);
                    ?>
                    <div class="eltd-post-mark"><span aria-hidden="true" class="icon_quotations"></span></div>
                    <div class="eltd-post-quote-text"><h3><a href="<?php echo get_permalink(); ?>" target="_blank"><?php echo esc_html($quote_text); ?></a></h3></div>
                    <div class="eltd-post-quote-author">
                        <h5 class="eltd-post-quote-author-text">
                        <span class="eltd-post-quote-author-name"><?php echo esc_html($quote_author) ?></span>
                        <?php if($quote_position !== '') { ?>
                            <span class="eltd-post-quote-position"><?php echo esc_attr($quote_position) ?></span>
                        <?php } ?>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner clearfix">
                <div class="eltd-post-link-holder">
                    <?php
                    $link_text = get_post_meta(get_the_ID(), "eltd_post_link_text_meta", true);
                    $link      = get_post_meta(get_the_ID(), "eltd_post_link_link_meta", true);
                    ?>
                    <div class="eltd-post-mark"><span aria-hidden="true" class="lnr lnr-link"></span></div>
                    <a href="<?php echo esc_url($link); ?>" target="_blank"><?php malmo_elated_get_module_template_part('templates/single/parts/title', 'blog'); ?></a>
                    <div class="eltd-post-info">
                        <?php malmo_elated_post_info(array(
                                 'date'     => 'yes',
                                 'like'     => 'yes',
                                 'comments' => 'yes',
                                 'category' => 'yes'
                        )) ?>
                    </div>
                    <div class="eltd-share-icons-single">
                        <?php $post_info_array['share'] = malmo_elated_options()->getOptionValue('enable_social_share') == 'yes'; ?>
                        <?php if($post_info_array['share'] == 'yes'): ?>
                            <span class="eltd-share-label"><?php esc_html_e('Share:', 'malmo'); ?></span>
                        <?php endif; ?>
                        <?php echo malmo_elated_get_social_share_html(array(
                            'type'      => 'list',
                            'icon_type' => 'normal'
                        )); ?>
                    </div>
                </div>
                <?php the_content(); ?>
            </div>
            <div class="eltd-tags-share-holder clearfix">
                <?php
                if(has_tag() && malmo_elated_options()->getOptionValue('blog_enable_single_tags') == 'yes') {
                    malmo_elated_get_module_template_part('templates/single/parts/tags', 'blog');
                }
                ?>
            </div>
        </div>
    </div>
</article>
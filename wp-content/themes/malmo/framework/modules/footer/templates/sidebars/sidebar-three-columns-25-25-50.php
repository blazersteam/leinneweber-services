<div class="eltd-grid-row eltd-footer-top-25-25-50">
	<div class="eltd-grid-col-6">
		<div class="eltd-grid-row">
			<div class="eltd-grid-col-6">
				<?php if(is_active_sidebar('footer_column_2')) : ?>
					<?php dynamic_sidebar('footer_column_2'); ?>
				<?php endif; ?>
			</div>
			<div class="eltd-grid-col-6">
				<?php if(is_active_sidebar('footer_column_3')) : ?>
					<?php dynamic_sidebar('footer_column_3'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="eltd-grid-col-6">
		<?php if(is_active_sidebar('footer_column_1')) : ?>
			<?php dynamic_sidebar('footer_column_1'); ?>
		<?php endif; ?>
	</div>
</div>
<?php

if(!function_exists('malmo_elated_header_top_bar_styles')) {
	/**
	 * Generates styles for header top bar
	 */
	function malmo_elated_header_top_bar_styles() {
		global $malmo_options;

		if($malmo_options['top_bar_height'] !== '') {
			echo malmo_elated_dynamic_css('.eltd-top-bar', array('height' => $malmo_options['top_bar_height'].'px'));
			echo malmo_elated_dynamic_css('.eltd-top-bar .eltd-logo-wrapper a', array('max-height' => $malmo_options['top_bar_height'].'px'));
		}

		if($malmo_options['top_bar_in_grid'] == 'yes') {
			$top_bar_grid_selector = '.eltd-top-bar .eltd-grid .eltd-vertical-align-containers';
			$top_bar_grid_styles   = array();
			if($malmo_options['top_bar_grid_background_color'] !== '') {
				$grid_background_color        = $malmo_options['top_bar_grid_background_color'];
				$grid_background_transparency = 1;

				if(malmo_elated_options()->getOptionValue('top_bar_grid_background_transparency')) {
					$grid_background_transparency = malmo_elated_options()->getOptionValue('top_bar_grid_background_transparency');
				}

				$grid_background_color                   = malmo_elated_rgba_color($grid_background_color, $grid_background_transparency);
				$top_bar_grid_styles['background-color'] = $grid_background_color;
			}

			echo malmo_elated_dynamic_css($top_bar_grid_selector, $top_bar_grid_styles);
		}

		$background_color = malmo_elated_options()->getOptionValue('top_bar_background_color');
		$top_bar_styles   = array();
		if($background_color !== '') {
			$background_transparency = 1;
			if(malmo_elated_options()->getOptionValue('top_bar_background_transparency') !== '') {
				$background_transparency = malmo_elated_options()->getOptionValue('top_bar_background_transparency');
			}

			$background_color                   = malmo_elated_rgba_color($background_color, $background_transparency);
			$top_bar_styles['background-color'] = $background_color;
		}

		echo malmo_elated_dynamic_css('.eltd-top-bar', $top_bar_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_header_top_bar_styles');
}

if(!function_exists('malmo_elated_header_standard_menu_area_styles')) {
	/**
	 * Generates styles for header standard menu
	 */
	function malmo_elated_header_standard_menu_area_styles() {
		global $malmo_options;

		$menu_area_header_standard_styles = array();

		if($malmo_options['menu_area_background_color_header_standard'] !== '') {
			$menu_area_background_color        = $malmo_options['menu_area_background_color_header_standard'];
			$menu_area_background_transparency = 1;

			if($malmo_options['menu_area_background_transparency_header_standard'] !== '') {
				$menu_area_background_transparency = $malmo_options['menu_area_background_transparency_header_standard'];
			}

			$menu_area_header_standard_styles['background-color'] = malmo_elated_rgba_color($menu_area_background_color, $menu_area_background_transparency);
		}

		if($malmo_options['menu_area_height_header_standard'] !== '') {
			$max_height = intval(malmo_elated_filter_px($malmo_options['menu_area_height_header_standard']) * 0.9).'px';
			echo malmo_elated_dynamic_css('.eltd-header-standard .eltd-page-header .eltd-logo-wrapper a', array('max-height' => $max_height));

			$menu_area_header_standard_styles['height'] = malmo_elated_filter_px($malmo_options['menu_area_height_header_standard']).'px';

		}

		echo malmo_elated_dynamic_css('.eltd-header-standard .eltd-page-header .eltd-menu-area', $menu_area_header_standard_styles);

		$menu_area_grid_header_standard_styles = array();

		if($malmo_options['menu_area_in_grid_header_standard'] == 'yes' && $malmo_options['menu_area_grid_background_color_header_standard'] !== '') {
			$menu_area_grid_background_color        = $malmo_options['menu_area_grid_background_color_header_standard'];
			$menu_area_grid_background_transparency = 1;

			if($malmo_options['menu_area_grid_background_transparency_header_standard'] !== '') {
				$menu_area_grid_background_transparency = $malmo_options['menu_area_grid_background_transparency_header_standard'];
			}

			$menu_area_grid_header_standard_styles['background-color'] = malmo_elated_rgba_color($menu_area_grid_background_color, $menu_area_grid_background_transparency);
		}

		echo malmo_elated_dynamic_css('.eltd-header-standard .eltd-page-header .eltd-menu-area .eltd-grid .eltd-vertical-align-containers', $menu_area_grid_header_standard_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_header_standard_menu_area_styles');
}

if(!function_exists('malmo_elated_header_minimal_menu_area_styles')) {
	/**
	 * Generates styles for header minimal menu
	 */
	function malmo_elated_header_minimal_menu_area_styles() {
		global $malmo_options;

		$menu_area_header_minimal_styles = array();

		if($malmo_options['menu_area_background_color_header_minimal'] !== '') {
			$menu_area_background_color        = $malmo_options['menu_area_background_color_header_minimal'];
			$menu_area_background_transparency = 1;

			if($malmo_options['menu_area_background_transparency_header_minimal'] !== '') {
				$menu_area_background_transparency = $malmo_options['menu_area_background_transparency_header_minimal'];
			}

			$menu_area_header_minimal_styles['background-color'] = malmo_elated_rgba_color($menu_area_background_color, $menu_area_background_transparency);
		}

		if($malmo_options['menu_area_height_header_minimal'] !== '') {
			$max_height = intval(malmo_elated_filter_px($malmo_options['menu_area_height_header_minimal']) * 0.9).'px';
			echo malmo_elated_dynamic_css('.eltd-header-minimal .eltd-page-header .eltd-logo-wrapper a', array('max-height' => $max_height));

			$menu_area_header_minimal_styles['height'] = malmo_elated_filter_px($malmo_options['menu_area_height_header_minimal']).'px';

		}

		echo malmo_elated_dynamic_css('.eltd-header-minimal .eltd-page-header .eltd-menu-area', $menu_area_header_minimal_styles);

		$menu_area_grid_header_minimal_styles = array();

		if($malmo_options['menu_area_in_grid_header_minimal'] == 'yes' && $malmo_options['menu_area_grid_background_color_header_minimal'] !== '') {
			$menu_area_grid_background_color        = $malmo_options['menu_area_grid_background_color_header_minimal'];
			$menu_area_grid_background_transparency = 1;

			if($malmo_options['menu_area_grid_background_transparency_header_minimal'] !== '') {
				$menu_area_grid_background_transparency = $malmo_options['menu_area_grid_background_transparency_header_minimal'];
			}

			$menu_area_grid_header_minimal_styles['background-color'] = malmo_elated_rgba_color($menu_area_grid_background_color, $menu_area_grid_background_transparency);
		}

		echo malmo_elated_dynamic_css('.eltd-header-minimal .eltd-page-header .eltd-menu-area .eltd-grid .eltd-vertical-align-containers', $menu_area_grid_header_minimal_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_header_minimal_menu_area_styles');
}

if(!function_exists('malmo_elated_header_centered_logo_area_styles')) {
	/**
	 * Generates styles for header centered menu
	 */
	function malmo_elated_header_centered_logo_area_styles() {
		global $malmo_options;

		$logo_area_header_centered_styles = array();

		if($malmo_options['logo_area_background_color_header_centered'] !== '') {
			$logo_area_background_color        = $malmo_options['logo_area_background_color_header_centered'];
			$logo_area_background_transparency = 1;

			if($malmo_options['logo_area_background_transparency_header_centered'] !== '') {
				$logo_area_background_transparency = $malmo_options['logo_area_background_transparency_header_centered'];
			}

			$logo_area_header_centered_styles['background-color'] = malmo_elated_rgba_color($logo_area_background_color, $logo_area_background_transparency);
		}
		
		if($malmo_options['logo_area_height_header_centered'] !== '') {
			$max_height = intval(malmo_elated_filter_px($malmo_options['logo_area_height_header_centered']) * 0.9).'px';
			echo malmo_elated_dynamic_css('.eltd-header-centered .eltd-page-header .eltd-logo-wrapper a', array('max-height' => $max_height));

			$logo_area_header_centered_styles['height'] = malmo_elated_filter_px($malmo_options['logo_area_height_header_centered']).'px';

		}

		echo malmo_elated_dynamic_css('.eltd-header-centered .eltd-page-header .eltd-logo-area', $logo_area_header_centered_styles);

		$logo_area_grid_header_centered_styles = array();

		if($malmo_options['logo_area_in_grid_header_centered'] == 'yes' && $malmo_options['logo_area_grid_background_color_header_centered'] !== '') {
			$logo_area_grid_background_color        = $malmo_options['logo_area_grid_background_color_header_centered'];
			$logo_area_grid_background_transparency = 1;

			if($malmo_options['logo_area_grid_background_transparency_header_centered'] !== '') {
				$logo_area_grid_background_transparency = $malmo_options['logo_area_grid_background_transparency_header_centered'];
			}

			$logo_area_grid_header_centered_styles['background-color'] = malmo_elated_rgba_color($logo_area_grid_background_color, $logo_area_grid_background_transparency);
		}

		if(malmo_elated_options()->getOptionValue('logo_area_in_grid_border_header_centered') == 'yes' &&
		   malmo_elated_options()->getOptionValue('logo_area_in_grid_border_color_header_centered') !== ''
		) {

			$logo_area_grid_header_centered_styles['border-bottom'] = '1px solid '.malmo_elated_options()->getOptionValue('logo_area_in_grid_border_color_header_centered');

		} else if(malmo_elated_options()->getOptionValue('logo_area_in_grid_border_header_centered') == 'no') {
			$logo_area_grid_header_centered_styles['border'] = '0';
		}

		echo malmo_elated_dynamic_css('.eltd-header-centered .eltd-page-header .eltd-logo-area .eltd-grid .eltd-vertical-align-containers', $logo_area_grid_header_centered_styles);

		if(malmo_elated_options()->getOptionValue('logo_wrapper_padding_header_centered') !== '') {
			echo malmo_elated_dynamic_css('.eltd-header-centered .eltd-logo-area .eltd-logo-wrapper', array('padding'=>malmo_elated_options()->getOptionValue('logo_wrapper_padding_header_centered')));
		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_header_centered_logo_area_styles');
}

if(!function_exists('malmo_elated_header_centered_menu_area_styles')) {
	/**
	 * Generates styles for header centered menu
	 */
	function malmo_elated_header_centered_menu_area_styles() {
		global $malmo_options;

		$menu_area_header_centered_styles = array();

		if($malmo_options['menu_area_background_color_header_centered'] !== '') {
			$menu_area_background_color        = $malmo_options['menu_area_background_color_header_centered'];
			$menu_area_background_transparency = 1;

			if($malmo_options['menu_area_background_transparency_header_centered'] !== '') {
				$menu_area_background_transparency = $malmo_options['menu_area_background_transparency_header_centered'];
			}

			$menu_area_header_centered_styles['background-color'] = malmo_elated_rgba_color($menu_area_background_color, $menu_area_background_transparency);
		}

		if(malmo_elated_options()->getOptionValue('menu_area_border_header_centered') == 'yes' &&
		   malmo_elated_options()->getOptionValue('menu_area_border_color_header_centered') !== ''
		) {

			$menu_area_header_centered_styles['border-bottom'] = '1px solid '.malmo_elated_options()->getOptionValue('menu_area_border_color_header_centered');
		}

		if($malmo_options['menu_area_height_header_centered'] !== '') {

			$menu_area_header_centered_styles['height'] = malmo_elated_filter_px($malmo_options['menu_area_height_header_centered']).'px';

		}

		echo malmo_elated_dynamic_css('.eltd-header-centered .eltd-page-header .eltd-menu-area', $menu_area_header_centered_styles);

		$menu_area_grid_header_centered_styles = array();

		if($malmo_options['menu_area_in_grid_header_centered'] == 'yes' && $malmo_options['menu_area_grid_background_color_header_centered'] !== '') {
			$menu_area_grid_background_color        = $malmo_options['menu_area_grid_background_color_header_centered'];
			$menu_area_grid_background_transparency = 1;

			if($malmo_options['menu_area_grid_background_transparency_header_centered'] !== '') {
				$menu_area_grid_background_transparency = $malmo_options['menu_area_grid_background_transparency_header_centered'];
			}

			$menu_area_grid_header_centered_styles['background-color'] = malmo_elated_rgba_color($menu_area_grid_background_color, $menu_area_grid_background_transparency);
		}

		if(malmo_elated_options()->getOptionValue('menu_area_in_grid_border_header_centered') == 'yes' &&
		   malmo_elated_options()->getOptionValue('menu_area_in_grid_border_color_header_centered') !== ''
		) {

			$menu_area_grid_header_centered_styles['border-bottom'] = '1px solid '.malmo_elated_options()->getOptionValue('menu_area_in_grid_border_color_header_centered');

		} else if(malmo_elated_options()->getOptionValue('menu_area_in_grid_border_header_centered') == 'no') {
			$menu_area_grid_header_centered_styles['border'] = '0';
		}

		echo malmo_elated_dynamic_css('.eltd-header-centered .eltd-page-header .eltd-menu-area .eltd-grid .eltd-vertical-align-containers', $menu_area_grid_header_centered_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_header_centered_menu_area_styles');
}

if(!function_exists('malmo_elated_vertical_menu_styles')) {
	/**
	 * Generates styles for sticky haeder
	 */
	function malmo_elated_vertical_menu_styles() {

		$vertical_header_styles = array();

		$vertical_header_selectors = array(
			'.eltd-header-vertical .eltd-vertical-area-background'
		);

		if(malmo_elated_options()->getOptionValue('vertical_header_background_color') !== '') {
			$vertical_header_styles['background-color'] = malmo_elated_options()->getOptionValue('vertical_header_background_color');
		}

		if(malmo_elated_options()->getOptionValue('vertical_header_transparency') !== '') {
			$vertical_header_styles['opacity'] = malmo_elated_options()->getOptionValue('vertical_header_transparency');
		}

		if(malmo_elated_options()->getOptionValue('vertical_header_background_image') !== '') {
			$vertical_header_styles['background-image'] = 'url('.malmo_elated_options()->getOptionValue('vertical_header_background_image').')';
		}


		echo malmo_elated_dynamic_css($vertical_header_selectors, $vertical_header_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_vertical_menu_styles');
}


if(!function_exists('malmo_elated_sticky_header_styles')) {
	/**
	 * Generates styles for sticky haeder
	 */
	function malmo_elated_sticky_header_styles() {
		global $malmo_options;

		if($malmo_options['sticky_header_in_grid'] == 'yes' && $malmo_options['sticky_header_grid_background_color'] !== '') {
			$sticky_header_grid_background_color        = $malmo_options['sticky_header_grid_background_color'];
			$sticky_header_grid_background_transparency = 1;

			if($malmo_options['sticky_header_grid_transparency'] !== '') {
				$sticky_header_grid_background_transparency = $malmo_options['sticky_header_grid_transparency'];
			}

			echo malmo_elated_dynamic_css('.eltd-page-header .eltd-sticky-header .eltd-grid .eltd-vertical-align-containers', array('background-color' => malmo_elated_rgba_color($sticky_header_grid_background_color, $sticky_header_grid_background_transparency)));
		}

		if($malmo_options['sticky_header_background_color'] !== '') {

			$sticky_header_background_color              = $malmo_options['sticky_header_background_color'];
			$sticky_header_background_color_transparency = 1;

			if($malmo_options['sticky_header_transparency'] !== '') {
				$sticky_header_background_color_transparency = $malmo_options['sticky_header_transparency'];
			}

			echo malmo_elated_dynamic_css('.eltd-page-header .eltd-sticky-header .eltd-sticky-holder', array('background-color' => malmo_elated_rgba_color($sticky_header_background_color, $sticky_header_background_color_transparency)));
		}

		if($malmo_options['sticky_header_height'] !== '') {
			$max_height = intval(malmo_elated_filter_px($malmo_options['sticky_header_height']) * 0.9).'px';

			echo malmo_elated_dynamic_css('.eltd-page-header .eltd-sticky-header', array('height' => $malmo_options['sticky_header_height'].'px'));
			echo malmo_elated_dynamic_css('.eltd-page-header .eltd-sticky-header .eltd-logo-wrapper a', array('max-height' => $max_height));
		}

		$sticky_menu_item_styles = array();
		if($malmo_options['sticky_color'] !== '') {
			$sticky_menu_item_styles['color'] = $malmo_options['sticky_color'];
		}
		if($malmo_options['sticky_google_fonts'] !== '-1') {
			$sticky_menu_item_styles['font-family'] = malmo_elated_get_formatted_font_family($malmo_options['sticky_google_fonts']);
		}
		if($malmo_options['sticky_fontsize'] !== '') {
			$sticky_menu_item_styles['font-size'] = $malmo_options['sticky_fontsize'].'px';
		}
		if($malmo_options['sticky_lineheight'] !== '') {
			$sticky_menu_item_styles['line-height'] = $malmo_options['sticky_lineheight'].'px';
		}
		if($malmo_options['sticky_texttransform'] !== '') {
			$sticky_menu_item_styles['text-transform'] = $malmo_options['sticky_texttransform'];
		}
		if($malmo_options['sticky_fontstyle'] !== '') {
			$sticky_menu_item_styles['font-style'] = $malmo_options['sticky_fontstyle'];
		}
		if($malmo_options['sticky_fontweight'] !== '') {
			$sticky_menu_item_styles['font-weight'] = $malmo_options['sticky_fontweight'];
		}
		if($malmo_options['sticky_letterspacing'] !== '') {
			$sticky_menu_item_styles['letter-spacing'] = $malmo_options['sticky_letterspacing'].'px';
		}

		$sticky_menu_item_selector = array(
			'.eltd-page-header .eltd-main-menu.eltd-sticky-nav > ul > li > a'
		);

		echo malmo_elated_dynamic_css($sticky_menu_item_selector, $sticky_menu_item_styles);

		$sticky_menu_item_hover_styles = array();
		if($malmo_options['sticky_hovercolor'] !== '') {
			$sticky_menu_item_hover_styles['color'] = $malmo_options['sticky_hovercolor'];
		}

		$sticky_menu_item_hover_selector = array(
			'.eltd-main-menu.eltd-sticky-nav > ul > li:hover > a',
			'.eltd-main-menu.eltd-sticky-nav > ul > li.eltd-active-item > a',
			'body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-sticky-nav > ul > li:hover > a',
			'body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-sticky-nav > ul > li.eltd-active-item > a'
		);

		echo malmo_elated_dynamic_css($sticky_menu_item_hover_selector, $sticky_menu_item_hover_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_sticky_header_styles');
}

if(!function_exists('malmo_elated_fixed_header_styles')) {
	/**
	 * Generates styles for fixed haeder
	 */
	function malmo_elated_fixed_header_styles() {
		global $malmo_options;

		if($malmo_options['fixed_header_grid_background_color'] !== '') {

			$fixed_header_grid_background_color              = $malmo_options['fixed_header_grid_background_color'];
			$fixed_header_grid_background_color_transparency = 1;

			if($malmo_options['fixed_header_grid_transparency'] !== '') {
				$fixed_header_grid_background_color_transparency = $malmo_options['fixed_header_grid_transparency'];
			}

			echo malmo_elated_dynamic_css('.eltd-header-type1 .eltd-fixed-wrapper.fixed .eltd-grid .eltd-vertical-align-containers,
                                    .eltd-header-type3 .eltd-fixed-wrapper.fixed .eltd-grid .eltd-vertical-align-containers',
				array('background-color' => malmo_elated_rgba_color($fixed_header_grid_background_color, $fixed_header_grid_background_color_transparency)));
		}

		if($malmo_options['fixed_header_background_color'] !== '') {

			$fixed_header_background_color              = $malmo_options['fixed_header_background_color'];
			$fixed_header_background_color_transparency = 1;

			if($malmo_options['fixed_header_transparency'] !== '') {
				$fixed_header_background_color_transparency = $malmo_options['fixed_header_transparency'];
			}

			echo malmo_elated_dynamic_css('.eltd-header-type1 .eltd-fixed-wrapper.fixed .eltd-menu-area,
                                    .eltd-header-type3 .eltd-fixed-wrapper.fixed .eltd-menu-area',
				array('background-color' => malmo_elated_rgba_color($fixed_header_background_color, $fixed_header_background_color_transparency)));
		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_fixed_header_styles');
}

if(!function_exists('malmo_elated_main_menu_styles')) {
	/**
	 * Generates styles for main menu
	 */
	function malmo_elated_main_menu_styles() {
		global $malmo_options;

		if($malmo_options['menu_color'] !== '' || $malmo_options['menu_fontsize'] != '' || $malmo_options['menu_fontstyle'] !== '' || $malmo_options['menu_fontweight'] !== '' || $malmo_options['menu_texttransform'] !== '' || $malmo_options['menu_letterspacing'] !== '' || $malmo_options['menu_google_fonts'] != "-1") { ?>
			.eltd-main-menu.eltd-default-nav > ul > li > a,
			.eltd-page-header #lang_sel > ul > li > a,
			.eltd-page-header #lang_sel_click > ul > li > a,
			.eltd-page-header #lang_sel ul > li:hover > a{
			<?php if($malmo_options['menu_color']) { ?> color: <?php echo esc_attr($malmo_options['menu_color']); ?>; <?php } ?>
			<?php if($malmo_options['menu_google_fonts'] != "-1") { ?>
				font-family: '<?php echo esc_attr(str_replace('+', ' ', $malmo_options['menu_google_fonts'])); ?>', sans-serif;
			<?php } ?>
			<?php if($malmo_options['menu_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($malmo_options['menu_fontsize']); ?>px; <?php } ?>
			<?php if($malmo_options['menu_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($malmo_options['menu_fontstyle']); ?>; <?php } ?>
			<?php if($malmo_options['menu_fontweight'] !== '') { ?> font-weight: <?php echo esc_attr($malmo_options['menu_fontweight']); ?>; <?php } ?>
			<?php if($malmo_options['menu_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($malmo_options['menu_texttransform']); ?>;  <?php } ?>
			<?php if($malmo_options['menu_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($malmo_options['menu_letterspacing']); ?>px; <?php } ?>
			}
		<?php } ?>

		<?php if($malmo_options['menu_google_fonts'] != "-1") { ?>
			.eltd-page-header #lang_sel_list{
			font-family: '<?php echo esc_attr(str_replace('+', ' ', $malmo_options['menu_google_fonts'])); ?>', sans-serif !important;
			}
		<?php } ?>

		<?php if($malmo_options['menu_hovercolor'] !== '') { ?>
			.eltd-main-menu.eltd-default-nav > ul > li:hover > a,
			.eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item:hover > a,
			body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-default-nav > ul > li:hover > a,
			body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item:hover > a,
			.eltd-page-header #lang_sel ul li a:hover,
			.eltd-page-header #lang_sel_click > ul > li a:hover{
			color: <?php echo esc_attr($malmo_options['menu_hovercolor']); ?> !important;
			}
		<?php } ?>

		<?php if($malmo_options['menu_activecolor'] !== '') { ?>
			.eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item > a,
			body:not(.eltd-menu-item-first-level-bg-color) .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item > a{
			color: <?php echo esc_attr($malmo_options['menu_activecolor']); ?>;
			}
		<?php } ?>

		<?php if($malmo_options['menu_text_background_color'] !== '') { ?>
			.eltd-main-menu.eltd-default-nav > ul > li > a span.item_inner{
			background-color: <?php echo esc_attr($malmo_options['menu_text_background_color']); ?>;
			}
		<?php } ?>

		<?php if($malmo_options['menu_hover_background_color'] !== '') {
			$menu_hover_background_color = $malmo_options['menu_hover_background_color'];

			if($malmo_options['menu_hover_background_color_transparency'] !== '') {
				$menu_hover_background_color_rgb = malmo_elated_hex2rgb($menu_hover_background_color);
				$menu_hover_background_color     = 'rgba('.$menu_hover_background_color_rgb[0].', '.$menu_hover_background_color_rgb[1].', '.$menu_hover_background_color_rgb[2].', '.$malmo_options['menu_hover_background_color_transparency'].')';
			} ?>

			.eltd-main-menu.eltd-default-nav > ul > li:hover > a span.item_inner,
			.eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item:hover > a span.item_inner {
			background-color: <?php echo esc_attr($menu_hover_background_color); ?>;
			}
		<?php } ?>

		<?php if($malmo_options['menu_active_background_color'] !== '') {
			$menu_active_background_color = $malmo_options['menu_active_background_color'];

			if($malmo_options['menu_active_background_color_transparency'] !== '') {
				$menu_active_background_color_rgb = malmo_elated_hex2rgb($menu_active_background_color);
				$menu_active_background_color     = 'rgba('.$menu_active_background_color_rgb[0].', '.$menu_active_background_color_rgb[1].', '.$menu_active_background_color_rgb[2].', '.$malmo_options['menu_active_background_color_transparency'].')';
			}
			?>
			.eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item > a span.item_inner {
			background-color: <?php echo esc_attr($menu_active_background_color); ?>;
			}
		<?php } ?>


		<?php if($malmo_options['menu_light_hovercolor'] !== '') { ?>
			.light .eltd-main-menu.eltd-default-nav > ul > li:hover > a,
			.light .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item:hover > a{
			color: <?php echo esc_attr($malmo_options['menu_light_hovercolor']); ?> !important;
			}
		<?php } ?>

		<?php if($malmo_options['menu_light_activecolor'] !== '') { ?>
			.light .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item > a{
			color: <?php echo esc_attr($malmo_options['menu_light_activecolor']); ?> !important;
			}
		<?php } ?>

		<?php if($malmo_options['menu_dark_hovercolor'] !== '') { ?>
			.dark .eltd-main-menu.eltd-default-nav > ul > li:hover > a,
			.dark .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item:hover > a{
			color: <?php echo esc_attr($malmo_options['menu_dark_hovercolor']); ?> !important;
			}
		<?php } ?>

		<?php if($malmo_options['menu_dark_activecolor'] !== '') { ?>
			.dark .eltd-main-menu.eltd-default-nav > ul > li.eltd-active-item > a{
			color: <?php echo esc_attr($malmo_options['menu_dark_activecolor']); ?>;
			}
		<?php } ?>

		<?php if($malmo_options['menu_lineheight'] != "" || $malmo_options['menu_padding_left_right'] !== '') { ?>
			.eltd-main-menu.eltd-default-nav > ul > li > a span.item_inner{
			<?php if($malmo_options['menu_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($malmo_options['menu_lineheight']); ?>px; <?php } ?>
			<?php if($malmo_options['menu_padding_left_right']) { ?> padding: 0  <?php echo esc_attr($malmo_options['menu_padding_left_right']); ?>px; <?php } ?>
			}
		<?php } ?>

		<?php if($malmo_options['menu_margin_left_right'] !== '') { ?>
			.eltd-main-menu.eltd-default-nav > ul > li{
			margin: 0  <?php echo esc_attr($malmo_options['menu_margin_left_right']); ?>px;
			}
		<?php } ?>

		<?php
		if($malmo_options['dropdown_background_color'] != "" || $malmo_options['dropdown_background_transparency'] != "") {

			//dropdown background and transparency styles
			$dropdown_bg_color_initial        = '#ffffff';
			$dropdown_bg_transparency_initial = 1;

			$dropdown_bg_color        = $malmo_options['dropdown_background_color'] !== "" ? $malmo_options['dropdown_background_color'] : $dropdown_bg_color_initial;
			$dropdown_bg_transparency = $malmo_options['dropdown_background_transparency'] !== "" ? $malmo_options['dropdown_background_transparency'] : $dropdown_bg_transparency_initial;

			$dropdown_bg_color_rgb = malmo_elated_hex2rgb($dropdown_bg_color);

			?>

            .eltd-drop-down .second .inner ul,
			.eltd-drop-down .second .inner ul,
			.eltd-drop-down .second .inner ul li ul,
			.shopping_cart_dropdown,
			li.narrow .second .inner ul,
			.eltd-main-menu.eltd-default-nav #lang_sel ul ul,
			.eltd-main-menu.eltd-default-nav #lang_sel_click  ul ul,
			.header-widget.widget_nav_menu ul ul,
			.eltd-drop-down .wide.wide_background .second{
			background-color: <?php echo esc_attr($dropdown_bg_color); ?>;
			background-color: rgba(<?php echo esc_attr($dropdown_bg_color_rgb[0]); ?>,<?php echo esc_attr($dropdown_bg_color_rgb[1]); ?>,<?php echo esc_attr($dropdown_bg_color_rgb[2]); ?>,<?php echo esc_attr($dropdown_bg_transparency); ?>);
			}

		<?php } //end dropdown background and transparency styles ?>

		<?php
		if($malmo_options['dropdown_top_padding'] !== '') {

			if($malmo_options['dropdown_top_padding'] !== '') {
				?>
				li.narrow .second .inner ul,
				.eltd-drop-down .wide .second .inner > ul{
				padding-top: <?php echo esc_attr($malmo_options['dropdown_top_padding']); ?>px;
				}
			<?php } ?>
		<?php } ?>

		<?php if($malmo_options['dropdown_bottom_padding'] !== '') { ?>
			li.narrow .second .inner ul,
			.eltd-drop-down .wide .second .inner > ul{
			padding-bottom: <?php echo esc_attr($malmo_options['dropdown_bottom_padding']); ?>px;
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_top_position'] !== '') { ?>
			header .eltd-drop-down .second {
			top: <?php echo esc_attr($malmo_options['dropdown_top_position']).'%;'; ?>
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_color'] !== '' || $malmo_options['dropdown_fontsize'] !== '' || $malmo_options['dropdown_lineheight'] !== '' || $malmo_options['dropdown_fontstyle'] !== '' || $malmo_options['dropdown_fontweight'] !== '' || $malmo_options['dropdown_google_fonts'] != "-1" || $malmo_options['dropdown_texttransform'] !== '' || $malmo_options['dropdown_letterspacing'] !== '') { ?>
			.eltd-drop-down .second .inner > ul > li > a,
			.eltd-drop-down .second .inner > ul > li > h4,
			.eltd-drop-down .wide .second .inner > ul > li > h4,
			.eltd-drop-down .wide .second .inner > ul > li > a,
			.eltd-drop-down .wide .second ul li ul li.menu-item-has-children > a,
			.eltd-drop-down .wide .second .inner ul li.sub ul li.menu-item-has-children > a,
			.eltd-drop-down .wide .second .inner > ul li.sub .flexslider ul li  h4 a,
			.eltd-drop-down .wide .second .inner > ul li .flexslider ul li  h4 a,
			.eltd-drop-down .wide .second .inner > ul li.sub .flexslider ul li  h4,
			.eltd-drop-down .wide .second .inner > ul li .flexslider ul li  h4,
			.eltd-main-menu.eltd-default-nav #lang_sel ul li li a,
			.eltd-main-menu.eltd-default-nav #lang_sel_click ul li ul li a,
			.eltd-main-menu.eltd-default-nav #lang_sel ul ul a,
			.eltd-main-menu.eltd-default-nav #lang_sel_click ul ul a{
			<?php if(!empty($malmo_options['dropdown_color'])) { ?> color: <?php echo esc_attr($malmo_options['dropdown_color']); ?>; <?php } ?>
			<?php if($malmo_options['dropdown_google_fonts'] != "-1") { ?>
				font-family: '<?php echo esc_attr(str_replace('+', ' ', $malmo_options['dropdown_google_fonts'])); ?>', sans-serif !important;
			<?php } ?>
			<?php if($malmo_options['dropdown_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($malmo_options['dropdown_fontsize']); ?>px; <?php } ?>
			<?php if($malmo_options['dropdown_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($malmo_options['dropdown_lineheight']); ?>px; <?php } ?>
			<?php if($malmo_options['dropdown_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($malmo_options['dropdown_fontstyle']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_fontweight'] !== '') { ?>font-weight: <?php echo esc_attr($malmo_options['dropdown_fontweight']); ?>; <?php } ?>
			<?php if($malmo_options['dropdown_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($malmo_options['dropdown_texttransform']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($malmo_options['dropdown_letterspacing']); ?>px;  <?php } ?>
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_color'] !== '') { ?>
			.shopping_cart_dropdown ul li
			.item_info_holder .item_left a,
			.shopping_cart_dropdown ul li .item_info_holder .item_right .amount,
			.shopping_cart_dropdown .cart_bottom .subtotal_holder .total,
			.shopping_cart_dropdown .cart_bottom .subtotal_holder .total_amount{
			color: <?php echo esc_attr($malmo_options['dropdown_color']); ?>;
			}
		<?php } ?>

		<?php if(!empty($malmo_options['dropdown_hovercolor'])) { ?>
			.eltd-drop-down .second .inner > ul > li:hover > a,
			.eltd-drop-down .wide .second ul li ul li.menu-item-has-children:hover > a,
			.eltd-drop-down .wide .second .inner ul li.sub ul li.menu-item-has-children:hover > a,
			.eltd-main-menu.eltd-default-nav #lang_sel ul li li:hover a,
			.eltd-main-menu.eltd-default-nav #lang_sel_click ul li ul li:hover a,
			.eltd-main-menu.eltd-default-nav #lang_sel ul li:hover > a,
			.eltd-main-menu.eltd-default-nav #lang_sel_click ul li:hover > a{
			color: <?php echo esc_attr($malmo_options['dropdown_hovercolor']); ?> !important;
			}
		<?php } ?>

		<?php if(!empty($malmo_options['dropdown_background_hovercolor'])) { ?>
			.eltd-drop-down li:not(.wide) .second .inner > ul > li:hover{
			background-color: <?php echo esc_attr($malmo_options['dropdown_background_hovercolor']); ?>;
			}
		<?php } ?>

		<?php if(!empty($malmo_options['dropdown_padding_top_bottom'])) { ?>
			.eltd-drop-down .wide .second>.inner>ul>li.sub>ul>li>a,
			.eltd-drop-down .second .inner ul li a,
			.eltd-drop-down .wide .second ul li a,
			.eltd-drop-down .second .inner ul.right li a{
			padding-top: <?php echo esc_attr($malmo_options['dropdown_padding_top_bottom']); ?>px;
			padding-bottom: <?php echo esc_attr($malmo_options['dropdown_padding_top_bottom']); ?>px;
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_wide_color'] !== '' || $malmo_options['dropdown_wide_fontsize'] !== '' || $malmo_options['dropdown_wide_lineheight'] !== '' || $malmo_options['dropdown_wide_fontstyle'] !== '' || $malmo_options['dropdown_wide_fontweight'] !== '' || $malmo_options['dropdown_wide_google_fonts'] !== "-1" || $malmo_options['dropdown_wide_texttransform'] !== '' || $malmo_options['dropdown_wide_letterspacing'] !== '') { ?>
			.eltd-drop-down .wide .second .inner > ul > li > a{
			<?php if($malmo_options['dropdown_wide_color'] !== '') { ?> color: <?php echo esc_attr($malmo_options['dropdown_wide_color']); ?>; <?php } ?>
			<?php if($malmo_options['dropdown_wide_google_fonts'] != "-1") { ?>
				font-family: '<?php echo esc_attr(str_replace('+', ' ', $malmo_options['dropdown_wide_google_fonts'])); ?>', sans-serif !important;
			<?php } ?>
			<?php if($malmo_options['dropdown_wide_fontsize'] !== '') { ?> font-size: <?php echo esc_attr($malmo_options['dropdown_wide_fontsize']); ?>px; <?php } ?>
			<?php if($malmo_options['dropdown_wide_lineheight'] !== '') { ?> line-height: <?php echo esc_attr($malmo_options['dropdown_wide_lineheight']); ?>px; <?php } ?>
			<?php if($malmo_options['dropdown_wide_fontstyle'] !== '') { ?> font-style: <?php echo esc_attr($malmo_options['dropdown_wide_fontstyle']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_wide_fontweight'] !== '') { ?>font-weight: <?php echo esc_attr($malmo_options['dropdown_wide_fontweight']); ?>; <?php } ?>
			<?php if($malmo_options['dropdown_wide_texttransform'] !== '') { ?> text-transform: <?php echo esc_attr($malmo_options['dropdown_wide_texttransform']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_wide_letterspacing'] !== '') { ?> letter-spacing: <?php echo esc_attr($malmo_options['dropdown_wide_letterspacing']); ?>px;  <?php } ?>
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_wide_hovercolor'] !== '') { ?>
			.eltd-drop-down .wide .second .inner > ul > li:hover > a {
			color: <?php echo esc_attr($malmo_options['dropdown_wide_hovercolor']); ?> !important;
			}
		<?php } ?>

		<?php if(!empty($malmo_options['dropdown_wide_background_hovercolor'])) { ?>
			.eltd-drop-down .wide .second .inner > ul > li:hover > a{
			background-color: <?php echo esc_attr($malmo_options['dropdown_wide_background_hovercolor']); ?>
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_wide_padding_top_bottom'] !== '') { ?>
			.eltd-drop-down .wide .second>.inner > ul > li.sub > ul > li > a,
			.eltd-drop-down .wide .second .inner ul li a,
			.eltd-drop-down .wide .second ul li a,
			.eltd-drop-down .wide .second .inner ul.right li a{
			padding-top: <?php echo esc_attr($malmo_options['dropdown_wide_padding_top_bottom']); ?>px;
			padding-bottom: <?php echo esc_attr($malmo_options['dropdown_wide_padding_top_bottom']); ?>px;
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_color_thirdlvl'] !== '' || $malmo_options['dropdown_fontsize_thirdlvl'] !== '' || $malmo_options['dropdown_lineheight_thirdlvl'] !== '' || $malmo_options['dropdown_fontstyle_thirdlvl'] !== '' || $malmo_options['dropdown_fontweight_thirdlvl'] !== '' || $malmo_options['dropdown_google_fonts_thirdlvl'] != "-1" || $malmo_options['dropdown_texttransform_thirdlvl'] !== '' || $malmo_options['dropdown_letterspacing_thirdlvl'] !== '') { ?>
			.eltd-drop-down .second .inner ul li.sub ul li a{
			<?php if($malmo_options['dropdown_color_thirdlvl'] !== '') { ?> color: <?php echo esc_attr($malmo_options['dropdown_color_thirdlvl']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_google_fonts_thirdlvl'] != "-1") { ?>
				font-family: '<?php echo esc_attr(str_replace('+', ' ', $malmo_options['dropdown_google_fonts_thirdlvl'])); ?>', sans-serif;
			<?php } ?>
			<?php if($malmo_options['dropdown_fontsize_thirdlvl'] !== '') { ?> font-size: <?php echo esc_attr($malmo_options['dropdown_fontsize_thirdlvl']); ?>px;  <?php } ?>
			<?php if($malmo_options['dropdown_lineheight_thirdlvl'] !== '') { ?> line-height: <?php echo esc_attr($malmo_options['dropdown_lineheight_thirdlvl']); ?>px;  <?php } ?>
			<?php if($malmo_options['dropdown_fontstyle_thirdlvl'] !== '') { ?> font-style: <?php echo esc_attr($malmo_options['dropdown_fontstyle_thirdlvl']); ?>;   <?php } ?>
			<?php if($malmo_options['dropdown_fontweight_thirdlvl'] !== '') { ?> font-weight: <?php echo esc_attr($malmo_options['dropdown_fontweight_thirdlvl']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_texttransform_thirdlvl'] !== '') { ?> text-transform: <?php echo esc_attr($malmo_options['dropdown_texttransform_thirdlvl']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_letterspacing_thirdlvl'] !== '') { ?> letter-spacing: <?php echo esc_attr($malmo_options['dropdown_letterspacing_thirdlvl']); ?>px;  <?php } ?>
			}
		<?php } ?>
		<?php if($malmo_options['dropdown_hovercolor_thirdlvl'] !== '') { ?>
			.eltd-drop-down .second .inner ul li.sub ul li:not(.flex-active-slide):hover > a:not(.flex-prev):not(.flex-next),
			.eltd-drop-down .second .inner ul li ul li:not(.flex-active-slide):hover > a:not(.flex-prev):not(.flex-next){
			color: <?php echo esc_attr($malmo_options['dropdown_hovercolor_thirdlvl']); ?> !important;
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_background_hovercolor_thirdlvl'] !== '') { ?>
			.eltd-drop-down .second .inner ul li.sub ul li:hover,
			.eltd-drop-down .second .inner ul li ul li:hover{
			background-color: <?php echo esc_attr($malmo_options['dropdown_background_hovercolor_thirdlvl']); ?>;
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_wide_color_thirdlvl'] !== '' || $malmo_options['dropdown_wide_fontsize_thirdlvl'] !== '' || $malmo_options['dropdown_wide_lineheight_thirdlvl'] !== '' || $malmo_options['dropdown_wide_fontstyle_thirdlvl'] !== '' || $malmo_options['dropdown_wide_fontweight_thirdlvl'] !== '' || $malmo_options['dropdown_wide_google_fonts_thirdlvl'] != "-1" || $malmo_options['dropdown_wide_texttransform_thirdlvl'] !== '' || $malmo_options['dropdown_wide_letterspacing_thirdlvl'] !== '') { ?>
			.eltd-drop-down .wide .second .inner ul li.sub ul li a,
			.eltd-drop-down .wide .second ul li ul li a{
			<?php if($malmo_options['dropdown_wide_color_thirdlvl'] !== '') { ?> color: <?php echo esc_attr($malmo_options['dropdown_wide_color_thirdlvl']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_wide_google_fonts_thirdlvl'] != "-1") { ?>
				font-family: '<?php echo esc_attr(str_replace('+', ' ', $malmo_options['dropdown_wide_google_fonts_thirdlvl'])); ?>', sans-serif;
			<?php } ?>
			<?php if($malmo_options['dropdown_wide_fontsize_thirdlvl'] !== '') { ?> font-size: <?php echo esc_attr($malmo_options['dropdown_wide_fontsize_thirdlvl']); ?>px;  <?php } ?>
			<?php if($malmo_options['dropdown_wide_lineheight_thirdlvl'] !== '') { ?> line-height: <?php echo esc_attr($malmo_options['dropdown_wide_lineheight_thirdlvl']); ?>px;  <?php } ?>
			<?php if($malmo_options['dropdown_wide_fontstyle_thirdlvl'] !== '') { ?> font-style: <?php echo esc_attr($malmo_options['dropdown_wide_fontstyle_thirdlvl']); ?>;   <?php } ?>
			<?php if($malmo_options['dropdown_wide_fontweight_thirdlvl'] !== '') { ?> font-weight: <?php echo esc_attr($malmo_options['dropdown_wide_fontweight_thirdlvl']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_wide_texttransform_thirdlvl'] !== '') { ?> text-transform: <?php echo esc_attr($malmo_options['dropdown_wide_texttransform_thirdlvl']); ?>;  <?php } ?>
			<?php if($malmo_options['dropdown_wide_letterspacing_thirdlvl'] !== '') { ?> letter-spacing: <?php echo esc_attr($malmo_options['dropdown_wide_letterspacing_thirdlvl']); ?>px;  <?php } ?>
			}
		<?php } ?>
		<?php if($malmo_options['dropdown_wide_hovercolor_thirdlvl'] !== '') { ?>
			.eltd-drop-down .wide .second .inner ul li.sub ul li:not(.flex-active-slide) > a:not(.flex-prev):not(.flex-next):hover,
			.eltd-drop-down .wide .second .inner ul li ul li:not(.flex-active-slide) > a:not(.flex-prev):not(.flex-next):hover{
			color: <?php echo esc_attr($malmo_options['dropdown_wide_hovercolor_thirdlvl']); ?> !important;
			}
		<?php } ?>

		<?php if($malmo_options['dropdown_wide_background_hovercolor_thirdlvl'] !== '') { ?>
			.eltd-drop-down .wide .second .inner ul li.sub ul li:hover,
			.eltd-drop-down .wide .second .inner ul li ul li:hover{
			background-color: <?php echo esc_attr($malmo_options['dropdown_wide_background_hovercolor_thirdlvl']); ?>;
			}
		<?php }
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_main_menu_styles');
}

if(!function_exists('malmo_elated_vertical_main_menu_styles')) {
	/**
	 * Generates styles for vertical main main menu
	 */
	function malmo_elated_vertical_main_menu_styles() {
		$dropdown_styles = array();

		if(malmo_elated_options()->getOptionValue('vertical_dropdown_background_color') !== '') {
			$dropdown_styles['background-color'] = malmo_elated_options()->getOptionValue('vertical_dropdown_background_color');
		}

		$dropdown_selector = array(
			'.eltd-header-vertical .eltd-vertical-dropdown-float .menu-item .second',
			'.eltd-header-vertical .eltd-vertical-dropdown-float .second .inner ul ul'
		);

		echo malmo_elated_dynamic_css($dropdown_selector, $dropdown_styles);

		$fist_level_styles       = array();
		$fist_level_hover_styles = array();

		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_color') !== '') {
			$fist_level_styles['color'] = malmo_elated_options()->getOptionValue('vertical_menu_1st_color');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_google_fonts') !== '-1') {
			$fist_level_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('vertical_menu_1st_google_fonts'));
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_fontsize') !== '') {
			$fist_level_styles['font-size'] = malmo_elated_options()->getOptionValue('vertical_menu_1st_fontsize').'px';
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_lineheight') !== '') {
			$fist_level_styles['line-height'] = malmo_elated_options()->getOptionValue('vertical_menu_1st_lineheight').'px';
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_texttransform') !== '') {
			$fist_level_styles['text-transform'] = malmo_elated_options()->getOptionValue('vertical_menu_1st_texttransform');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_fontstyle') !== '') {
			$fist_level_styles['font-style'] = malmo_elated_options()->getOptionValue('vertical_menu_1st_fontstyle');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_fontweight') !== '') {
			$fist_level_styles['font-weight'] = malmo_elated_options()->getOptionValue('vertical_menu_1st_fontweight');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_letter_spacing') !== '') {
			$fist_level_styles['letter-spacing'] = malmo_elated_options()->getOptionValue('vertical_menu_1st_letter_spacing').'px';
		}

		if(malmo_elated_options()->getOptionValue('vertical_menu_1st_hover_color') !== '') {
			$fist_level_hover_styles['color'] = malmo_elated_options()->getOptionValue('vertical_menu_1st_hover_color');
		}

		$first_level_selector       = array(
			'.eltd-header-vertical .eltd-vertical-menu > ul > li > a'
		);
		$first_level_hover_selector = array(
			'.eltd-header-vertical .eltd-vertical-menu > ul > li > a:hover',
			'.eltd-header-vertical .eltd-vertical-menu > ul > li > a.eltd-active-item'
		);

		echo malmo_elated_dynamic_css($first_level_selector, $fist_level_styles);
		echo malmo_elated_dynamic_css($first_level_hover_selector, $fist_level_hover_styles);

		$second_level_styles       = array();
		$second_level_hover_styles = array();

		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_color') !== '') {
			$second_level_styles['color'] = malmo_elated_options()->getOptionValue('vertical_menu_2nd_color');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_google_fonts') !== '-1') {
			$second_level_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('vertical_menu_2nd_google_fonts'));
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_fontsize') !== '') {
			$second_level_styles['font-size'] = malmo_elated_options()->getOptionValue('vertical_menu_2nd_fontsize').'px';
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_lineheight') !== '') {
			$second_level_styles['line-height'] = malmo_elated_options()->getOptionValue('vertical_menu_2nd_lineheight').'px';
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_texttransform') !== '') {
			$second_level_styles['text-transform'] = malmo_elated_options()->getOptionValue('vertical_menu_2nd_texttransform');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_fontstyle') !== '') {
			$second_level_styles['font-style'] = malmo_elated_options()->getOptionValue('vertical_menu_2nd_fontstyle');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_fontweight') !== '') {
			$second_level_styles['font-weight'] = malmo_elated_options()->getOptionValue('vertical_menu_2nd_fontweight');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_letter_spacing') !== '') {
			$second_level_styles['letter-spacing'] = malmo_elated_options()->getOptionValue('vertical_menu_2nd_letter_spacing').'px';
		}

		if(malmo_elated_options()->getOptionValue('vertical_menu_2nd_hover_color') !== '') {
			$second_level_hover_styles['color'] = malmo_elated_options()->getOptionValue('vertical_menu_2nd_hover_color');
		}

		$second_level_selector = array(
			'.eltd-header-vertical .eltd-vertical-menu .second .inner > ul > li > a'
		);

		$second_level_hover_selector = array(
			'.eltd-header-vertical .eltd-vertical-menu .second .inner > ul > li > a:hover',
			'.eltd-header-vertical .eltd-vertical-menu .second .inner > ul > li > a.eltd-active-item'
		);

		echo malmo_elated_dynamic_css($second_level_selector, $second_level_styles);
		echo malmo_elated_dynamic_css($second_level_hover_selector, $second_level_hover_styles);

		$third_level_styles       = array();
		$third_level_hover_styles = array();

		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_color') !== '') {
			$third_level_styles['color'] = malmo_elated_options()->getOptionValue('vertical_menu_3rd_color');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_google_fonts') !== '-1') {
			$third_level_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('vertical_menu_3rd_google_fonts'));
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_fontsize') !== '') {
			$third_level_styles['font-size'] = malmo_elated_options()->getOptionValue('vertical_menu_3rd_fontsize').'px';
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_lineheight') !== '') {
			$third_level_styles['line-height'] = malmo_elated_options()->getOptionValue('vertical_menu_3rd_lineheight').'px';
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_texttransform') !== '') {
			$third_level_styles['text-transform'] = malmo_elated_options()->getOptionValue('vertical_menu_3rd_texttransform');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_fontstyle') !== '') {
			$third_level_styles['font-style'] = malmo_elated_options()->getOptionValue('vertical_menu_3rd_fontstyle');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_fontweight') !== '') {
			$third_level_styles['font-weight'] = malmo_elated_options()->getOptionValue('vertical_menu_3rd_fontweight');
		}
		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_letter_spacing') !== '') {
			$third_level_styles['letter-spacing'] = malmo_elated_options()->getOptionValue('vertical_menu_3rd_letter_spacing').'px';
		}

		if(malmo_elated_options()->getOptionValue('vertical_menu_3rd_hover_color') !== '') {
			$third_level_hover_styles['color'] = malmo_elated_options()->getOptionValue('vertical_menu_3rd_hover_color');
		}

		$third_level_selector = array(
			'.eltd-header-vertical .eltd-vertical-menu .second .inner ul li ul li a'
		);

		$third_level_hover_selector = array(
			'.eltd-header-vertical .eltd-vertical-menu .second .inner ul li ul li a:hover',
			'.eltd-header-vertical .eltd-vertical-menu .second .inner ul li ul li a.eltd-active-item'
		);

		echo malmo_elated_dynamic_css($third_level_selector, $third_level_styles);
		echo malmo_elated_dynamic_css($third_level_hover_selector, $third_level_hover_styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_vertical_main_menu_styles');
}
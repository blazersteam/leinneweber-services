<?php do_action('malmo_elated_before_mobile_logo'); ?>

	<div class="eltd-mobile-logo-wrapper">
		<a href="<?php echo esc_url(home_url('/')); ?>" <?php malmo_elated_inline_style($logo_styles); ?>>
			<img <?php echo malmo_elated_get_inline_attrs($logo_dimensions_attr); ?> src="<?php echo esc_url($logo_image); ?>" alt="<?php esc_attr_e( 'mobile-logo', 'malmo' ); ?>"/>
		</a>
	</div>

<?php do_action('malmo_elated_after_mobile_logo'); ?>
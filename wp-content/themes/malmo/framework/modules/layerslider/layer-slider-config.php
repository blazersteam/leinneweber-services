<?php
if(!function_exists('malmo_elated_layerslider_overrides')) {
	/**
	 * Disables Layer Slider auto update box
	 */
	function malmo_elated_layerslider_overrides() {
		$GLOBALS['lsAutoUpdateBox'] = false;
	}

	add_action('layerslider_ready', 'malmo_elated_layerslider_overrides');
}
?>
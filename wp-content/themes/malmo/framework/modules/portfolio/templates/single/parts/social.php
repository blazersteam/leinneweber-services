<div class="eltd-portfolio-item-social">
    <div class="eltd-portfolio-single-likes">
        <?php echo malmo_elated_like_portfolio_list(get_the_ID()); ?>
    </div>
    <?php if(malmo_elated_options()->getOptionValue('enable_social_share') == 'yes' && malmo_elated_options()->getOptionValue('enable_social_share_on_portfolio-item') == 'yes') : ?>
        <div class="eltd-portfolio-single-share-holder">
            <?php echo malmo_elated_get_social_share_html(array('type' => 'dropdown')) ?>
        </div>
    <?php endif; ?>
</div>

<div class="eltd-fullscreen-search-holder">
    <div class="eltd-fullscreen-search-table">
        <div class="eltd-fullscreen-search-cell">
            <div class="eltd-fullscreen-search-inner">
                <form action="<?php echo esc_url(home_url('/')); ?>" class="eltd-fullscreen-search-form" method="get">
                    <div class="eltd-form-holder">
                        <div class="eltd-field-holder">
                            <input type="text" name="s" placeholder="<?php esc_attr_e('Search on site...', 'malmo'); ?>" class="eltd-search-field" autocomplete="off"/>

                            <div class="eltd-line"></div>
                            <input type="submit" class="eltd-search-submit" value="&#x55;"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
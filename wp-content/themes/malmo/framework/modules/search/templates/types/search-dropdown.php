<form action="<?php echo esc_url(home_url('/')); ?>" class="eltd-search-dropdown-holder" method="get">
	<div class="form-inner">
		<input type="text" placeholder="<?php esc_attr_e('Search...', 'malmo'); ?>" name="s" class="eltd-search-field" autocomplete="off"/>
		<input value="<?php esc_attr_e('Search', 'malmo'); ?>" type="submit" class="eltd-btn eltd-btn-solid eltd-btn-small">
	</div>
</form>
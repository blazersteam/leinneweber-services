<?php
namespace Malmo\Modules\Shortcodes\BlogSlider;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

class BlogSlider implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    function __construct() {
        $this->base = 'eltd_blog_slider';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            'name'                      => esc_html__('Blog Slider', 'malmo'),
            'base'                      => $this->base,
            'icon'                      => 'icon-wpb-blog-slider extended-custom-icon',
            'category'                  => esc_html__('by ELATED', 'malmo'),
            'allowed_container_element' => 'vc_row',
            'params'                    => array(
                array(
                    'type'        => 'textfield',
                    'holder'      => 'div',
                    'class'       => '',
                    'heading'     => esc_html__('Number of Posts', 'malmo'),
                    'param_name'  => 'number_of_posts',
                    'description' => ''
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'class'       => '',
                    'heading'     => esc_html__('Faded Inactive Posts', 'malmo'),
                    'param_name'  => 'faded_posts',
                    'value'       => array(
                        esc_html__('No', 'malmo') => 'no',
                        esc_html__('Yes', 'malmo')  => 'yes'
                    ),
                    'save_always' => true,
                    'description' => esc_html__('Current post will be displayed grid-wide and the surrounding ones will be faded.', 'malmo')
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'class'       => '',
                    'heading'     => esc_html__('Order By', 'malmo'),
                    'param_name'  => 'order_by',
                    'value'       => array(
                        esc_html__('Title', 'malmo') => 'title',
                        esc_html__('Date', 'malmo')  => 'date'
                    ),
                    'save_always' => true,
                    'description' => ''
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'class'       => '',
                    'heading'     => esc_html__('Order', 'malmo'),
                    'param_name'  => 'order',
                    'value'       => array(
                        esc_html__('ASC', 'malmo')  => 'ASC',
                        esc_html__('DESC', 'malmo') => 'DESC'
                    ),
                    'save_always' => true,
                    'description' => ''
                ),
                array(
                    'type'        => 'textfield',
                    'holder'      => 'div',
                    'class'       => '',
                    'heading'     => esc_html__('Category Slug', 'malmo'),
                    'param_name'  => 'category',
                    'description' => esc_html__('Leave empty for all or use comma for list', 'malmo')
                ),
                array(
                    'type'        => 'textfield',
                    'holder'      => 'div',
                    'class'       => '',
                    'heading'     => esc_html__('Text length', 'malmo'),
                    'param_name'  => 'text_length',
                    'description' => esc_html__('Number of characters', 'malmo')
                )
            )
        ));

    }

    public function render($atts, $content = null) {

        $default_atts = array(
            'number_of_posts' => '',
            'faded_posts'     => '',
            'order_by'        => '',
            'order'           => '',
            'category'        => '',
            'text_length'     => '90',
        );

        $params = shortcode_atts($default_atts, $atts);

        $queryParams = $this->generateBlogQueryArray($params);

        $query = new \WP_Query($queryParams);

        $params['query'] = $query;

        return malmo_elated_get_shortcode_module_template_part('templates/blog-slider-template', 'blog-slider', '', $params);
    }


    /**
     * Generates query array
     *
     * @param $params
     *
     * @return array
     */
    public function generateBlogQueryArray($params) {

        $queryArray = array(
            'orderby'        => $params['order_by'],
            'order'          => $params['order'],
            'posts_per_page' => $params['number_of_posts'],
            'category_name'  => $params['category']
        );

        return $queryArray;
    }
}
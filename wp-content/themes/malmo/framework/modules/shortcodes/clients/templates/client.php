<div class="eltd-client">
    <div class="eltd-client-inner">
        <div class="eltd-cl-hor-helper"></div>
        <div class="eltd-cl-ver-helper"></div>
        <div class="eltd-cl-image">
            <?php if($link != '') : ?>
            <a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
                <?php endif; ?>
                <?php echo wp_get_attachment_image($image, 'full', '', array("class" => "eltd-cl-img")); ?>
                <?php echo wp_get_attachment_image($hover_image, 'full', '', array("class" => "eltd-cl-hover-img")); ?>
                <?php if($link != '') : ?>
            </a>
        <?php endif; ?>
        </div>
    </div>
</div>

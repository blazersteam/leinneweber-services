<?php if(is_array($features) && count($features)) : ?>
	<div <?php malmo_elated_class_attribute($holder_classes); ?>>
		<div class="eltd-cpt-features-holder eltd-cpt-table">
			<?php if($display_border) : ?>
				<div class="eltd-cpt-table-border-top" <?php malmo_elated_inline_style($border_style); ?>></div>
			<?php endif; ?>

			<div class="eltd-cpt-features-title eltd-cpt-table-head-holder">
				<div class="eltd-cpt-table-head-holder-inner">
					<h4><?php echo wp_kses_post(preg_replace('#^<\/p>|<p>$#', '', $title)); ?></h4>
				</div>
			</div>
			<div class="eltd-cpt-features-list-holder eltd-cpt-table-content">
				<ul class="eltd-cpt-features-list">
					<?php foreach($features as $feature) : ?>
						<li class="eltd-cpt-features-item"><span><?php echo esc_html($feature); ?></span></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<?php echo do_shortcode($content); ?>
	</div>
<?php endif; ?>
<?php
/**
 * Counter shortcode template
 */
?>
<div <?php malmo_elated_class_attribute($counter_classes); ?> <?php echo malmo_elated_get_inline_style($counter_holder_styles); ?>>
	<?php if($icon != ''): ?>
		<div class="eltd-counter-icon"><?php echo malmo_elated_icon_collections()->renderIcon($icon, $icon_pack); ?></div>
	<?php endif; ?>
	<span class="eltd-counter <?php echo esc_attr($type) ?>" <?php echo malmo_elated_get_inline_style($counter_styles); ?>>
		<?php echo esc_attr($digit); ?>
	</span>
	<<?php echo esc_html($title_tag); ?> class="eltd-counter-title">
	<?php echo esc_attr($title); ?>
</<?php echo esc_html($title_tag);; ?>>
<?php if($text != "") { ?>
	<p class="eltd-counter-text"><?php echo esc_html($text); ?></p>
<?php } ?>

</div>
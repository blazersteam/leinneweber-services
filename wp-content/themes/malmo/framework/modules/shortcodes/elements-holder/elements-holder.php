<?php
namespace Malmo\Modules\ElementsHolder;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

class ElementsHolder implements ShortcodeInterface {
    private $base;

    function __construct() {
        $this->base = 'eltd_elements_holder';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'      => esc_html__('Elements Holder', 'malmo'),
            'base'      => $this->base,
            'icon'      => 'icon-wpb-elements-holder extended-custom-icon',
            'category'  => esc_html__('by ELATED', 'malmo'),
            'as_parent' => array('only' => 'eltd_elements_holder_item, eltd_info_box'),
            'js_view'   => 'VcColumnView',
            'params'    => array(
                array(
                    'type'        => 'colorpicker',
                    'class'       => '',
                    'heading'     => esc_html__('Background Color', 'malmo'),
                    'param_name'  => 'background_color',
                    'value'       => '',
                    'description' => ''
                ),
                array(
                    'type'        => 'dropdown',
                    'class'       => '',
                    'heading'     => esc_html__('Columns', 'malmo'),
                    'admin_label' => true,
                    'param_name'  => 'number_of_columns',
                    'value'       => array(
                        esc_html__('1 Column', 'malmo')  => 'one-column',
                        esc_html__('2 Columns', 'malmo') => 'two-columns',
                        esc_html__('3 Columns', 'malmo') => 'three-columns',
                        esc_html__('4 Columns', 'malmo') => 'four-columns',
                        esc_html__('5 Columns', 'malmo') => 'five-columns',
                        esc_html__('6 Columns', 'malmo') => 'six-columns'
                    ),
                    'description' => ''
                ),
                array(
                    'type'        => 'checkbox',
                    'class'       => '',
                    'heading'     => esc_html__('Items Float Left', 'malmo'),
                    'param_name'  => 'items_float_left',
                    'value'       => array('Make Items Float Left?' => 'yes'),
                    'description' => ''
                ),
                array(
                    'type'        => 'dropdown',
                    'class'       => '',
                    'group'       => esc_html__('Width & Responsiveness', 'malmo'),
                    'heading'     => esc_html__('Switch to One Column', 'malmo'),
                    'param_name'  => 'switch_to_one_column',
                    'value'       => array(
                        esc_html__('Default', 'malmo')      => '',
                        esc_html__('Below 1440px', 'malmo') => '1440',
                        esc_html__('Below 1280px', 'malmo') => '1280',
                        esc_html__('Below 1024px', 'malmo') => '1024',
                        esc_html__('Below 768px', 'malmo')  => '768',
                        esc_html__('Below 600px', 'malmo')  => '600',
                        esc_html__('Below 480px', 'malmo')  => '480',
                        esc_html__('Never', 'malmo')        => 'never'
                    ),
                    'description' => esc_html__('Choose on which stage item will be in one column', 'malmo')
                ),
                array(
                    'type'        => 'dropdown',
                    'class'       => '',
                    'group'       => esc_html__('Width & Responsiveness', 'malmo'),
                    'heading'     => esc_html__('Choose Alignment In Responsive Mode', 'malmo'),
                    'param_name'  => 'alignment_one_column',
                    'value'       => array(
                        esc_html__('Default', 'malmo') => '',
                        esc_html__('Left', 'malmo')    => 'left',
                        esc_html__('Center', 'malmo')  => 'center',
                        esc_html__('Right', 'malmo')   => 'right'
                    ),
                    'description' => esc_html__('Alignment When Items are in One Column', 'malmo')
                )
            )
        ));
    }

    public function render($atts, $content = null) {

        $args   = array(
            'number_of_columns'    => '',
            'switch_to_one_column' => '',
            'alignment_one_column' => '',
            'items_float_left'     => '',
            'background_color'     => ''
        );
        $params = shortcode_atts($args, $atts);
        extract($params);

        $html = '';

        $elements_holder_classes   = array();
        $elements_holder_classes[] = 'eltd-elements-holder';
        $elements_holder_style     = '';

        if($number_of_columns != '') {
            $elements_holder_classes[] .= 'eltd-'.$number_of_columns;
        }

        if($switch_to_one_column != '') {
            $elements_holder_classes[] = 'eltd-responsive-mode-'.$switch_to_one_column;
        } else {
            $elements_holder_classes[] = 'eltd-responsive-mode-768';
        }

        if($alignment_one_column != '') {
            $elements_holder_classes[] = 'eltd-one-column-alignment-'.$alignment_one_column;
        }

        if($items_float_left !== '') {
            $elements_holder_classes[] = 'eltd-elements-items-float';
        }

        if($background_color != '') {
            $elements_holder_style .= 'background-color:'.$background_color.';';
        }

        $elements_holder_class = implode(' ', $elements_holder_classes);

        $html .= '<div '.malmo_elated_get_class_attribute($elements_holder_class).' '.malmo_elated_get_inline_attr($elements_holder_style, 'style').'>';
        $html .= do_shortcode($content);
        $html .= '</div>';

        return $html;

    }

}

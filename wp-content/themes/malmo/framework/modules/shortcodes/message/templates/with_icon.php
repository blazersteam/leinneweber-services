<?php
$icon_html = malmo_elated_icon_collections()->renderIcon($icon, $icon_pack);
?>

<div class="eltd-message-icon-holder">
	<div class="eltd-message-icon" <?php malmo_elated_inline_style($icon_attributes); ?>>
		<div class="eltd-message-icon-inner">
			<?php
			echo malmo_elated_get_module_part( $icon_html );
			?>
		</div>
	</div>
</div>


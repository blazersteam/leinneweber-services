<div <?php malmo_elated_class_attribute($pricing_table_classes) ?>>
    <div class="eltd-price-table-outer">
        <div class="eltd-price-table-inner">
            <?php if(!empty($label)) : ?>
                <div class="eltd-pt-label-holder">
                    <div class="eltd-pt-label-inner">
                        <div class="eltd-pt-label-content">
                            <span><?php echo esc_html($label); ?></span>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="eltd-icon-pt-holder">
                <span class="eltd-separator-left"></span>
                <?php echo malmo_elated_icon_collections()->renderIcon($icon, $icon_pack); ?>
                <span class="eltd-separator-right"></span>
            </div>
            <ul>
                <li class="eltd-table-title">
                    <h3 <?php malmo_elated_inline_style($title_styles); ?> class="eltd-title-content"><?php echo esc_html($title) ?></h3>
                </li>
                <li class="eltd-table-prices">
                    <div class="eltd-price-in-table">
                        <?php if(!empty($price)) : ?>
                            <h2 class="eltd-price">
                                <sup>
                                    <?php echo esc_html($currency); ?>
                                </sup>
                                <span><?php echo esc_html($price); ?></span>
                            </h2>
                        <?php endif; ?>
                    </div>
                    <?php if(!empty($price_period)) : ?>
                        <div class="eltd-pt-price-period">
                            <span class="eltd-pt-price-period-content">/ <?php echo esc_html($price_period) ?></span>
                        </div>
                    <?php endif; ?>
                </li>
                <li class="eltd-table-content">
                    <?php echo do_shortcode(preg_replace('#^<\/p>|<p>$#', '', $content)); ?>
                </li>
                <?php
                if(is_array($button_params) && count($button_params)) { ?>
                    <li class="eltd-price-button">
                        <?php echo malmo_elated_get_button_html($button_params); ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

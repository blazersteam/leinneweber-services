<?php
namespace Malmo\Modules\Shortcodes\Process;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProcessHolder implements ShortcodeInterface {
    private $base;

    public function __construct() {
        $this->base = 'eltd_process_holder';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'                    => esc_html__('Process', 'malmo'),
            'base'                    => $this->getBase(),
            'as_parent'               => array('only' => 'eltd_process_item'),
            'content_element'         => true,
            'show_settings_on_create' => true,
            'category'                => esc_html__('by ELATED', 'malmo'),
            'icon'                    => 'icon-wpb-process extended-custom-icon',
            'js_view'                 => 'VcColumnView',
            'params'                  => array(
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'process_type',
                    'heading'     => esc_html__('Process type', 'malmo'),
                    'value'       => array(
                        esc_html__('Horizontal', 'malmo') => 'horizontal_process',
                        esc_html__('Vertical', 'malmo')   => 'vertical_process'
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => esc_html__('Icons on Process Items are available only in Horizontal Type.', 'malmo')
                ),
                array(
                    'type'        => 'dropdown',
                    'param_name'  => 'number_of_items',
                    'heading'     => esc_html__('Number of Process Items', 'malmo'),
                    'value'       => array(
                        esc_html__('Four', 'malmo')  => 'four',
                        esc_html__('Three', 'malmo') => 'three'
                    ),
                    'save_always' => true,
                    'admin_label' => true,
                    'description' => ''
                )
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'process_type'    => '',
            'number_of_items' => 'four'
        );

        $params            = shortcode_atts($default_atts, $atts);
        $params['content'] = $content;

        if($params['process_type'] == 'horizontal_process') {
            $params['holder_classes'] = array(
                'eltd-process-holder',
                'eltd-process-horizontal',
                'eltd-process-holder-items-'.$params['number_of_items']
            );

            return malmo_elated_get_shortcode_module_template_part('templates/horizontal-process-holder-template', 'process', '', $params);
        } else {
            $params['holder_classes'] = array(
                'eltd-process-holder',
                'eltd-process-vertical'
            );

            return malmo_elated_get_shortcode_module_template_part('templates/vertical-process-holder-template', 'process', '', $params);
        }
    }
}
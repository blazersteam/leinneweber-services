<div class="eltd-pl-holder <?php echo esc_attr($holder_classes) ?>">
    <div class="eltd-pl-outer">
        <?php if($type === 'masonry') { ?>
            <div class="eltd-pl-sizer"></div>
            <div class="eltd-pl-gutter"></div>
        <?php } ?>
        <?php if($query_result->have_posts()): while($query_result->have_posts()) : $query_result->the_post(); ?>
            <?php
            $masonry_image_size = get_post_meta(get_the_ID(), 'eltd_product_featured_image_size', true);
            if(empty($masonry_image_size)) {
                $masonry_image_size = '';
            }

            $new_layout = malmo_elated_get_meta_field_intersect('single_product_new');
            ?>
            <div class="eltd-pli <?php echo esc_html($masonry_image_size); ?>">
                <div class="eltd-pli-inner">
                    <div class="eltd-pli-image">
                        <?php echo malmo_elated_woocommerce_image_html_part('pli', $image_size, $new_layout); ?>
                    </div>
                    <div class="eltd-pli-text-outer">
                        <div class="eltd-pli-text-inner">

                            <?php echo malmo_elated_woocommerce_template_loop_product_title(); ?>

                            <?php echo malmo_elated_woocommerce_price_html_part('pli'); ?>

                            <?php echo malmo_elated_woocommerce_rating_html_part('pli'); ?>

                        </div>
                    </div>

                    <a class="eltd-pli-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"></a>
                </div>
            </div>
        <?php endwhile;
        else:
            malmo_elated_woocommerce_no_products_found_html_part('pli');
        endif;
        wp_reset_postdata();
        ?>
    </div>
</div>
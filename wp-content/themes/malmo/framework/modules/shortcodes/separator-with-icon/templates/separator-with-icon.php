<div class="eltd-separator-with-icon-holder clearfix">

    <div class="eltd-icon-holder">
        <span class="eltd-separator-left" <?php echo malmo_elated_get_inline_style($separator_style); ?>></span>
        <?php echo malmo_elated_icon_collections()->renderIcon($icon, $icon_pack); ?>
        <span class="eltd-separator-right" <?php echo malmo_elated_get_inline_style($separator_style); ?>></span>
    </div>
</div>
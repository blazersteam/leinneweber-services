<?php
if(!function_exists('malmo_elated_tabs_typography_styles')) {
	function malmo_elated_tabs_typography_styles() {
		$selector              = '.eltd-tabs .eltd-tabs-nav li a';
		$tabs_tipography_array = array();
		$font_family           = malmo_elated_options()->getOptionValue('tabs_font_family');

		if(malmo_elated_is_font_option_valid($font_family)) {
			$tabs_tipography_array['font-family'] = malmo_elated_is_font_option_valid($font_family);
		}

		$text_transform = malmo_elated_options()->getOptionValue('tabs_text_transform');
		if(!empty($text_transform)) {
			$tabs_tipography_array['text-transform'] = $text_transform;
		}

		$font_style = malmo_elated_options()->getOptionValue('tabs_font_style');
		if(!empty($font_style)) {
			$tabs_tipography_array['font-style'] = $font_style;
		}

		$letter_spacing = malmo_elated_options()->getOptionValue('tabs_letter_spacing');
		if($letter_spacing !== '') {
			$tabs_tipography_array['letter-spacing'] = malmo_elated_filter_px($letter_spacing).'px';
		}

		$font_weight = malmo_elated_options()->getOptionValue('tabs_font_weight');
		if(!empty($font_weight)) {
			$tabs_tipography_array['font-weight'] = $font_weight;
		}

		echo malmo_elated_dynamic_css($selector, $tabs_tipography_array);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_tabs_typography_styles');
}

if(!function_exists('malmo_elated_tabs_inital_color_styles')) {
	function malmo_elated_tabs_inital_color_styles() {
		$selector = '.eltd-tabs .eltd-tabs-nav li a';
		$styles   = array();

		if(malmo_elated_options()->getOptionValue('tabs_color')) {
			$styles['color'] = malmo_elated_options()->getOptionValue('tabs_color');
		}
		if(malmo_elated_options()->getOptionValue('tabs_back_color')) {
			$styles['background-color'] = malmo_elated_options()->getOptionValue('tabs_back_color');
		}
		if(malmo_elated_options()->getOptionValue('tabs_border_color')) {
			$styles['border-color'] = malmo_elated_options()->getOptionValue('tabs_border_color');
		}

		echo malmo_elated_dynamic_css($selector, $styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_tabs_inital_color_styles');
}
if(!function_exists('malmo_elated_tabs_active_color_styles')) {
	function malmo_elated_tabs_active_color_styles() {
		$selector = '.eltd-tabs .eltd-tabs-nav li.ui-state-active a, .eltd-tabs .eltd-tabs-nav li.ui-state-hover a';
		$styles   = array();

		if(malmo_elated_options()->getOptionValue('tabs_color_active')) {
			$styles['color'] = malmo_elated_options()->getOptionValue('tabs_color_active');
		}
		if(malmo_elated_options()->getOptionValue('tabs_back_color_active')) {
			$styles['background-color'] = malmo_elated_options()->getOptionValue('tabs_back_color_active');
		}
		if(malmo_elated_options()->getOptionValue('tabs_border_color_active')) {
			$styles['border-color'] = malmo_elated_options()->getOptionValue('tabs_border_color_active');
		}

		echo malmo_elated_dynamic_css($selector, $styles);
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_tabs_active_color_styles');
}
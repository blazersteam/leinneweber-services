<?php
namespace Malmo\Modules\TeamList;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class Team List
 */
class TeamList implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    public function __construct() {
        $this->base = 'eltd_team_list';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer. Hooked on vc_before_init
     *
     * @see eltd_core_get_carousel_slider_array_vc()
     */
    public function vcMap() {

        vc_map(array(
            'name'                      => esc_html__('Team List', 'malmo'),
            'base'                      => $this->base,
            'category'                  => esc_html__('by ELATED', 'malmo'),
            'icon'                      => 'icon-wpb-team-list extended-custom-icon',
            'as_parent'               => array('only' => 'eltd_team'),
            'content_element'         => true,
            'show_settings_on_create' => true,
            'js_view'                 => 'VcColumnView',
            'params'                    => array(
                array(
                    'heading'     => esc_html__('Number of columns', 'malmo'),
                    'type'        => 'dropdown',
                    'admin-label' => true,
                    'param_name'  => 'columns',
                    'value'       => array(
                        esc_html__('Three', 'malmo') => 'three',
                        esc_html__('Four', 'malmo')  => 'four',
                        esc_html__('Six', 'malmo')  => 'six'
                    ),
                    'save_always' => true,
                    'description' => ''
                ),
                array(
                    'heading'     => esc_html__('Space Between Items', 'malmo'),
                    'type'        => 'dropdown',
                    'admin-label' => true,
                    'param_name'  => 'space_between',
                    'value'       => array(
                        esc_html__('No', 'malmo') => 'no',
                        esc_html__('Yes', 'malmo')  => 'yes'
                    ),
                    'save_always' => true,
                    'description' => ''
                )
            )
        ));

    }

    /**
     * Renders shortcodes HTML
     *
     * @param $atts array of shortcode params
     * @param $content string shortcode content
     *
     * @return string
     */
    public function render($atts, $content = null) {

        $args = array(
            'columns' => '',
            'space_between' => 'no'
        );

        $params = shortcode_atts($args, $atts);

        extract($params);

        $params['content'] = $content;

        return malmo_elated_get_shortcode_module_template_part('templates/team-list', 'team', '', $params);

    }

}
<?php

if(!function_exists('malmo_elated_title_options_map')) {

    function malmo_elated_title_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '_title_page',
                'title' => esc_html__('Title', 'malmo'),
                'icon'  => 'fa fa-list-alt'
            )
        );

        $panel_title = malmo_elated_add_admin_panel(
            array(
                'page'  => '_title_page',
                'name'  => 'panel_title',
                'title' => esc_html__('Title Settings', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'show_title_area',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Show Title Area', 'malmo'),
                'description'   => esc_html__('This option will enable/disable Title Area', 'malmo'),
                'parent'        => $panel_title,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_show_title_area_container"
                )
            )
        );

        $show_title_area_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_title,
                'name'            => 'show_title_area_container',
                'hidden_property' => 'show_title_area',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'title_area_type',
                'type'          => 'select',
                'default_value' => 'standard',
                'label'         => esc_html__('Title Area Type', 'malmo'),
                'description'   => esc_html__('Choose title type', 'malmo'),
                'parent'        => $show_title_area_container,
                'options'       => array(
                    'standard'   => esc_html__('Standard', 'malmo'),
                    'breadcrumb' => esc_html__('Breadcrumb', 'malmo')
                ),
                'args'          => array(
                    "dependence" => true,
                    "hide"       => array(
                        "standard"   => "",
                        "breadcrumb" => "#eltd_title_area_type_container"
                    ),
                    "show"       => array(
                        "standard"   => "#eltd_title_area_type_container",
                        "breadcrumb" => ""
                    )
                )
            )
        );

        $title_area_type_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $show_title_area_container,
                'name'            => 'title_area_type_container',
                'hidden_property' => 'title_area_type',
                'hidden_value'    => '',
                'hidden_values'   => array('breadcrumb'),
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'title_area_enable_icon',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Enable Separator and Icon', 'malmo'),
                'description'   => esc_html__('This option will display Separator and Icon in Title Area', 'malmo'),
                'parent'        => $show_title_area_container,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#eltd_title_area_enable_icon_container"
                )
            )
        );

        //init icon pack hide and show array. It will be populated dinamically from collections array
        $title_area_icon_pack_hide_array = array();
        $title_area_icon_pack_show_array = array();

//do we have some collection added in collections array?
        if(is_array(malmo_elated_icon_collections()->iconCollections) && count(malmo_elated_icon_collections()->iconCollections)) {
            //get collections params array. It will contain values of 'param' property for each collection
            $title_area_icon_collections_params = malmo_elated_icon_collections()->getIconCollectionsParams();

            //foreach collection generate hide and show array
            foreach(malmo_elated_icon_collections()->iconCollections as $dep_collection_key => $dep_collection_object) {
                $title_area_icon_pack_hide_array[$dep_collection_key] = '';


                //we need to include only current collection in show string as it is the only one that needs to show
                $title_area_icon_pack_show_array[$dep_collection_key] = '#eltd_title_area_icon_'.$dep_collection_object->param.'_container';

                //for all collections param generate hide string
                foreach($title_area_icon_collections_params as $title_area_icon_collections_param) {
                    //we don't need to include current one, because it needs to be shown, not hidden
                    if($title_area_icon_collections_param !== $dep_collection_object->param) {
                        $title_area_icon_pack_hide_array[$dep_collection_key] .= '#eltd_title_area_icon_'.$title_area_icon_collections_param.'_container,';
                    }
                }

                //remove remaining ',' character
                $title_area_icon_pack_hide_array[$dep_collection_key] = rtrim($title_area_icon_pack_hide_array[$dep_collection_key], ',');
            }

        }

        $title_area_enable_icon_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $show_title_area_container,
                'name'            => 'title_area_enable_icon_container',
                'hidden_property' => 'title_area_enable_icon',
                'hidden_value'    => 'no'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $title_area_enable_icon_container,
                'type'          => 'select',
                'name'          => 'title_icon_pack',
                'default_value' => '',
                'label'         => esc_html__('Select Icon Pack', 'malmo'),
                'description'   => esc_html__('Choose icon pack for title area icon', 'malmo'),
                'options'       => malmo_elated_icon_collections()->getIconCollections(),
                'args'          => array(
                    'dependence' => true,
                    'hide'       => $title_area_icon_pack_hide_array,
                    'show'       => $title_area_icon_pack_show_array
                )
            )
        );

        if(is_array(malmo_elated_icon_collections()->iconCollections) && count(malmo_elated_icon_collections()->iconCollections)) {
            //foreach icon collection we need to generate separate container that will have dependency set
            //it will have one field inside with icons dropdown
            foreach(malmo_elated_icon_collections()->iconCollections as $collection_key => $collection_object) {
                $icons_array = $collection_object->getIconsArray();

                //get icon collection keys (keys from collections array, e.g 'font_awesome', 'font_elegant' etc.)
                $icon_collections_keys = malmo_elated_icon_collections()->getIconCollectionsKeys();

                //unset current one, because it doesn't have to be included in dependency that hides icon container
                unset($icon_collections_keys[array_search($collection_key, $icon_collections_keys)]);

                $title_area_icon_hide_values = $icon_collections_keys;

                $title_area_icon_container = malmo_elated_add_admin_container(
                    array(
                        'parent'          => $title_area_enable_icon_container,
                        'name'            => 'title_area_icon_'.$collection_object->param.'_container',
                        'hidden_property' => 'title_icon_pack',
                        'hidden_value'    => '',
                        'hidden_values'   => $title_area_icon_hide_values
                    )
                );

                malmo_elated_add_admin_field(
                    array(
                        'parent'        => $title_area_icon_container,
                        'type'          => 'select',
                        'name'          => 'title_area_icon_'.$collection_object->param,
                        'default_value' => '',
                        'label'         => esc_html__('Title Area Icon', 'malmo'),
                        'description'   => esc_html__('Choose Title Area Icon', 'malmo'),
                        'options'       => $icons_array,
                    )
                );

            }

        }


        malmo_elated_add_admin_field(
            array(
                'name'          => 'title_area_enable_breadcrumbs',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Enable Breadcrumbs', 'malmo'),
                'description'   => esc_html__('This option will display Breadcrumbs in Title Area', 'malmo'),
                'parent'        => $title_area_type_container,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'title_area_animation',
                'type'          => 'select',
                'default_value' => 'no',
                'label'         => esc_html__('Animations', 'malmo'),
                'description'   => esc_html__('Choose an animation for Title Area', 'malmo'),
                'parent'        => $show_title_area_container,
                'options'       => array(
                    'no'         => esc_html__('No Animation', 'malmo'),
                    'right-left' => esc_html__('Text right to left', 'malmo'),
                    'left-right' => esc_html__('Text left to right', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'title_area_vertial_alignment',
                'type'          => 'select',
                'default_value' => 'header_bottom',
                'label'         => esc_html__('Vertical Alignment', 'malmo'),
                'description'   => esc_html__('Specify title vertical alignment', 'malmo'),
                'parent'        => $show_title_area_container,
                'options'       => array(
                    'header_bottom' => esc_html__('From Bottom of Header', 'malmo'),
                    'window_top'    => esc_html__('From Window Top', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'title_area_content_alignment',
                'type'          => 'select',
                'default_value' => 'left',
                'label'         => esc_html__('Horizontal Alignment', 'malmo'),
                'description'   => esc_html__('Specify title horizontal alignment', 'malmo'),
                'parent'        => $show_title_area_container,
                'options'       => array(
                    'left'   => esc_html__('Left', 'malmo'),
                    'center' => esc_html__('Center', 'malmo'),
                    'right'  => esc_html__('Right', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'title_area_background_color',
                'type'        => 'color',
                'label'       => esc_html__('Background Color', 'malmo'),
                'description' => esc_html__('Choose a background color for Title Area', 'malmo'),
                'parent'      => $show_title_area_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'title_area_background_image',
                'type'        => 'image',
                'label'       => esc_html__('Background Image', 'malmo'),
                'description' => esc_html__('Choose an Image for Title Area', 'malmo'),
                'parent'      => $show_title_area_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'title_area_background_image_responsive',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Background Responsive Image', 'malmo'),
                'description'   => esc_html__('Enabling this option will make Title background image responsive', 'malmo'),
                'parent'        => $show_title_area_container,
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "#eltd_title_area_background_image_responsive_container",
                    "dependence_show_on_yes" => ""
                )
            )
        );

        $title_area_background_image_responsive_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $show_title_area_container,
                'name'            => 'title_area_background_image_responsive_container',
                'hidden_property' => 'title_area_background_image_responsive',
                'hidden_value'    => 'yes'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'title_area_background_image_parallax',
                'type'          => 'select',
                'default_value' => 'no',
                'label'         => esc_html__('Background Image in Parallax', 'malmo'),
                'description'   => esc_html__('Enabling this option will make Title background image parallax', 'malmo'),
                'parent'        => $title_area_background_image_responsive_container,
                'options'       => array(
                    'no'       => esc_html__('No', 'malmo'),
                    'yes'      => esc_html__('Yes', 'malmo'),
                    'yes_zoom' => esc_html__('Yes, with zoom out', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(array(
            'name'        => 'title_area_height',
            'type'        => 'text',
            'label'       => esc_html__('Height', 'malmo'),
            'description' => esc_html__('Set a height for Title Area', 'malmo'),
            'parent'      => $title_area_background_image_responsive_container,
            'args'        => array(
                'col_width' => 2,
                'suffix'    => 'px'
            )
        ));


        $panel_typography = malmo_elated_add_admin_panel(
            array(
                'page'  => '_title_page',
                'name'  => 'panel_title_typography',
                'title' => esc_html__('Typography', 'malmo')
            )
        );

        $group_page_title_styles = malmo_elated_add_admin_group(array(
            'name'        => 'group_page_title_styles',
            'title'       => esc_html__('Title', 'malmo'),
            'description' => esc_html__('Define styles for page title', 'malmo'),
            'parent'      => $panel_typography
        ));

        $row_page_title_styles_1 = malmo_elated_add_admin_row(array(
            'name'   => 'row_page_title_styles_1',
            'parent' => $group_page_title_styles
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'page_title_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'malmo'),
            'parent'        => $row_page_title_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_title_fontsize',
            'default_value' => '',
            'label'         => esc_html__('Font Size', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_title_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_title_lineheight',
            'default_value' => '',
            'label'         => esc_html__('Line Height', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_title_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_title_texttransform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'malmo'),
            'options'       => malmo_elated_get_text_transform_array(),
            'parent'        => $row_page_title_styles_1
        ));

        $row_page_title_styles_2 = malmo_elated_add_admin_row(array(
            'name'   => 'row_page_title_styles_2',
            'parent' => $group_page_title_styles,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'fontsimple',
            'name'          => 'page_title_google_fonts',
            'default_value' => '-1',
            'label'         => esc_html__('Font Family', 'malmo'),
            'parent'        => $row_page_title_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_title_fontstyle',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'malmo'),
            'options'       => malmo_elated_get_font_style_array(),
            'parent'        => $row_page_title_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_title_fontweight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'malmo'),
            'options'       => malmo_elated_get_font_weight_array(),
            'parent'        => $row_page_title_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_title_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_title_styles_2
        ));

        $group_page_subtitle_styles = malmo_elated_add_admin_group(array(
            'name'        => 'group_page_subtitle_styles',
            'title'       => esc_html__('Subtitle', 'malmo'),
            'description' => esc_html__('Define styles for page subtitle', 'malmo'),
            'parent'      => $panel_typography
        ));

        $row_page_subtitle_styles_1 = malmo_elated_add_admin_row(array(
            'name'   => 'row_page_subtitle_styles_1',
            'parent' => $group_page_subtitle_styles
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'page_subtitle_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'malmo'),
            'parent'        => $row_page_subtitle_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_subtitle_fontsize',
            'default_value' => '',
            'label'         => esc_html__('Font Size', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_subtitle_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_subtitle_lineheight',
            'default_value' => '',
            'label'         => esc_html__('Line Height', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_subtitle_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_subtitle_texttransform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'malmo'),
            'options'       => malmo_elated_get_text_transform_array(),
            'parent'        => $row_page_subtitle_styles_1
        ));

        $row_page_subtitle_styles_2 = malmo_elated_add_admin_row(array(
            'name'   => 'row_page_subtitle_styles_2',
            'parent' => $group_page_subtitle_styles,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'fontsimple',
            'name'          => 'page_subtitle_google_fonts',
            'default_value' => '-1',
            'label'         => esc_html__('Font Family', 'malmo'),
            'parent'        => $row_page_subtitle_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_subtitle_fontstyle',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'malmo'),
            'options'       => malmo_elated_get_font_style_array(),
            'parent'        => $row_page_subtitle_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_subtitle_fontweight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'malmo'),
            'options'       => malmo_elated_get_font_weight_array(),
            'parent'        => $row_page_subtitle_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_subtitle_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_subtitle_styles_2
        ));

        $group_page_breadcrumbs_styles = malmo_elated_add_admin_group(array(
            'name'        => 'group_page_breadcrumbs_styles',
            'title'       => esc_html__('Breadcrumbs', 'malmo'),
            'description' => esc_html__('Define styles for page breadcrumbs', 'malmo'),
            'parent'      => $panel_typography
        ));

        $row_page_breadcrumbs_styles_1 = malmo_elated_add_admin_row(array(
            'name'   => 'row_page_breadcrumbs_styles_1',
            'parent' => $group_page_breadcrumbs_styles
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'page_breadcrumb_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'malmo'),
            'parent'        => $row_page_breadcrumbs_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_breadcrumb_fontsize',
            'default_value' => '',
            'label'         => esc_html__('Font Size', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_breadcrumbs_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_breadcrumb_lineheight',
            'default_value' => '',
            'label'         => esc_html__('Line Height', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_breadcrumbs_styles_1
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_breadcrumb_texttransform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'malmo'),
            'options'       => malmo_elated_get_text_transform_array(),
            'parent'        => $row_page_breadcrumbs_styles_1
        ));

        $row_page_breadcrumbs_styles_2 = malmo_elated_add_admin_row(array(
            'name'   => 'row_page_breadcrumbs_styles_2',
            'parent' => $group_page_breadcrumbs_styles,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'fontsimple',
            'name'          => 'page_breadcrumb_google_fonts',
            'default_value' => '-1',
            'label'         => esc_html__('Font Family', 'malmo'),
            'parent'        => $row_page_breadcrumbs_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_breadcrumb_fontstyle',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'malmo'),
            'options'       => malmo_elated_get_font_style_array(),
            'parent'        => $row_page_breadcrumbs_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'selectblanksimple',
            'name'          => 'page_breadcrumb_fontweight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'malmo'),
            'options'       => malmo_elated_get_font_weight_array(),
            'parent'        => $row_page_breadcrumbs_styles_2
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'textsimple',
            'name'          => 'page_breadcrumb_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            ),
            'parent'        => $row_page_breadcrumbs_styles_2
        ));

        $row_page_breadcrumbs_styles_3 = malmo_elated_add_admin_row(array(
            'name'   => 'row_page_breadcrumbs_styles_3',
            'parent' => $group_page_breadcrumbs_styles,
            'next'   => true
        ));

        malmo_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'page_breadcrumb_hovercolor',
            'default_value' => '',
            'label'         => esc_html__('Hover/Active Color', 'malmo'),
            'parent'        => $row_page_breadcrumbs_styles_3
        ));

    }

    add_action('malmo_elated_options_map', 'malmo_elated_title_options_map', 4);

}
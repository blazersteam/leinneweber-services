<?php

class MalmoElatedCallToActionButton extends MalmoElatedWidget {
    public function __construct() {
        parent::__construct(
            'eltd_call_to_action_button', // Base ID
            esc_html__('Elated Call To Action Button', 'malmo') // Name
        );

        $this->setParams();
    }

    protected function setParams() {

        $this->params = array_merge(
            malmo_elated_icon_collections()->getWidgetIconParams(),
            array(
                array(
                    'type'        => 'textfield',
                    'title'       => esc_html__('Button Text', 'malmo'),
                    'name'        => 'button_text',
                    'description' => ''
                ),
                array(
                    'type'        => 'textfield',
                    'title'       => esc_html__('Link', 'malmo'),
                    'name'        => 'link',
                    'description' => ''
                ),
                array(
                    'type'        => 'dropdown',
                    'title'       => esc_html__('Link Target', 'malmo'),
                    'name'        => 'link_target',
                    'options'     => array(
                        '_self'  => esc_html__('Same Window', 'malmo'),
                        '_blank' => esc_html__('New Window', 'malmo')
                    ),
                    'description' => ''
                ),
                array(
                    'type'        => 'textfield',
                    'title'       => esc_html__('Text Color', 'malmo'),
                    'name'        => 'text_color',
                    'description' => ''
                ),
                array(
                    'type'        => 'textfield',
                    'title'       => esc_html__('Background Color', 'malmo'),
                    'name'        => 'background_color',
                    'description' => ''
                )
            )
        );

    }

    public function widget($args, $instance) {
		echo malmo_elated_get_module_part($args['before_widget']);

        $iconPack = $instance['icon_pack'];
        $iconHtml = '';

        if($iconPack !== '') {
            $iconPackName = malmo_elated_icon_collections()->getIconCollectionParamNameByKey($iconPack);
            $icon         = $instance[$iconPackName];

            $iconHtml = malmo_elated_icon_collections()->renderIcon($icon, $iconPack);
        }

        $buttonStyles = array();

        if($instance['background_color'] !== '') {
            $buttonStyles[] = 'background-color: '.$instance['background_color'];
        }

        if($instance['text_color'] !== '') {
            $buttonStyles[] = 'color: '.$instance['text_color'];
        }

        ?>

        <?php if($instance['link'] !== '' && $instance['button_text'] !== '') : ?>
            <a <?php malmo_elated_inline_style($buttonStyles); ?> class="eltd-call-to-action-button" target="<?php echo esc_attr($instance['link_target']); ?>" href="<?php echo esc_url($instance['link']) ?>">
				<span class="eltd-ctab-holder">
					<?php if($iconHtml !== '') : ?>
                        <span class="eltd-ctab-icon">
							<?php echo malmo_elated_get_module_part($iconHtml); ?>
						</span>
                    <?php endif; ?>
                    <?php echo esc_html($instance['button_text']); ?>
				</span>
            </a>
        <?php endif; ?>

        <?php echo malmo_elated_get_module_part($args['after_widget']);
    }
}
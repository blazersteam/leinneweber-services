<?php

/**
 * Widget that adds separator boxes type
 *
 * Class Separator_Widget
 */
class MalmoElatedSeparatorWidget extends MalmoElatedWidget {
    /**
     * Set basic widget options and call parent class construct
     */
    public function __construct() {
        parent::__construct(
            'eltd_separator_widget', // Base ID
            esc_html__('Elated Separator Widget', 'malmo') // Name
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
                'type'    => 'dropdown',
                'title'   => esc_html__('Type', 'malmo'),
                'name'    => 'type',
                'options' => array(
                    'normal'     => esc_html__('Normal', 'malmo'),
                    'full-width' => esc_html__('Full Width', 'malmo')
                )
            ),
            array(
                'type'    => 'dropdown',
                'title'   => esc_html__('Position', 'malmo'),
                'name'    => 'position',
                'options' => array(
                    'center' => esc_html__('Center', 'malmo'),
                    'left'   => esc_html__('Left', 'malmo'),
                    'right'  => esc_html__('Right', 'malmo')
                )
            ),
            array(
                'type'    => 'dropdown',
                'title'   => esc_html__('Style', 'malmo'),
                'name'    => 'border_style',
                'options' => array(
                    'solid'  => esc_html__('Solid', 'malmo'),
                    'dashed' => esc_html__('Dashed', 'malmo'),
                    'dotted' => esc_html__('Dotted', 'malmo')
                )
            ),
            array(
                'type'  => 'textfield',
                'title' => esc_html__('Color', 'malmo'),
                'name'  => 'color'
            ),
            array(
                'type'        => 'textfield',
                'title'       => esc_html__('Width', 'malmo'),
                'name'        => 'width',
                'description' => ''
            ),
            array(
                'type'        => 'textfield',
                'title'       => esc_html__('Thickness (px)', 'malmo'),
                'name'        => 'thickness',
                'description' => ''
            ),
            array(
                'type'        => 'textfield',
                'title'       => esc_html__('Top Margin', 'malmo'),
                'name'        => 'top_margin',
                'description' => ''
            ),
            array(
                'type'        => 'textfield',
                'title'       => esc_html__('Bottom Margin', 'malmo'),
                'name'        => 'bottom_margin',
                'description' => ''
            )
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {

        extract($args);

        //prepare variables
        $params = '';

        //is instance empty?
        if(is_array($instance) && count($instance)) {
            //generate shortcode params
            foreach($instance as $key => $value) {
                $params .= " $key='$value' ";
            }
        }

        echo '<div class="widget eltd-separator-widget">';

        //finally call the shortcode
        echo do_shortcode("[eltd_separator $params]"); // XSS OK

        echo '</div>'; //close div.eltd-separator-widget
    }
}
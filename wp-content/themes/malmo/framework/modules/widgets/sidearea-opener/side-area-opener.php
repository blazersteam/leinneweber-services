<?php

class MalmoElatedSideAreaOpener extends MalmoElatedWidget {
    public function __construct() {
        parent::__construct(
            'eltd_side_area_opener', // Base ID
            esc_html__('Elated Side Area Opener', 'malmo') // Name
        );

        $this->setParams();
    }

    protected function setParams() {

        $this->params = array(
            array(
                'name'        => 'side_area_opener_icon_color',
                'type'        => 'textfield',
                'title'       => esc_html__('Icon Color', 'malmo'),
                'description' => esc_html__('Define color for Side Area opener icon', 'malmo')
            )
        );

    }


    public function widget($args, $instance) {

        $sidearea_icon_styles = array();

        if(!empty($instance['side_area_opener_icon_color'])) {
            $sidearea_icon_styles[] = 'color: '.$instance['side_area_opener_icon_color'];
        }

		echo malmo_elated_get_module_part($args['before_widget']);

        $icon_size = '';
        if(malmo_elated_options()->getOptionValue('side_area_predefined_icon_size')) {
            $icon_size = malmo_elated_options()->getOptionValue('side_area_predefined_icon_size');
        }

        $default_sidearea_opener = malmo_elated_options()->getOptionValue('side_area_enable_default_opener') === 'yes';

        $default_sidearea_opener_class = $default_sidearea_opener ? 'eltd-side-menu-button-opener-default' : '';

        ?>
        <a class="eltd-side-menu-button-opener <?php echo esc_attr($icon_size.' '.$default_sidearea_opener_class); ?>" <?php malmo_elated_inline_style($sidearea_icon_styles) ?> href="javascript:void(0)">
            <?php echo malmo_elated_get_side_menu_icon_html(); ?>
        </a>

        <?php echo malmo_elated_get_module_part($args['after_widget']); ?>

    <?php }

}
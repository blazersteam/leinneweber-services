<?php

if(!function_exists('malmo_elated_woocommerce_options_map')) {

    /**
     * Add Woocommerce options page
     */
    function malmo_elated_woocommerce_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '_woocommerce_page',
                'title' => esc_html__('Woocommerce', 'malmo'),
                'icon'  => 'fa fa-shopping-cart'
            )
        );

        /**
         * Product List Settings
         */
        $panel_product_list = malmo_elated_add_admin_panel(
            array(
                'page'  => '_woocommerce_page',
                'name'  => 'panel_product_list',
                'title' => esc_html__('Product List', 'malmo')
            )
        );

        malmo_elated_add_admin_field(array(
            'name'          => 'eltd_woo_product_list_columns',
            'type'          => 'select',
            'label'         => esc_html__('Product List Columns', 'malmo'),
            'default_value' => 'eltd-woocommerce-columns-4',
            'description'   => esc_html__('Choose number of columns for product listing and related products on single product', 'malmo'),
            'options'       => array(
                'eltd-woocommerce-columns-3' => esc_html__('3 Columns (2 with sidebar)', 'malmo'),
                'eltd-woocommerce-columns-4' => esc_html__('4 Columns (3 with sidebar)', 'malmo')
            ),
            'parent'        => $panel_product_list,
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'eltd_woo_products_per_page',
            'type'          => 'text',
            'label'         => esc_html__('Number of products per page', 'malmo'),
            'default_value' => '',
            'description'   => esc_html__('Set number of products on shop page', 'malmo'),
            'parent'        => $panel_product_list,
            'args'          => array(
                'col_width' => 3
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'eltd_products_list_title_tag',
            'type'          => 'select',
            'label'         => esc_html__('Products Title Tag', 'malmo'),
            'default_value' => 'h4',
            'description'   => '',
            'options'       => array(
                'h2' => 'h2',
                'h3' => 'h3',
                'h4' => 'h4',
                'h5' => 'h5',
                'h6' => 'h6',
            ),
            'parent'        => $panel_product_list,
        ));

        /**
         * Single Product Settings
         */
        $panel_single_product = malmo_elated_add_admin_panel(
            array(
                'page'  => '_woocommerce_page',
                'name'  => 'panel_single_product',
                'title' => esc_html__('Single Product', 'malmo')
            )
        );

        malmo_elated_add_admin_field(array(
            'name'          => 'eltd_single_product_title_tag',
            'type'          => 'select',
            'label'         => esc_html__('Single Product Title Tag', 'malmo'),
            'default_value' => 'h2',
            'description'   => '',
            'options'       => array(
                'h2' => 'h2',
                'h3' => 'h3',
                'h4' => 'h4',
                'h5' => 'h5',
                'h6' => 'h6',
            ),
            'parent'        => $panel_single_product,
        ));

        /**
         * DropDown Cart Widget Settings
         */
        $panel_dropdown_cart = malmo_elated_add_admin_panel(
            array(
                'page'  => '_woocommerce_page',
                'name'  => 'panel_dropdown_cart',
                'title' => esc_html__('Dropdown Cart Widget', 'malmo')
            )
        );

        malmo_elated_add_admin_field(array(
            'name'          => 'eltd_woo_dropdown_cart_description',
            'type'          => 'text',
            'label'         => esc_html__('Cart Description', 'malmo'),
            'default_value' => '',
            'description'   => esc_html__('Enter dropdown cart description', 'malmo'),
            'parent'        => $panel_dropdown_cart
        ));
    }

    add_action('malmo_elated_options_map', 'malmo_elated_woocommerce_options_map', 18);
}
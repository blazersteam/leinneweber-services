<?php

class MalmoElatedWoocommerceDropdownCart extends WP_Widget {
    public function __construct() {
        parent::__construct(
            'eltd_woocommerce_dropdown_cart', // Base ID
            esc_html__('Elated Woocommerce Dropdown Cart', 'malmo'), // Name
            array('description' => esc_html__('Elated Woocommerce Dropdown Cart', 'malmo'),) // Args
        );

        $this->setParams();
    }

    protected function setParams() {

        $this->params = array(
            array(
                'name'        => 'woocommerce_dropdown_cart_margin',
                'type'        => 'textfield',
                'title'       => esc_html__('Margin (top right bottom left)', 'malmo'),
                'description' => esc_html__('Define margin for woocommerce dropdown cart icon', 'malmo')
            ),
            array(
                'type'        => 'dropdown',
                'title'       => esc_html__('Enable Cart Info', 'malmo'),
                'name'        => 'woocommerce_enable_cart_info',
                'options'     => array(
                    'no'  => esc_html__('No', 'malmo'),
                    'yes' => esc_html__('Yes', 'malmo')
                ),
                'description' => esc_html__('Enabling this option will show cart info (products number and price) at the right side of dropdown cart icon', 'malmo')
            ),
        );
    }

    /**
     * Generate widget form based on $params attribute
     *
     * @param array $instance
     *
     * @return null
     */
    public function form($instance) {
        if(is_array($this->params) && count($this->params)) {
            foreach($this->params as $param_array) {
                $param_name    = $param_array['name'];
                ${$param_name} = isset($instance[$param_name]) ? esc_attr($instance[$param_name]) : '';
            }

            foreach($this->params as $param) {
                switch($param['type']) {
                    case 'textfield':
                        ?>
                        <p>
                            <label for="<?php echo esc_attr($this->get_field_id($param['name'])); ?>"><?php echo
                                esc_html($param['title']); ?>:</label>
                            <input class="widefat" id="<?php echo esc_attr($this->get_field_id($param['name'])); ?>" name="<?php echo esc_attr($this->get_field_name($param['name'])); ?>" type="text" value="<?php echo esc_attr(${$param['name']}); ?>"/>
                            <?php if(!empty($param['description'])) : ?>
                                <span class="eltd-field-description"><?php echo esc_html($param['description']); ?></span>
                            <?php endif; ?>
                        </p>
                        <?php
                        break;
                    case 'dropdown':
                        ?>
                        <p>
                            <label for="<?php echo esc_attr($this->get_field_id($param['name'])); ?>"><?php echo
                                esc_html($param['title']); ?>:</label>
                            <?php if(isset($param['options']) && is_array($param['options']) && count($param['options'])) { ?>
                                <select class="widefat" name="<?php echo esc_attr($this->get_field_name($param['name'])); ?>" id="<?php echo esc_attr($this->get_field_id($param['name'])); ?>">
                                    <?php foreach($param['options'] as $param_option_key => $param_option_val) {
                                        $option_selected = '';
                                        if(${$param['name']} == $param_option_key) {
                                            $option_selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo esc_attr($option_selected); ?> value="<?php echo esc_attr($param_option_key); ?>"><?php echo esc_attr($param_option_val); ?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>
                            <?php if(!empty($param['description'])) : ?>
                                <span class="eltd-field-description"><?php echo esc_html($param['description']); ?></span>
                            <?php endif; ?>
                        </p>

                        <?php
                        break;
                }
            }
        } else { ?>
            <p><?php esc_html_e('There are no options for this widget.', 'malmo'); ?></p>
        <?php }
    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     *
     * @return array
     */
    public function update($new_instance, $old_instance) {
        $instance = array();
        foreach($this->params as $param) {
            $param_name = $param['name'];

            $instance[$param_name] = sanitize_text_field($new_instance[$param_name]);
        }

        return $instance;
    }

    public function widget($args, $instance) {
        global $post;
        extract($args);

        global $woocommerce;

        $icon_styles = array();

        if(!empty($instance['woocommerce_dropdown_cart_margin'])) {
            $icon_styles[] = 'padding: '.$instance['woocommerce_dropdown_cart_margin'];
        }

        $icon_class = 'eltd-cart-info-is-disabled';

        if(!empty($instance['woocommerce_enable_cart_info']) && $instance['woocommerce_enable_cart_info'] === 'yes') {
            $icon_class = 'eltd-cart-info-is-active';
        }

        $cart_description = malmo_elated_options()->getOptionValue('eltd_woo_dropdown_cart_description');
        ?>
        <div class="eltd-shopping-cart-holder <?php echo esc_html($icon_class); ?>" <?php malmo_elated_inline_style($icon_styles) ?>>
            <div class="eltd-shopping-cart-inner">
                <?php $cart_is_empty = sizeof($woocommerce->cart->get_cart()) <= 0; ?>
                <a class="eltd-header-cart" href="<?php echo esc_url(wc_get_cart_url()); ?>">
					<span class="eltd-cart-icon">
                         <?php echo malmo_elated_icon_collections()->renderIcon('lnr-cart ', 'linear_icons'); ?>
                        <span class="eltd-cart-count"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?></span>
                    </span>
					<span class="eltd-cart-info">
						<span class="eltd-cart-info-number"><?php echo sprintf(_n('%d item', '%d items', WC()->cart->cart_contents_count, 'malmo'), WC()->cart->cart_contents_count); ?></span>
						<span class="eltd-cart-info-total"><?php echo wp_kses($woocommerce->cart->get_cart_subtotal(), array(
                                'span' => array(
                                    'class' => true,
                                    'id'    => true
                                )
                            )); ?></span>
					</span>
                </a>
                <?php if(!$cart_is_empty) : ?>
                    <div class="eltd-shopping-cart-dropdown">
                        <ul>
                            <?php foreach($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) :
                                $_product = $cart_item['data'];
                                // Only display if allowed
                                if(!$_product->exists() || $cart_item['quantity'] == 0) {
                                    continue;
                                }
                                // Get price
                                $product_price = get_option( 'woocommerce_tax_display_cart' ) == 'excl' ? wc_get_price_excluding_tax( $_product ) : wc_get_price_including_tax( $_product );
?>
                                <li>
                                    <div class="eltd-item-image-holder">
                                        <div class="eltd-item-image-overlay">
                                            <div class="eltd-item-image-overlay-inner">
                                            </div>
                                        </div>
                                        <a href="<?php echo esc_url(get_permalink($cart_item['product_id'])); ?>">
                                            <?php echo wp_kses($_product->get_image(), array(
                                                'img' => array(
                                                    'src'    => true,
                                                    'width'  => true,
                                                    'height' => true,
                                                    'class'  => true,
                                                    'alt'    => true,
                                                    'title'  => true,
                                                    'id'     => true
                                                )
                                            )); ?>
                                        </a>
                                    </div>
                                    <div class="eltd-item-info-holder">
                                        <h5 class="eltd-product-title">
                                            <a href="<?php echo esc_url(get_permalink($cart_item['product_id'])); ?>"><?php echo apply_filters('malmo_elated_woo_widget_cart_product_title', $_product->get_title(), $_product); ?></a>
                                        </h5>
                                        <div class="eltd-quantity-price">
                                            <span class="eltd-quantity"><?php echo esc_html($cart_item['quantity']); ?></span>
                                            <span> x </span>
                                            <?php echo apply_filters('malmo_elated_woo_cart_item_price_html', wc_price($product_price), $cart_item, $cart_item_key); ?>
                                        </div>
                                        <?php echo apply_filters('malmo_elated_woo_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s"><span class="icon_close"></span></a>', esc_url( wc_get_cart_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'malmo')), $cart_item_key); ?>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                            <div class="eltd-cart-bottom">
                                <div class="eltd-subtotal-holder clearfix">
                                    <span class="eltd-total"><?php esc_html_e('Order Total:', 'malmo'); ?></span>
									<span class="eltd-total-amount">
										<?php echo wp_kses($woocommerce->cart->get_cart_subtotal(), array(
                                            'span' => array(
                                                'class' => true,
                                                'id'    => true
                                            )
                                        )); ?>
									</span>
                                </div>
                                <?php if(!empty($cart_description)) { ?>
                                    <div class="eltd-cart-description">
                                        <div class="eltd-cart-description-inner">
                                            <span><?php echo esc_html($cart_description); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="eltd-btn-holder clearfix">
                                    <?php echo malmo_elated_get_button_html(array(
                                        'link'         => wc_get_checkout_url(),
                                        'custom_class' => 'checkout',
                                        'text'         => esc_html__('Checkout', 'malmo'),
                                        'size'         => 'small',
                                        'type'         => 'solid'
                                    )); ?>
                                    <?php echo malmo_elated_get_button_html(array(
                                        'link'         => wc_get_cart_url(),
                                        'custom_class' => 'view-cart',
                                        'text'         => esc_html__('View Cart', 'malmo'),
                                        'size'         => 'small',
                                        'type'         => 'solid'
                                    )); ?>
                                </div>
                            </div>
                        </ul>
                    </div>
                <?php else : ?>
                    <div class="eltd-shopping-cart-dropdown">
                        <ul>
                            <li class="eltd-empty-cart"><?php esc_html_e('No products in the cart.', 'malmo'); ?></li>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php
    }
}


add_filter('add_to_cart_fragments', 'malmo_elated_woocommerce_header_add_to_cart_fragment');

function malmo_elated_woocommerce_header_add_to_cart_fragment($fragments) {
    global $woocommerce;

    $cart_description = malmo_elated_options()->getOptionValue('eltd_woo_dropdown_cart_description');

    ob_start();

    ?>

    <div class="eltd-shopping-cart-inner">
        <?php $cart_is_empty = sizeof($woocommerce->cart->get_cart()) <= 0; ?>
        <a class="eltd-header-cart" href="<?php echo esc_url(wc_get_cart_url()); ?>">
            <span class="eltd-cart-icon">
                <?php echo malmo_elated_icon_collections()->renderIcon('lnr-cart ', 'linear_icons'); ?>
                <span class="eltd-cart-count"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?></span>
            </span>
			<span class="eltd-cart-info">
				<span class="eltd-cart-info-number"><?php echo sprintf(_n('%d item', '%d items', WC()->cart->cart_contents_count, 'malmo'), WC()->cart->cart_contents_count); ?></span>
				<span class="eltd-cart-info-total"><?php echo wp_kses($woocommerce->cart->get_cart_subtotal(), array(
                        'span' => array(
                            'class' => true,
                            'id'    => true
                        )
                    )); ?></span>
			</span>
        </a>
        <?php if(!$cart_is_empty) : ?>
            <div class="eltd-shopping-cart-dropdown">
                <ul>
                    <?php foreach($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) :
                        $_product = $cart_item['data'];
                        // Only display if allowed
                        if(!$_product->exists() || $cart_item['quantity'] == 0) {
                            continue;
                        }
                        // Get price
                        $product_price = get_option( 'woocommerce_tax_display_cart' ) == 'excl' ? wc_get_price_excluding_tax( $_product ) : wc_get_price_including_tax( $_product );
?>
                        <li>
                            <div class="eltd-item-image-holder">
                                <div class="eltd-item-image-overlay">
                                    <div class="eltd-item-image-overlay-inner">
                                    </div>
                                </div>
                                <a href="<?php echo esc_url(get_permalink($cart_item['product_id'])); ?>">
                                    <?php echo wp_kses($_product->get_image(), array(
                                        'img' => array(
                                            'src'    => true,
                                            'width'  => true,
                                            'height' => true,
                                            'class'  => true,
                                            'alt'    => true,
                                            'title'  => true,
                                            'id'     => true
                                        )
                                    )); ?>
                                </a>
                            </div>
                            <div class="eltd-item-info-holder">
                                <h5 class="eltd-product-title">
                                    <a href="<?php echo esc_url(get_permalink($cart_item['product_id'])); ?>"><?php echo apply_filters('malmo_elated_woo_widget_cart_product_title', $_product->get_title(), $_product); ?></a>
                                </h5>
                                <div class="eltd-quantity-price">
                                    <span class="eltd-quantity"><?php echo esc_html($cart_item['quantity']); ?></span>
                                    <span> x </span>
                                    <?php echo apply_filters('malmo_elated_woo_cart_item_price_html', wc_price($product_price), $cart_item, $cart_item_key); ?>
                                </div>
                                <?php echo apply_filters('malmo_elated_woo_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s"><span class="icon_close"></span></a>', esc_url( wc_get_cart_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'malmo')), $cart_item_key); ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    <div class="eltd-cart-bottom">
                        <div class="eltd-subtotal-holder clearfix">
                            <span class="eltd-total"><?php esc_html_e('Order Total:', 'malmo'); ?></span>
							<span class="eltd-total-amount">
								<?php echo wp_kses($woocommerce->cart->get_cart_subtotal(), array(
                                    'span' => array(
                                        'class' => true,
                                        'id'    => true
                                    )
                                )); ?>
							</span>
                        </div>
                        <?php if(!empty($cart_description)) { ?>
                            <div class="eltd-cart-description">
                                <div class="eltd-cart-description-inner">
                                    <span><?php echo esc_html($cart_description); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="eltd-btn-holder clearfix">
                            <?php echo malmo_elated_get_button_html(array(
                                'link'         => wc_get_checkout_url(),
                                'custom_class' => 'checkout',
                                'text'         => esc_html__('Checkout', 'malmo'),
                                'size'         => 'small',
                                'type'         => 'solid'
                            )); ?>
                            <?php echo malmo_elated_get_button_html(array(
                                'link'         => wc_get_cart_url(),
                                'custom_class' => 'view-cart',
                                'text'         => esc_html__('View Cart', 'malmo'),
                                'size'         => 'small',
                                'type'         => 'solid'
                            )); ?>
                        </div>
                    </div>
                </ul>
            </div>
        <?php else : ?>
            <div class="eltd-shopping-cart-dropdown">
                <ul>
                    <li class="eltd-empty-cart"><?php esc_html_e('No products in the cart.', 'malmo'); ?></li>
                </ul>
            </div>
        <?php endif; ?>
    </div>

    <?php
    $fragments['div.eltd-shopping-cart-inner'] = ob_get_clean();

    return $fragments;
}

?>
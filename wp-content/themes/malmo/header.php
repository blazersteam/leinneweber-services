<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php if (!malmo_elated_is_ajax_request()) malmo_elated_wp_title(); ?>
    <?php
    /**
     * @see malmo_elated_header_meta() - hooked with 10
     * @see eltd_user_scalable - hooked with 10
     */
    ?>
	<?php if (!malmo_elated_is_ajax_request()) do_action('malmo_elated_header_meta'); ?>

	<?php if (!malmo_elated_is_ajax_request()) wp_head(); ?>
</head>

<body <?php body_class();?> <?php $image_url = malmo_elated_get_fixed_background_image_url(); if ($image_url != '') echo 'style="background-image: url(\''.esc_url($image_url).'\')"'; ?>>
<?php if (!malmo_elated_is_ajax_request()) malmo_elated_get_side_area(); ?>


<?php
if((!malmo_elated_is_ajax_request()) && malmo_elated_options()->getOptionValue('smooth_page_transitions') == "yes") {
	$ajax_class = malmo_elated_options()->getOptionValue('smooth_pt_true_ajax') === 'yes' ? 'eltd-ajax' : 'eltd-mimic-ajax';
?>
<div class="eltd-smooth-transition-loader <?php echo esc_attr($ajax_class); ?>">
    <div class="eltd-st-loader">
        <div class="eltd-st-loader1">
            <?php malmo_elated_loading_spinners(); ?>
        </div>
    </div>
</div>
<?php } ?>

<div class="eltd-wrapper">
    <div class="eltd-wrapper-inner">
        
	    <?php if (!malmo_elated_is_ajax_request()) malmo_elated_get_header(); ?>
	    <?php if ((!malmo_elated_is_ajax_request()) && malmo_elated_get_meta_field_intersect('show_back_button') == "yes") { ?>
            <a id='eltd-back-to-top'  href='#'>
                <span class="eltd-icon-stack">
                     <?php echo malmo_elated_icon_collections()->renderIcon('lnr-chevron-up', 'linear_icons'); ?>
                </span>
            </a>
        <?php } ?>
	    <?php if (!malmo_elated_is_ajax_request()) malmo_elated_get_full_screen_menu(); ?>

        <div class="eltd-content" <?php malmo_elated_content_elem_style_attr(); ?>>
            <?php if(malmo_elated_is_ajax_enabled()) { ?>
            <div class="eltd-meta">
                <?php do_action('malmo_elated_ajax_meta'); ?>
                <span id="eltd-page-id"><?php echo esc_html($wp_query->get_queried_object_id()); ?></span>
                <div class="eltd-body-classes"><?php echo esc_html(implode( ',', get_body_class())); ?></div>
            </div>
            <?php } ?>
            <div class="eltd-content-inner">

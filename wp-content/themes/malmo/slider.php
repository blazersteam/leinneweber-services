<?php
$malmo_slider_shortcode = get_post_meta(malmo_elated_get_page_id(), 'eltd_page_slider_meta', true);
$malmo_slider_shortcode = apply_filters('malmo_elated_slider_shortcode', $malmo_slider_shortcode);

if(!empty($malmo_slider_shortcode)) : ?>
	<div class="eltd-slider">
		<div class="eltd-slider-inner">
			<?php echo do_shortcode(wp_kses_post($malmo_slider_shortcode)); // XSS OK ?>
		</div>
	</div>
<?php endif; ?>